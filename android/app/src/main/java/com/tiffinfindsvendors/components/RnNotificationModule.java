// RnNotificationModule.java
/*
* Author: Yadhukrishnan Ekambaran
* */

package com.tiffinfindsvendors.components;

import android.app.Activity;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.tiffinfindsvendors.NotificationReceiver;


import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class RnNotificationModule extends ReactContextBaseJavaModule {

    private static final String TAG = "RnNotificationModule";

    private final ReactApplicationContext reactContext;

    public static final String INTENT_FILTER = "notification_message";
    private static final String NOTIFICATION_PLACEHOLDER_ICON_NAME = "ic_launcher";
    private static final String NOTIFICATION_PLACEHOLDER_ICON_TYPE = "mipmap";
    private static final String NOTIFICATION_ICON_NAME = "ic_notification";
    private static final String NOTIFICATION_ICON_TYPE = "drawable";

    private static final int NOTIFICATION_ID = 1;

    private final NotificationManager mNotificationManager;

    public RnNotificationModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
        mNotificationManager = (NotificationManager) reactContext.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    public String getName() {
        return "RnNotification";
    }

    @ReactMethod
    public void createNotificationChannel(String channelId, String channelName, int importance, String description) {
        if (TextUtils.isEmpty(channelId)) {
            throw new IllegalArgumentException("Channel Id can not be empty");
        }

        if (TextUtils.isEmpty(channelName)) {
            throw new IllegalArgumentException("Channel name can not be empty");
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, importance);
            notificationChannel.setShowBadge(true);
            notificationChannel.enableVibration(true);
            if (description != null) {
                notificationChannel.setDescription(description);
            }

            mNotificationManager.createNotificationChannel(notificationChannel);
        }
    }

    @ReactMethod
    public void showNotification(String channelId, String notificationContent) {
        if (TextUtils.isEmpty(channelId)) throw new IllegalArgumentException("Channel Id is empty");

        try {
            PendingIntent intent = getPendingIntent(notificationContent);
            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(reactContext, channelId);
            builder.setContentTitle(getTitle(notificationContent));
            builder.setContentText(getBody(notificationContent));
            builder.setContentIntent(intent);
            builder.setAutoCancel(true);
            int smallIcon = getCurrentActivity().getResources()
                    .getIdentifier(NOTIFICATION_ICON_NAME,
                            NOTIFICATION_ICON_TYPE,
                            getCurrentActivity().getPackageName());
            if (smallIcon == 0) {
                smallIcon = getCurrentActivity().getResources()
                        .getIdentifier(NOTIFICATION_PLACEHOLDER_ICON_NAME,
                                NOTIFICATION_PLACEHOLDER_ICON_TYPE,
                                getCurrentActivity().getPackageName());
                if (smallIcon == 0) {
                    throw new IllegalArgumentException("ic_launcher could not be found");
                }
            }

            builder.setSmallIcon(smallIcon);

            mNotificationManager.notify(NOTIFICATION_ID, builder.build());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @ReactMethod
    public void onMessageClicked() {
        Log.d(TAG, "--- onMessageClicked ---");
        LocalBroadcastManager.getInstance(getCurrentActivity()).registerReceiver(messageReceiver, new IntentFilter(INTENT_FILTER));
    }

    private final BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "--- MESSAGE RECEIVED ---");

            reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
            .emit("NotificationInteraction", getSendingData(intent.getExtras()));
        }
    };

    @ReactMethod
    public void getInitialNotification(@NonNull Promise promise) {
        String data = getNotificationFromPreference();
        clearNotification();
        if (data != null) {
            promise.resolve(data);
        } else {
            promise.reject(new NullPointerException("Notification data is empty"));
        }
    }

    private PendingIntent getPendingIntent(String notificationContent) {
        Intent intent = new Intent(getCurrentActivity(), NotificationReceiver.class);
        intent.setAction("TiffinFinds_Message_receiver");
        try {
            intent.putExtra("data", getData(notificationContent));
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        int requestId = (int) System.currentTimeMillis();
        int flags = PendingIntent.FLAG_CANCEL_CURRENT;
        return PendingIntent.getBroadcast(reactContext, requestId, intent, flags);
    }

    private String getTitle(String notificationContent) throws JSONException {
        JSONObject jsonObject = new JSONObject(notificationContent);
        JSONObject notification = jsonObject.getJSONObject("notification");
        return notification.getString("title");
    }

    private String getBody(String notificationContent) throws JSONException {
        JSONObject jsonObject = new JSONObject(notificationContent);
        JSONObject notification = jsonObject.getJSONObject("notification");
        return notification.getString("body");
    }

    private String getData(String notificationContent) throws JSONException {
        JSONObject jsonObject = new JSONObject(notificationContent);
        return jsonObject.getJSONObject("data").toString();
    }

    private String getSendingData(Bundle bundle) {
        return bundle.getString("data");
    }

    private String getNotificationFromPreference() {
        SharedPreferences preferences = reactContext.getSharedPreferences("TiffinFinds-Notification", Context.MODE_PRIVATE);
        return preferences.getString("NotificationData", null);
    }

    private void clearNotification() {
        SharedPreferences preferences = reactContext.getSharedPreferences("TiffinFinds-Notification", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }
}
