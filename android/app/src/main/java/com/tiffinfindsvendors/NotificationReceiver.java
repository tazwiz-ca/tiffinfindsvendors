package com.tiffinfindsvendors;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;


import com.tiffinfindsvendors.components.RnNotificationModule;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class NotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent broadcastIntent = new Intent(RnNotificationModule.INTENT_FILTER);
        broadcastIntent.putExtras(intent.getExtras());
        boolean isSuccess = LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastIntent);
        if (isSuccess) {
            Log.d("NotificationReceiver", "--- BROADCAST SUCCESS ---");
            Intent activityIntent = new Intent(context, MainActivity.class);
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            context.startActivity(activityIntent);
        } else {
            saveExtras(context, intent.getExtras().getString("data"));
            Intent activityIntent = new Intent(context, MainActivity.class);
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(activityIntent);
            Log.d("NotificationReceiver", "--- BROADCAST failed ---");
        }
    }

    private void saveExtras(Context context, String data) {
        SharedPreferences preferences = context.getSharedPreferences("TiffinFinds-Notification", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("NotificationData", data);
        editor.apply();
    }
}
