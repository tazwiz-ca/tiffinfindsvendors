/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import { notificationBackgroundHandler } from './src/notification/NotificationService';

AppRegistry.registerComponent(appName, () => App);

notificationBackgroundHandler();
