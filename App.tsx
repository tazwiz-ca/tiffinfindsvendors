import * as React from 'react';
import { Provider } from 'react-redux';
import * as Sentry from '@sentry/react-native';
import { NavigationContainer } from '@react-navigation/native';
import { LogBox } from 'react-native';

import { store } from './src/store/index';
import RootStackNav from './src/navigations/RootstackNav';
import AppNoInternet from './src/components/common/AppNoInternet';
import AppToasts from './src/components/common/AppToasts';
import AppLoader from './src/components/common/AppLoader';
import { subscribe, APP_CHANNEL } from './src/notification/NotificationService';
import { createNotificationChannel } from './src/notification/NotificationService';

const App: React.FunctionComponent<any> = () => {
  React.useEffect(() => {
    //Subscribing to app channel for general notifications
    subscribe(APP_CHANNEL);
    //Channel creation for local notification
    createNotificationChannel();

    //Sentry logging
    Sentry.init({
      dsn:
        'https://62576966fb5e4982b5c950cc1656ec6b@o439611.ingest.sentry.io/5650987',
      enableAutoSessionTracking: true,
      // Sessions close after app is 10 seconds in the background.
      sessionTrackingIntervalMillis: 10000,
      maxBreadcrumbs: 50,
      debug: true,
      enableNative: false,
    });

    /** Development-mode  */
    LogBox.ignoreAllLogs(); //Ignore all log warnings
    console.error = () => {}; //Ignore all errors
  }, []);

  return (
    <Provider store={store}>
      <AppNoInternet>
        <AppLoader>
          <AppToasts>
            <NavigationContainer>
              <RootStackNav />
            </NavigationContainer>
          </AppToasts>
        </AppLoader>
      </AppNoInternet>
    </Provider>
  );
};

export default App;
