import HttpService from '../network/HttpService';
import serviceConst from '../configs/serviceConst';
import { PaymentState } from '../store/reducers/payments/payments';

export const savePaymentInfo = async (
  token: string,
  request: PaymentState,
): Promise<any> => {
  const { BASE_URL, SAVE_PAYMENT } = serviceConst;
  HttpService.setHeader(token);
  const { data } = await HttpService.post(BASE_URL + SAVE_PAYMENT, request);

  return data;
};
