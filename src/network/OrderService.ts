import HttpService from '../network/HttpService';
import serviceConst from '../configs/serviceConst';

export const getOrders = async (token: string) => {
  const { BASE_URL, GET_ORDERS } = serviceConst;
  HttpService.setHeader(token);
  const { data } = await HttpService.get(BASE_URL + GET_ORDERS);
  return data;
};

export const updateOrderStatus = async (
  token: string,
  orderId: string,
  status: boolean,
  description: string,
) => {
  const { BASE_URL, UPDATE_ORDER_STATUS } = serviceConst;
  let isAccepted = status ? true : false;
  let isRejected = status ? false : true;
  const requestParam = {
    isAccepted: isAccepted,
    isRejected: isRejected,
    cancelReason: description,
  };
  HttpService.setHeader(token);
  const { data } = await HttpService.put(
    BASE_URL + UPDATE_ORDER_STATUS + orderId,
    requestParam,
  );
  return data;
};
