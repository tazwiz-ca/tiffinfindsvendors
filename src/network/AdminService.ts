import HttpService from '../network/HttpService';
import serviceConst from '../configs/serviceConst';

export const fetchAllProviderInfos = async (token: string): Promise<any> => {
  const { BASE_URL, GET_PROVIDER_INFOS } = serviceConst;
  HttpService.setHeader(token);
  const { data } = await HttpService.get(BASE_URL + GET_PROVIDER_INFOS);

  return data;
};

export const verifyProvider = async (
  token: string,
  providerId: string,
  isApprove: string,
): Promise<any> => {
  const { BASE_URL, APPROVE_PROVIDER } = serviceConst;
  HttpService.setHeader(token);
  const { data } = await HttpService.put(
    BASE_URL + APPROVE_PROVIDER + providerId + '/' + isApprove,
  );

  return data;
};

export const fetchProviderById = async (request: any) => {
  const { BASE_URL, FETCH_PROVIDER_DETAILS } = serviceConst;
  const { data } = await HttpService.post(
    BASE_URL + FETCH_PROVIDER_DETAILS,
    request,
  );

  return data;
};
