import HttpService from '../network/HttpService';
import serviceConst from '../configs/serviceConst';

export const getUserProfile = async (token: string): Promise<any> => {
  const { BASE_URL, USER_PROFILE } = serviceConst;
  HttpService.setHeader(token);
  const { data } = await HttpService.get(BASE_URL + USER_PROFILE);

  return data;
};

export const fetchLoginFormDetails = async (
  email: string,
  password: string,
  typeOfUser: string,
) => {
  const { BASE_URL, FETCH_LOGIN_API_URI } = serviceConst;
  const { data } = await HttpService.post(BASE_URL + FETCH_LOGIN_API_URI, {
    email,
    password,
    typeOfUser,
  });

  return data;
};

export const updatePhoneNumber = async (
  token: string,
  email: string,
  contactNumber: string,
) => {
  const { BASE_URL, UPDATE_PHONE_API } = serviceConst;
  HttpService.setHeader(token);
  const { data } = await HttpService.post(
    BASE_URL + UPDATE_PHONE_API + contactNumber,
    { email },
  );
  return data;
};

export const registerUser = async (body: object) => {
  const { BASE_URL, REGISTER_API_URI } = serviceConst;
  const { data } = await HttpService.post(BASE_URL + REGISTER_API_URI, body);
  return data;
};

//Reset the password from the forgot password screen with email
export const sendEmailForgotPwd = async (email: string) => {
  const { BASE_URL, FORGOT_PWD_API_URI } = serviceConst;
  const apiUrl = `${BASE_URL}${FORGOT_PWD_API_URI}${email}`;
  const { data } = await HttpService.post(apiUrl);

  return data;
};

//Resend the email from check your email page
export const resendEmailForgotPwd = async (email: string) => {
  const { BASE_URL, RESEND_API_URI } = serviceConst;
  const apiUrl = `${BASE_URL}${RESEND_API_URI}${email}`;
  const { data } = await HttpService.post(apiUrl);

  return data;
};

//Reset pwd with email and token from reset pwd screen
export const resetPassword = async (
  email: string,
  token: string,
  password: string,
) => {
  const { BASE_URL, RESET_API_URI } = serviceConst;
  const { data } = await HttpService.post(BASE_URL + RESET_API_URI, {
    email,
    token,
    password,
  });
  return data;
};

export const changePassword = async (token: string, request: object) => {
  const { BASE_URL, RESET_API_URI } = serviceConst;
  const url = BASE_URL + RESET_API_URI;
  HttpService.setHeader(token);
  const { data } = await HttpService.put(url, request);
  return data;
};

export const generateOTP = async (token: string, contactNumber: string) => {
  const { BASE_URL, OTP_GENERATE_URI } = serviceConst;
  const url = BASE_URL + OTP_GENERATE_URI + contactNumber;
  HttpService.setHeader(token);
  const { data } = await HttpService.post(url);
  return data;
};

export const verifyOTP = async (token: string, otp: string) => {
  const { BASE_URL, VERIFY_OTP } = serviceConst;
  const url = BASE_URL + VERIFY_OTP + otp;
  HttpService.setHeader(token);
  const { data } = await HttpService.put(url);
  return data;
};
