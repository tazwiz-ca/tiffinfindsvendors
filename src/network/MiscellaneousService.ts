import HttpService from './HttpService';
import serviceConst from '../configs/serviceConst';

export const getNotifications = async (token: string) => {
  const { BASE_URL, FETCH_NOTIFICATIONS } = serviceConst;
  HttpService.setHeader(token);
  const { data } = await HttpService.get(BASE_URL + FETCH_NOTIFICATIONS);

  return data;
};

export const handleHelp = async (token: string, description: string) => {
  const { BASE_URL, GET_HELP_URI } = serviceConst;
  HttpService.setHeader(token);
  const { data } = await HttpService.post(BASE_URL + GET_HELP_URI, {
    description,
  });

  return data;
};

export const getLatestVersions = async (
  platform: string,
  buildNumber: string,
  buildVersion: string,
) => {
  const { BASE_URL, MOBILE_APP_VERSIONS } = serviceConst;
  const { data } = await HttpService.get(
    BASE_URL +
      MOBILE_APP_VERSIONS +
      platform +
      '/' +
      buildNumber +
      '/' +
      buildVersion,
  );

  return data;
};

export const getSupportCtNumber = async (token: string) => {
  const { BASE_URL, WHATSAPP_CHAT_NUMBER } = serviceConst;
  HttpService.setHeader(token);
  const { data } = await HttpService.get(BASE_URL + WHATSAPP_CHAT_NUMBER);

  return data;
};
