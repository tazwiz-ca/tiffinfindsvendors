import HttpService from '../network/HttpService';
import serviceConst from '../configs/serviceConst';
import { MenuRequestBody } from '../store/reducers/helpers/reducerUtil';

export const createOrUpdateMenu = async (
  token: string,
  request: MenuRequestBody,
): Promise<any> => {
  const { BASE_URL, CREATE_MENU } = serviceConst;
  HttpService.setHeader(token);
  const { data } = await HttpService.post(BASE_URL + CREATE_MENU, request);

  return data;
};

export const fetchMenu = async (token: string, id: string): Promise<any> => {
  const { BASE_URL, FETCH_MENU } = serviceConst;
  HttpService.setHeader(token);
  const { data } = await HttpService.get(BASE_URL + FETCH_MENU + id);

  return data;
};

export const deleteMenu = async (
  token: string,
  planId: string,
): Promise<any> => {
  const { BASE_URL, DELETE_MENU } = serviceConst;
  HttpService.setHeader(token);
  const { data } = await HttpService.delete(BASE_URL + DELETE_MENU + planId);

  return data;
};
