import { Platform } from 'react-native';
import jwtDecode from 'jwt-decode';
import moment from 'moment';
import _ from 'lodash';
import * as Sentry from '@sentry/react-native';
import AsyncStorage from '@react-native-community/async-storage';
import RNFetchBlob from 'rn-fetch-blob';

/**
 *
 * @param {*} date
 * Check if the date is tomorow
 */
const isDateTomorow = (date: string) => {
  let days = moment(moment(date, 'Do MMM,YYYY').toDate()).diff(
    moment(),
    'days',
  );

  //Check if the date is yesterday, to avoid from checking the days
  if (!moment(moment(date, 'Do MMM,YYYY').toDate()).isAfter(moment())) return;

  return days + 1 === 1;
};

/**
 *
 * @param {*} date
 * Check if the date is next month
 */
const isDateNextMonth = (date: string) => {
  let days = moment(moment(date, 'Do MMM,YYYY').toDate()).diff(
    moment(),
    'days',
  );

  return days + 1 === 30;
};

/**
 *
 * @param {*} date
 * Check if the date is next week
 */
const isDateNextWeek = (date: string) => {
  let days = moment(moment(date, 'Do MMM,YYYY').toDate()).diff(
    moment(),
    'days',
  );

  return days + 1 === 7;
};

const getTomorowDate = () => {
  return moment().add(1, 'days');
};

const getNextWeekDate = () => {
  return moment().add(7, 'days');
};

const getNextMonthDate = () => {
  return moment().add(30, 'days');
};

const bytesToMegaBytes = (bytes: number) => bytes / (1024 * 1024);

/**
 *
 * @param {*} name
 * input --> lambtoncollegeofferletter.pdf
 * output --> lambt...PDF
 */
const getOptimalFileName = (name: string) => {
  if (!name) return;
  const ext = '.' + name.split('.')[1];
  const fileName = name.split('.')[0];

  return fileName.length > 5
    ? fileName.substring(0, 5) + '..' + ext.toUpperCase()
    : fileName + ext.toUpperCase();
};

const isEmpty = (obj: object) => {
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) return false;
  }
  return true;
};
/**
 *
 * @param {*} text
 * @param {*} cropNumber
 * Return text with a cropped number
 */
const renderTrimmedText = (text: string, cropNumber: number) => {
  if (!text) return;
  if (text.replace(/\s/g, '').length > cropNumber)
    return text.slice(0, cropNumber) + '...';

  return text;
};

const getFormattedTime = (timestamp: string, format = 'hh:mm a') => {
  if (!timestamp) return moment().format(format);
  return moment(timestamp).format(format);
};

const fetchValueFromKey = (data: { [key: string]: string }, isKey: boolean) => {
  if (!data) return;
  for (let item in data) {
    return isKey ? item : data[item];
  }
};

const formatDateToDaysAgo = (timestamp: string) => {
  if (!timestamp) return;
  return moment(timestamp).fromNow();
};

const addSpaceBetweenWords = (string: string) => {
  return string.replace(/([a-z])([A-Z])/, '$1 $2');
};

// Capitalize first letter of a string
const capitalizeFirstLetter = (string: string): string => {
  if (!string) return '';
  return string.toString().charAt(0).toUpperCase() + string.toString().slice(1);
};

const capitalizeAndSpace = (string: string) => {
  let upperCase = capitalizeFirstLetter(string);
  return addSpaceBetweenWords(upperCase);
};

const captializedArray = (arr = []) => {
  return arr.map(
    (val: any) =>
      val?.toString()?.charAt(0)?.toUpperCase() + val?.toString()?.slice(1),
  );
};

const isValidEmail = (email: string) => {
  let regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
  return regex.test(email);
};

const storeData = async (key: string, value: any) => {
  try {
    const jsonValue = JSON.stringify(value);
    await AsyncStorage.setItem(key, jsonValue);
  } catch (e) {
    Sentry.captureException(e);
    console.log(e);
  }
};

const getData = async (key: string) => {
  try {
    const jsonValue = await AsyncStorage.getItem(key);
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch (e) {
    Sentry.captureException(e);
    console.log(e);
  }
};

const removeData = async (key: string) => {
  try {
    await AsyncStorage.removeItem(key);
  } catch (error) {
    Sentry.captureException(error);
    console.log(error);
  }
};

const getNameSign = (fullName: string) => {
  try {
    if (!fullName) return;
    if (fullName.trim().split(' ').length > 1) {
      const firstName = fullName.split(' ')[0];
      const lastName = fullName.split(' ')[1];
      return (
        firstName.charAt(0)?.toUpperCase() + lastName.charAt(0)?.toUpperCase()
      );
    } else {
      return fullName.charAt(0)?.toUpperCase();
    }
  } catch (error) {
    Sentry.captureException(error);
    console.log(error);
    return '';
  }
};

const zeroPad = (num: string, places: number) => {
  var zero = places - num.toString().length + 1;
  return Array(+(zero > 0 && zero)).join('0') + num;
};

const formatOrderId = (orderId: string) => {
  if (!orderId) return;

  return zeroPad(orderId, 5);
};

/**
 * Check if the current time is greater than 7pm
 * @returns
 */
const getStartDateForCart = () => {
  try {
    let now = moment();
    let hourToCheck = 19;
    let dateToCheck = now.hour(hourToCheck).minute(0);

    let isAfterSeven = moment().isAfter(dateToCheck);

    return moment()
      .add(isAfterSeven ? 2 : 1, 'days')
      .toDate();
  } catch (error) {
    Sentry.captureException(error);
    return moment().add(1, 'days').toDate();
  }
};

const getBase64Path = async (uri: any, type: any) => {
  if (!uri) return;
  let path = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
  const base64Data = await RNFetchBlob.fs.readFile(path, 'base64');
  return 'data:' + type + ';base64,' + base64Data;
};

const decodeToken = (token: string) => {
  //decode the token to get the vendor object
  const vendor = jwtDecode(token);
  //save the token and vendor obj in async storage
  storeData('vendor', { vendor, token });
  return vendor;
};

export default {
  isDateTomorow,
  isDateNextMonth,
  isDateNextWeek,
  getTomorowDate,
  getNextWeekDate,
  getNextMonthDate,
  bytesToMegaBytes,
  getOptimalFileName,
  isEmpty,
  zeroPad,
  renderTrimmedText,
  getFormattedTime,
  fetchValueFromKey,
  formatDateToDaysAgo,
  capitalizeAndSpace,
  capitalizeFirstLetter,
  captializedArray,
  isValidEmail,
  storeData,
  getData,
  removeData,
  getNameSign,
  formatOrderId,
  getStartDateForCart,
  getBase64Path,
  decodeToken,
};
