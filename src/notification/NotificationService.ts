import messaging from '@react-native-firebase/messaging';
import * as Sentry from '@sentry/react-native';
import { ForegroundNotification } from './ForegroundNotification';
import { Platform } from 'react-native';
import { store } from '../store';
import { fetchOrders } from '../store/reducers/orders/orders';

export const APP_CHANNEL = 'TiffinVendors';

export const LOCAL_NOTIFICATION_CHANNEL_ID = 'TIFFINFINDS_CHANNEL_V1';
export const LOCAL_NOTIFICATION_CHANNEL_NAME = 'TiffinFinds';
export const LOCATION_NOTIFICATION_IMPORTANCE = 4;
export const LOCATION_NOTIFICATION_DESCRIPTION =
  'Used for showing notification while the app is open';

export const AUTHERIZATION_NOT_DETERMINED = -1;
export const AUTHERIZATION_DENIED = 0;
export const AUTHERIZATION_GRANTED = 1;
export const AUTHERIZATION_PROVISIONAL = 2;

export const NotificationService = () => {};

export const checkNotificationPermission = () => {
  return new Promise((resolved, rejected) => {
    messaging()
      .hasPermission()
      .then(authStatus => {
        resolved(authStatus);
      })
      .catch(error => {
        rejected(error);
        Sentry.captureException(error);
      });
  });
};

export const requestNotificationPermission = () => {
  return new Promise((resolved, rejected) => {
    messaging()
      .requestPermission()
      .then(authStatus => {
        resolved(authStatus);
      })
      .catch(error => {
        rejected(error);
        Sentry.captureException(error);
      });
  });
};

export const subscribe = (topic: any) => {
  console.log('--- SUBSCRIBING CHANNEL --- ', topic);
  messaging()
    .subscribeToTopic(topic)
    .then(() => {
      console.log('---- SUBSCRIBED TO TOPIC ----');
    })
    .catch(error => {
      Sentry.captureException(error);
      console.log('---- ERROR WHILE SUBSCRIBING TO TOPIC ----');
    });
};

export const unsubscribe = (topic: any) => {
  return new Promise((resolved, rejected) => {
    messaging()
      .unsubscribeFromTopic(topic?.toString())
      .then(() => {
        console.log('--- UNSUBSCRIBED FROM TOPIC ---');
        resolved(true);
      })
      .catch(error => {
        Sentry.captureException(error);
        console.log('---- ERROR WHILE UNSUBSCRIBING FROM TOPIC ----');
        rejected(error);
      });
  });
};

export const createNotificationChannel = (
  channelId = LOCAL_NOTIFICATION_CHANNEL_ID,
  channelName = LOCAL_NOTIFICATION_CHANNEL_NAME,
  description = LOCATION_NOTIFICATION_DESCRIPTION,
  importance = LOCATION_NOTIFICATION_IMPORTANCE,
) => {
  if (Platform.OS === 'android') {
    ForegroundNotification.createNotificationChannel(
      channelId,
      channelName,
      importance,
      description,
    );
  }
};

export const showNotification = (
  notification: any,
  channelId = LOCAL_NOTIFICATION_CHANNEL_ID,
) => {
  ForegroundNotification.showNotification(channelId, notification);
};

export const onMessageClicked = (callback: any) => {
  ForegroundNotification.onMessageClicked(callback);
};

export const getInitialNotification = () => {
  return ForegroundNotification.getInitialNotification();
};

//This method is called from the index.js file.
export const notificationBackgroundHandler = () => {
  messaging().setBackgroundMessageHandler(async remoteMessage => {
    console.log(
      '--- notificationBackgroundHandler --- ',
      JSON.stringify(remoteMessage),
    );
  });
};

export const notificationForegroundHandler = (token: string) => (
  dispatch: any,
) => {
  return messaging().onMessage(async remoteMessage => {
    console.log(remoteMessage);
    dispatch(fetchOrders(token));

    /* if (data.screen === 'Order') {
      //dispatch(notificationForOrderUpdate(data));
    } else if (data.screen === 'Review') {
      //dispatch(reviewForOrderCompletion(data, navigation));
    } */

    showNotification(JSON.stringify(remoteMessage));
  });
};

export const onNotificationClicked = () => async (
  dispatch: typeof store.dispatch,
) => {
  messaging().onNotificationOpenedApp(remoteMessage => {
    const data = remoteMessage.data;
    if (data && data.screen === 'Order') {
      //dispatch(notificationForOrderScreenClicked(data));
    } /* else if (data.screen === 'Review') {
      dispatch(reviewForOrderCompletion(data, navigation));
    } */
  });
};
