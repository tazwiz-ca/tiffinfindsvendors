import { NativeEventEmitter, NativeModules, Platform } from 'react-native';
const { RnNotification } = NativeModules;

export const ForegroundNotification = {
  createNotificationChannel: (
    channelId: string,
    channelName: string,
    importance: string,
    description: string,
  ) => {
    RnNotification.createNotificationChannel(
      channelId,
      channelName,
      importance,
      description,
    );
  },

  requestNotificationPermission: () => {
    return RnNotification.requestNotificationPermission();
  },

  showNotification: (channelId: string, notification: any) => {
    Platform.OS === 'android'
      ? RnNotification.showNotification(channelId, notification)
      : RnNotification.showNotification(channelId, notification, '');
  },

  onMessageClicked: (callback: Function) => {
    Platform.OS === 'android' && RnNotification.onMessageClicked();
    const eventEmitter = new NativeEventEmitter(NativeModules.RnNotification);

    return eventEmitter.addListener('NotificationInteraction', messageData => {
      callback(messageData);
    });
  },

  getInitialNotification: () => {
    return Platform.OS === 'android'
      ? RnNotification.getInitialNotification()
      : new Promise((resolve, reject) => {});
  },
};
