import React, { useEffect, useState } from 'react';
import { View, StyleSheet, FlatList, Modal } from 'react-native';
import * as Animatable from 'react-native-animatable';

import colors from '../../configs/colors';
import RNTextView from '../../components/common/RNTextView';
import CustomButton from '../../components/common/CustomButton';

type Menu = {
  visibility: boolean;
  days?: { [key: string]: string };
  onCloseClicked: Function;
};
type MenuItem = {
  day: string;
  menu: string;
};

const ViewMenu: React.FunctionComponent<Menu> = ({
  visibility,
  onCloseClicked,
  days,
}) => {
  const [animation, setAnimation] = useState('slideInUp');
  const [menuItems, setMenuItems] = useState<Array<MenuItem>>();

  useEffect(() => {
    if (!menuItems || menuItems.length === 0) {
      getMenuItems();
    }
  });

  const getMenuItems = () => {
    const tempMenuItems: Array<MenuItem> = [];
    if (!days) return;
    Object.keys(days).forEach((key: string) => {
      let menu: MenuItem = {
        day: key,
        menu: days[key],
      };

      tempMenuItems.push(menu);
    });

    setMenuItems(tempMenuItems);
  };

  const onCloseHandle = () => {
    setAnimation('slideOutDown');
    setTimeout(() => {
      onCloseClicked();
      setAnimation('slideInUp');
    }, 100);
  };

  const MenuItemView = ({ day, menu }: MenuItem) => (
    <View style={styles.menuItem}>
      <RNTextView style={styles.menuTextKey}>{day}</RNTextView>
      <RNTextView>-</RNTextView>
      <RNTextView style={styles.menuTextValue}>{menu}</RNTextView>
    </View>
  );

  return (
    <Modal visible={visibility} transparent>
      <View style={styles.overlay}>
        <Animatable.View
          style={styles.container}
          animation={animation}
          duration={500}>
          <RNTextView style={styles.title} textType={'bold'}>
            Menu
          </RNTextView>
          <FlatList
            data={menuItems}
            renderItem={({ item }) => (
              <MenuItemView day={item.day} menu={item.menu} />
            )}
            keyExtractor={(_, index) => index.toString()}
          />
          <CustomButton
            customStyles={styles.closeButton}
            title={'Close'}
            onPress={onCloseHandle}
          />
        </Animatable.View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  overlay: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  container: {
    backgroundColor: colors.white,
    padding: 20,
    borderTopStartRadius: 16,
    borderTopEndRadius: 16,
  },
  title: {
    fontSize: 24,
    marginVertical: 12,
    textAlign: 'center',
    fontFamily: 'JosefinSans-Bold',
    fontWeight: 'bold',
  },
  menuItem: {
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: 16,
    marginVertical: 6,
    paddingVertical: 6,
  },
  menuTextKey: {
    width: 110,
    paddingHorizontal: 8,
    fontSize: 18,
    fontWeight: 'bold',
    fontFamily: 'Apercu-Bold',
  },
  menuTextValue: {
    marginLeft: 8,
    fontSize: 16,
    fontFamily: 'Apercu-Light',
  },
  closeButton: {
    marginVertical: 16,
  },
});

export default ViewMenu;
