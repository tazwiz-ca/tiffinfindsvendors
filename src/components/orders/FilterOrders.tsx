import React, { FunctionComponent } from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  TouchableWithoutFeedback,
} from 'react-native';
import Modal from 'react-native-modal';
import { useDispatch } from 'react-redux';

import colors from '../../configs/colors';
import {
  filterOrderByStatus,
  resetOrderStatus,
} from '../../store/reducers/orders/orders';
import RNTextView from '../common/RNTextView';

interface Props {
  modalVisible: boolean;
  onClose: () => void;
}

const FilterOrders: FunctionComponent<Props> = ({ modalVisible, onClose }) => {
  const data = [
    { text: 'Pending' },
    { text: 'Active' },
    { text: 'Declined' },
    { text: 'Completed' },
    { text: 'Cancelled' },
    { text: 'Reset' },
  ];
  const dispatch = useDispatch();

  const handlePress = (item: { text: string }) => {
    onClose();
    if (item.text === 'Reset') {
      dispatch({
        type: resetOrderStatus.type,
      });
    } else
      dispatch({
        type: filterOrderByStatus.type,
        payload: { status: item.text },
      });
  };

  type FilterProps = {
    text: string;
    onPress: () => void;
  };

  const FilterComp = ({ text, onPress }: FilterProps) => (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={[styles.textWrapper]}>
        <RNTextView style={styles.text}>{text}</RNTextView>
      </View>
    </TouchableWithoutFeedback>
  );

  return (
    <>
      <Modal
        isVisible={modalVisible}
        onBackdropPress={onClose}
        style={styles.modalContainer}>
        <View style={styles.container}>
          <FlatList
            data={data}
            renderItem={({ item }: any) => (
              <FilterComp text={item.text} onPress={() => handlePress(item)} />
            )}
            keyExtractor={(_, index) => index.toString()}
          />
        </View>
        <View style={styles.footer}>
          <FilterComp text="Cancel" onPress={onClose} />
        </View>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    justifyContent: 'flex-end',
  },
  container: {
    backgroundColor: colors.white,
    justifyContent: 'flex-end',
    borderRadius: 8,
    marginBottom: 10,
  },
  textWrapper: {
    alignItems: 'center',
    borderBottomColor: colors.lightGrayColor,
    borderBottomWidth: 1,
    padding: 20,
  },
  text: {
    fontFamily: 'Apercu-Regular',
    fontSize: 16,
    color: colors.textInputBorder,
  },
  footer: {
    marginVertical: 5,
    backgroundColor: colors.white,
    borderRadius: 8,
  },
});

export default FilterOrders;
