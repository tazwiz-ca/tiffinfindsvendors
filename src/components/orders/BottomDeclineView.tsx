import React, { useState } from 'react';
import { View, StyleSheet, Modal } from 'react-native';
import * as Animatable from 'react-native-animatable';

import colors from '../../configs/colors';

import RNTextView from '../common/RNTextView';
import CustomButton from '../common/CustomButton';
import { TextInput } from 'react-native-gesture-handler';

type Props = {
  visibility: boolean;
  onOKHandle: Function;
  onCancel: () => void;
};
const BottomDeclineView: React.FunctionComponent<Props> = (props: Props) => {
  const [animation, setAnimation] = useState('slideInUp');

  const [reason, setReason] = useState('');

  const onOKHandle = () => {
    setAnimation('slideOutDown');
    setTimeout(() => {
      props.onOKHandle(reason);
      setAnimation('slideInUp');
    }, 300);
  };
  return (
    <Modal visible={props.visibility} transparent>
      <View style={styles.overlay}>
        <Animatable.View
          style={styles.container}
          animation={animation}
          duration={500}>
          <RNTextView style={styles.title}>Why declining?</RNTextView>
          <TextInput
            style={styles.reasonText}
            placeholder={'Reason for decline?'}
            onChangeText={text => {
              setReason(text);
            }}
          />
          <CustomButton
            title={'OK'}
            enabledColor="#B90039"
            onPress={onOKHandle}
          />
          <CustomButton
            title={'Cancel'}
            onPress={props.onCancel}
            customStyles={styles.btnOutline}
            customTextStyles={styles.customTextStyles}
          />
        </Animatable.View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  overlay: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
  container: {
    backgroundColor: colors.white,
    padding: 20,
  },
  title: {
    fontFamily: 'Apercu-Bold',
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },
  reasonText: {
    marginVertical: 20,
    borderColor: colors.textInputBorder,
    borderWidth: 1,
    borderRadius: 8,
    paddingHorizontal: 8,
    paddingVertical: 10,
  },
  btnOutline: {
    borderColor: colors.headerCreateBtnOutline,
    backgroundColor: colors.white,
    textAlign: 'center',
    fontSize: 16,
  },
  customTextStyles: {
    color: colors.textInputBorder,
    opacity: 0.4,
  },
});

export default BottomDeclineView;
