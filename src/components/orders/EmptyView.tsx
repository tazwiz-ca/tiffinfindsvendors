import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import colors from '../../configs/colors';
import RNTextView from '../common/RNTextView';

const IC_EMPTY = require('../../assets/img/ic_empty_order.png');

const EmptyView = () => {
  return (
    <View style={styles.container}>
      <Image source={IC_EMPTY} style={styles.image} />
      <RNTextView style={styles.slogan} textType={'medium'}>
        No Orders yet
      </RNTextView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 100,
  },
  image: {
    width: 200,
    height: 200,
  },
  slogan: {
    fontFamily: 'Apercu-Regular',
    fontSize: 20,
    paddingVertical: 10,
    color: colors.textInputBorder,
    opacity: 0.7,
  },
});

export default EmptyView;
