import React from 'react';
import { FunctionComponent } from 'react';
import { useState } from 'react';
import { View, StyleSheet, TextInput } from 'react-native';

interface OtpProps {
  onValidCount: (arg0: boolean, arg1: Array<number>) => void;
  style: object;
}
const OTP: FunctionComponent<OtpProps> = ({ onValidCount, style }) => {
  const pinCount = 4;
  const refArray = new Array(pinCount).fill(React.createRef());
  const [pin, setPin] = useState(new Array(pinCount).fill(-1));

  const validatePin = () => {
    let isValid = true;
    pin.map(number => {
      if (number == -1) {
        isValid = false;
        return;
      }
    });

    return isValid;
  };

  const onTextChange = (index: number, text: string) => {
    if (text) {
      /* if (isNaN(text)) {
        return;
      } */
      pin[index] = text;
      setPin(pin);
      const isValidPin = validatePin();
      const temp = index + 1;
      if (temp < pinCount) {
        refArray[temp].focus();
      }
      onValidCount(isValidPin, pin);
      return text;
    }
  };

  const onKeyPressed = (index: number, key: string) => {
    if (key === 'Backspace') {
      if (index >= 0) {
        pin[index] = -1;
        console.log(pin);
        setPin(pin);
        if (index > 0) {
          refArray[index - 1].focus();
        }

        onValidCount(false, []);
      }
    }
  };

  const generateTI = (index: number) => {
    return (
      <View key={index}>
        <TextInput
          //focus only on the first textinput
          autoFocus={index === 0}
          key={index}
          ref={id => {
            refArray[index] = id;
          }}
          style={styles.pinBox}
          secureTextEntry={false}
          keyboardType="number-pad"
          keyboardAppearance="default"
          textContentType="oneTimeCode"
          placeholder={''}
          maxLength={1}
          numberOfLines={1}
          caretHidden={true}
          returnKeyType="done"
          onChangeText={text => {
            console.log('--- CHANGE ---');
            onTextChange(index, text);
          }}
          onKeyPress={event => {
            console.log('--- PRESSED ---' + event.nativeEvent.key);
            if (event.nativeEvent.key === '.') {
              console.log('--- PRESSED INSIDE ---');
              event.preventDefault();
              return false;
            }
            onKeyPressed(index, event.nativeEvent.key);
          }}></TextInput>
      </View>
    );
  };

  const generatePinView = () => {
    return refArray.map((row, index) => generateTI(index));
  };

  return <View style={[styles.container, style]}>{generatePinView()}</View>;
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
  },
  pinBox: {
    width: 50,
    height: 50,
    fontWeight: '500',
    fontSize: 24,
    borderRadius: 4,
    backgroundColor: '#E5F7F0',
    marginEnd: 8,
    textAlign: 'center',
  },
});

export default OTP;
