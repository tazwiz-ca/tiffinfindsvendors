import React, { useState, useEffect, useRef } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

import commons from '../../configs/commons';

interface Props {
  onSearch: (arg0: string, arg1: any) => void;
  data?: any;
  address?: any;
  disableMap?: boolean;
}

export default function CustomLocationSearch({
  onSearch,
  data,
  address,
  disableMap = false,
}: Props) {
  const locationRef = useRef<any>();
  const [latLng, setLatLng] = useState({
    latitude: data && data.log ? data.log : 37.78825,
    longitude: data && data.lat ? data.lat : -122.4324,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });

  useEffect(() => {
    if (address) {
      setLatLng({
        latitude:
          address && address.coordinates
            ? address.coordinates.coordinates[1]
            : 37.78825,
        longitude:
          address && address.coordinates
            ? address.coordinates.coordinates[0]
            : -122.4324,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      });
      if (address && address.fullAddress)
        locationRef?.current?.setAddressText(address.fullAddress);
    }
  }, [address]);

  const handleSearchSelection = (fullAddress: string, geometricalLoc: any) => {
    onSearch(fullAddress, geometricalLoc);
  };

  const getFullAddress = (addressString: string): any => {
    if (!addressString) return {};
    var parts = addressString.split(',');
    let province = '',
      city = '',
      address = [];
    for (var i = parts.length - 1; i >= 0; i--) {
      if (i === parts.length - 2) {
        province = parts[i];
      } else if (i === parts.length - 3) {
        city = parts[i];
      } else if (i < parts.length - 3) {
        address.push(parts[i]);
      } else {
        address.push(addressString);
      }
    }
    let addressStr = address.reverse().join(', ');
    return {
      province: province ? province.trim() : '',
      city: city ? city.trim() : '',
      address: addressStr ? addressStr.trim() : '',
      fullAddress: addressString ? addressString.trim() : '',
      postalCode: '',
    };
  };

  type SelectSuggestProps = {
    address_components?: any;
    geometry?: any;
    description?: string;
  };

  const handleSelectSuggest = ({
    address_components,
    geometry,
    description,
  }: SelectSuggestProps): string => {
    if (
      !address_components ||
      !geometry ||
      !geometry.location ||
      !description
    ) {
      return '';
    }

    let codeDetails = address_components.filter(
      (x: any) => x.types && x.types.indexOf('postal_code') !== -1,
    );
    let fullAddress = getFullAddress(description);
    if (codeDetails.length > 0) {
      fullAddress.postalCode = codeDetails[0].long_name;
    }
    fullAddress.coordinates = {
      coordinates: [geometry.location.lng, geometry.location.lat],
    };

    return fullAddress;
  };
  return (
    <View>
      <View style={styles.container}>
        <GooglePlacesAutocomplete
          ref={locationRef}
          minLength={2}
          placeholder="Search location"
          listViewDisplayed={'auto'}
          //returnKeyType={'default'}
          fetchDetails={true}
          onPress={(data, details = null) => {
            setLatLng({
              latitude: details?.geometry?.location?.lat
                ? details.geometry.location.lat
                : latLng.latitude,
              longitude: details?.geometry?.location?.lng
                ? details.geometry.location.lng
                : latLng.longitude,
              latitudeDelta: 0.001,
              longitudeDelta: 0.001,
            });
            let params = { ...data, ...details };
            handleSearchSelection(handleSelectSuggest(params), {
              latitude: details?.geometry?.location?.lat
                ? details.geometry.location.lat
                : latLng.latitude,
              longitude: details?.geometry?.location?.lng
                ? details.geometry.location.lng
                : latLng.longitude,
              latitudeDelta: 0.001,
              longitudeDelta: 0.001,
            });
          }}
          textInputProps={{
            onChangeText: text => {
              if (text && text.length === 0) handleSearchSelection(text, null);
            },
          }}
          enablePoweredByContainer={false}
          //value={locationTxt}
          query={{
            key: commons.placesApiKey,
            language: 'en',
            components: 'country:ca',
            types: 'address',
          }}
          styles={{
            textInputContainer: styles.textInputContainer,
            textInput: styles.textInput,
          }}
        />
        <Image
          style={styles.img}
          width={20}
          height={20}
          source={require('../../assets/img/map-pin.png')}
        />
      </View>
      {!disableMap && (
        <MapView
          provider={PROVIDER_GOOGLE}
          style={styles.mapStyle}
          initialRegion={latLng}
          mapType="standard"
          showsMyLocationButton={true}
          showsCompass={true}
          //showsMyLocationButton={true}
          //chacheEnabled={false}
          zoomEnabled={true}
          region={latLng}>
          <Marker
            coordinate={{
              latitude: latLng.latitude,
              longitude: latLng.longitude,
            }}
          />
        </MapView>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    borderColor: 'rgba(84, 101, 233, 0.2)',
    borderWidth: 1,
    borderRadius: 4,
    marginVertical: 15,
    flexDirection: 'row',
    alignItems: 'center',
  },
  textInputContainer: {
    backgroundColor: 'transparent',
    borderTopColor: 'transparent',
    borderBottomColor: 'transparent',
    borderTopWidth: 0,
    borderBottomWidth: 0,
  },
  textInput: {
    padding: 10,
    flexGrow: 1,
  },
  img: {
    marginRight: 10,
  },
  mapStyle: {
    height: 300,
    backgroundColor: '#000',
  },
});
