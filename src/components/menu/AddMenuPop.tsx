import { useRoute } from '@react-navigation/core';
import React, { useEffect, useState } from 'react';
import * as Sentry from '@sentry/react-native';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  SafeAreaView,
  FlatList,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useDispatch, useSelector } from 'react-redux';

import colors from '../../configs/colors';
import CustomButton from '../common/CustomButton';
import FormTextInput from './FormTextInput';
import {
  updateMealOfferings,
  updateMenuDays,
} from '../../store/reducers/menus/menus';
import { RootState } from '../../store';
import BottomSlideMenu from '../common/BottomSlideMenu';
import AppErrorText from '../common/AppErrorText';

type MenusProps = {
  offering: string;
};

type RouteProps = {
  openDialog?: boolean;
  popupTitle?: string;
  Component?: any;
  props: MenusProps;
};

const AddMenuPop: React.FunctionComponent = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const dispatch = useDispatch();
  const [showModal, setShowModal] = useState(false);
  const [isDisabled, setIsDisabled] = useState(true);
  const [errorText, setErrorText] = useState('');

  if (!route || !route.params) return <></>;

  const { popupTitle, Component, props } = route.params as RouteProps;

  const mealOffering = useSelector(
    (state: RootState) => state.menus,
  ).mealOfferings.find(item => item.text === props.offering)!;

  {
    /** Validation for enabling Add Menu button */
  }
  useEffect(() => {
    try {
      //Check if atleast one meal frequency is added
      if (mealOffering.frequency.filter(item => item.selected).length === 0) {
        setErrorText('Atleast one meal frequency has to be selected');
        return setIsDisabled(true);
      }
      const selectedFreqs = mealOffering.frequency.filter(
        item => item.selected,
      );
      // Check if any of the price is 0
      if (selectedFreqs.filter(item => item.price == 0).length > 0) {
        setErrorText('Price cannot be zero');
        return setIsDisabled(true);
      }
      //Check if the menu days are empty
      if (mealOffering.dayMenus.filter(item => item.menu !== '').length === 0) {
        setErrorText('Atleast one menu days has to be filled');
        return setIsDisabled(true);
      }
      setIsDisabled(false);
      setErrorText('');
    } catch (error) {
      console.log(error);
      Sentry.captureException(error);
    }
  }, [mealOffering]);

  const onClose = (): void => {
    if (popupTitle === 'Update Menu') {
      return navigation.goBack();
    }
    setShowModal(true);
  };

  //arg1 --> text && arg2 --> name (menu Day Monday/tuesday,etc)
  const onMenuDaysChange = (text: string, name: string): void => {
    dispatch({
      type: updateMenuDays.type,
      payload: { text, name, offering: props.offering },
    });
  };

  const handleAddMenus = (): void => {
    navigation.goBack();
  };

  const handleDontDiscard = () => {
    setShowModal(false);
  };

  const handleDiscard = (): void => {
    //If discarding a menu, update the selected bool as false, to uncheck the checkbox in menu info screen
    dispatch({
      type: updateMealOfferings.type,
      payload: { text: props.offering },
    });
    navigation.goBack();
    setShowModal(false);
  };

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{ backgroundColor: colors.white }} />
      <BottomSlideMenu
        visibility={showModal}
        title="Are you sure you want to Discard this menu?"
        onSuccess={handleDontDiscard}
        onCancel={handleDiscard}
      />
      <View style={styles.container}>
        <KeyboardAwareScrollView nestedScrollEnabled={true}>
          <View style={styles.modalContainer}>
            <Text style={styles.titleStyle}>{popupTitle}</Text>
            <View style={styles.contentContainer}>
              <Component {...props} />
            </View>
            <FlatList
              nestedScrollEnabled={true}
              data={mealOffering.dayMenus}
              renderItem={({ item }) => (
                <FormTextInput
                  value={item.menu}
                  textTitle={`${item.text} Menu`}
                  onChange={text => onMenuDaysChange(text, item.text)}
                />
              )}
              keyExtractor={(_, index) => index.toString()}
            />
          </View>
        </KeyboardAwareScrollView>
      </View>
      <View style={styles.footerContainer}>
        <View style={styles.footerBtn}>
          {/** Render error messages if the button is disabled */}
          {errorText ? (
            <View style={styles.errorContainer}>
              <AppErrorText>{errorText}</AppErrorText>
            </View>
          ) : (
            <></>
          )}
          <CustomButton
            title={popupTitle}
            disabled={isDisabled}
            enabledColor="#00C476"
            onPress={handleAddMenus}
          />
          <CustomButton
            title="Cancel"
            disabled={false}
            customStyles={[styles.btnOutline, { paddingBottom: 0 }]}
            onPress={onClose}
            customTextStyles={styles.customTextStyles}
          />
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  modalContainer: {
    padding: 20,
  },
  contentContainer: {
    fontFamily: 'Apercu-Regular',
    paddingBottom: 24,
  },
  titleCloseBtn: {
    width: 22,
    height: 22,
  },
  titleStyle: {
    fontSize: 24,
    fontFamily: 'JosefinSans-Bold',
    textAlign: 'center',
  },
  titleContainer: {
    margin: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  footerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingVertical: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
  },
  btnOutline: {
    borderColor: 'rgba(30, 33, 76, .4)',
    backgroundColor: colors.white,
    textAlign: 'center',
    fontSize: 16,
  },
  customTextStyles: {
    color: colors.textInputBorder,
    opacity: 0.4,
  },
  footerBtn: {
    flex: 1,
    marginHorizontal: 20,
  },
  errorContainer: {
    alignSelf: 'center',
    marginBottom: 10,
  },
});

export default AddMenuPop;
