import React from 'react';
import { StyleSheet, Image } from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

import RNTextView from '../common/RNTextView';

interface Props {
  selected: boolean;
  onPress: (arg0: string) => void;
  style?: object;
  textStyle?: object;
  size?: number;
  text?: string;
}

const CheckBox: React.FunctionComponent<Props> = ({
  selected,
  onPress,
  style,
  textStyle,
  size = 30,
  text = '',
}) => (
  <TouchableWithoutFeedback
    style={[styles.checkBox, style]}
    onPress={() => onPress(text)}>
    <Image
      source={
        selected
          ? require('../../assets/img/checked.png')
          : require('../../assets/img/unchecked.png')
      }
      style={{ width: size / 2, height: size / 2 }}
    />
    <RNTextView style={textStyle}> {text} </RNTextView>
  </TouchableWithoutFeedback>
);

const styles = StyleSheet.create({
  checkBox: {
    marginTop: 8,
  },
});

export default CheckBox;
