import React, { useEffect, useState } from 'react';
import { FlatList, View, StyleSheet } from 'react-native';
import AppErrorText from '../../components/common/AppErrorText';

import RNTextView from '../../components/common/RNTextView';
import CheckBox from '../../components/menu/CheckBox';
import colors from '../../configs/colors';

type DataProp = {
  text: string;
  selected: boolean;
};
interface Props {
  titleText: string;
  data: Array<DataProp>;
  onChange: (arg0: string) => void;
  isRequired?: boolean;
  containerStyle?: object;
}

const MealOptions: React.FunctionComponent<Props> = ({
  titleText,
  data,
  onChange,
  isRequired,
  containerStyle,
}) => {
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    if (!isRequired) return;
    let isSelected = false;
    data.map(item => {
      item.selected ? (isSelected = true) : '';
    });
    setIsError(!isSelected);
  }, [data]);

  return (
    <View style={[{ marginBottom: 15 }, containerStyle]}>
      <RNTextView style={styles.title}>
        {titleText}
        {isRequired && <RNTextView style={{ color: 'red' }}>*</RNTextView>}
      </RNTextView>
      <FlatList
        data={data}
        nestedScrollEnabled={true}
        showsHorizontalScrollIndicator={false}
        numColumns={2}
        contentContainerStyle={{ justifyContent: 'center' }}
        columnWrapperStyle={styles.rowStyle}
        renderItem={({ item }) => (
          <View style={{ flex: 1, marginVertical: 5 }}>
            <CheckBox
              selected={item.selected}
              onPress={() => onChange(item.text)}
              text={item.text}
              style={{ flexDirection: 'row' }}
              textStyle={styles.checkboxTxt}
              size={35}
            />
          </View>
        )}
        keyExtractor={(_, index) => index.toString()}
      />
      {isError && (
        <AppErrorText>Please Select atleast one checkbox</AppErrorText>
      )}
    </View>
  );
};

export default MealOptions;

const styles = StyleSheet.create({
  title: {
    marginTop: 15,
    fontFamily: 'JosefinSans-SemiBold',
    fontSize: 18,
    color: colors.textInputBorder,
  },
  rowStyle: {
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  checkboxTxt: {
    fontFamily: 'Apercu-Regular',
    fontSize: 16,
    color: colors.textInputBorder,
    opacity: 0.8,
  },
});
