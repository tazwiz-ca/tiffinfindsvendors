import React from 'react';
import { View, StyleSheet, TextInput } from 'react-native';

import colors from '../../configs/colors';
import RNTextView from '../common/RNTextView';

interface Props {
  title: string;
  value: any;
  isRequired?: boolean;
  onChange: (arg0: string) => void;
  customTitleStyle?: object;
  containerStyle?: object;
}

const MealPrice: React.FunctionComponent<Props> = ({
  title,
  value,
  onChange,
  customTitleStyle,
  containerStyle,
  isRequired,
}) => {
  return (
    <View style={[styles.container, containerStyle]}>
      <RNTextView style={[styles.titleTxt, customTitleStyle]}>
        {title}
        {isRequired && <RNTextView style={{ color: 'red' }}>*</RNTextView>}
      </RNTextView>
      <TextInput
        maxLength={100}
        keyboardType="decimal-pad"
        style={styles.titleInput}
        value={value?.toString()}
        onChangeText={onChange}
        returnKeyType="done"
      />
    </View>
  );
};

export default MealPrice;

const styles = StyleSheet.create({
  container: {},
  titleTxt: {
    fontFamily: 'JosefinSans-SemiBold',
    fontSize: 14,
    color: colors.textInputBorder,
    marginTop: 12,
    textAlign: 'center',
  },
  titleInput: {
    width: 100,
    fontFamily: 'Apercu-Regular',
    borderColor: '#DDE0FB',
    borderWidth: 1,
    backgroundColor: '#F8F9FE',
    padding: 15,
    color: colors.textInputBorder,
  },
});
