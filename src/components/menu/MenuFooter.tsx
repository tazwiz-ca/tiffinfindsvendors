import React from 'react';
import { View, StyleSheet } from 'react-native';

import CustomButton from '../../components/common/CustomButton';
import colors from '../../configs/colors';
import AppErrorText from '../common/AppErrorText';

interface Props {
  firstBtnText?: string;
  handlePress?: () => void;
  goBack?: () => void;
  disabled?: boolean;
  errorText?: string;
}

const MenuFooter: React.FunctionComponent<Props> = ({
  firstBtnText,
  handlePress,
  goBack,
  disabled,
  errorText,
}) => {
  return (
    <View style={styles.footerContainer}>
      {/** Render error messages if the button is disabled */}
      {errorText ? (
        <View style={styles.errorContainer}>
          <AppErrorText>{errorText}</AppErrorText>
        </View>
      ) : (
        <></>
      )}
      <CustomButton
        disabled={disabled}
        title={firstBtnText ? firstBtnText : 'Next'}
        enabledColor={'#00C476'}
        onPress={handlePress ? handlePress : () => {}}
        customStyles={{ width: '90%' }}
      />
      {goBack && (
        <CustomButton
          title="Previous"
          disabled={false}
          customStyles={styles.btnOutline}
          onPress={goBack}
          customTextStyles={styles.customTextStyles}
        />
      )}
    </View>
  );
};

export default MenuFooter;

const styles = StyleSheet.create({
  footerContainer: {
    alignItems: 'center',
    backgroundColor: 'white',
    paddingVertical: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
  },
  btnOutline: {
    borderColor: colors.headerCreateBtnOutline,
    backgroundColor: colors.white,
    textAlign: 'center',
    fontSize: 16,
    paddingBottom: 10,
  },
  btnSpacing: {
    top: 30,
  },
  customTextStyles: {
    color: colors.textInputBorder,
    opacity: 0.4,
  },
  errorContainer: {
    alignSelf: 'center',
    marginBottom: 10,
    marginHorizontal: 20,
  },
});
