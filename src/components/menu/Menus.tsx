import React from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import colors from '../../configs/colors';
import { RootState } from '../../store';

import RNTextView from '../common/RNTextView';
import MealOptions from './MealOptions';
import MealPrice from './MealPrice';
import {
  updateMenuFrequency,
  updateMenuPrices,
} from '../../store/reducers/menus/menus';

interface Props {
  offering: string;
}

const Menus: React.FunctionComponent<Props> = ({ offering }) => {
  const dispatch = useDispatch();
  const mealOffering = useSelector(
    (state: RootState) => state.menus,
  ).mealOfferings.find(item => item.text === offering)!;

  const handleMealFreqChange = (text: string) => {
    dispatch({
      type: updateMenuFrequency.type,
      payload: { offering: mealOffering, text },
    });
  };

  //arg1 -- price && arg2 -- text
  const handleMealPriceChange = (text: string, name: string) => {
    dispatch({
      type: updateMenuPrices.type,
      payload: { offering: mealOffering, price: text, text: name },
    });
  };

  return (
    <View style={styles.container}>
      <MealOptions
        isRequired={true}
        titleText="Meal Frequency"
        data={mealOffering.frequency}
        onChange={handleMealFreqChange}
      />

      {mealOffering.frequency.filter(item => item.selected).length > 0 && (
        <RNTextView style={styles.titleTxt}>
          Meal Price<RNTextView style={{ color: 'red' }}>*</RNTextView>
        </RNTextView>
      )}
      <FlatList
        nestedScrollEnabled={true}
        data={mealOffering.frequency.filter(item => item.selected)}
        showsHorizontalScrollIndicator={false}
        numColumns={2}
        contentContainerStyle={{ justifyContent: 'center' }}
        columnWrapperStyle={styles.rowStyle}
        renderItem={({ item }) => (
          <MealPrice
            title={item.text}
            value={item?.price}
            onChange={text => handleMealPriceChange(text, item.text)}
          />
        )}
        keyExtractor={(_, index) => index.toString()}
      />
    </View>
  );
};

export default Menus;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
  },
  titleTxt: {
    fontFamily: 'JosefinSans-SemiBold',
    fontSize: 18,
    color: colors.textInputBorder,
    marginVertical: 10,
  },
  rowStyle: {
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
