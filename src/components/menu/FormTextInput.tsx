import React from 'react';
import { View, StyleSheet, TextInput } from 'react-native';

import colors from '../../configs/colors';
import AppErrorText from '../common/AppErrorText';
import RNTextView from '../common/RNTextView';

interface Props {
  value: string;
  textTitle: string;
  placeholder?: string;
  onChange: (arg0: string) => void;
  isRequired?: boolean;
  containerStyle?: object;
}

const FormTextInput: React.FunctionComponent<Props> = ({
  value,
  textTitle,
  placeholder,
  onChange,
  isRequired,
  containerStyle,
}) => {
  const handleInputChange = (text: string) => {
    onChange(text);
  };

  return (
    <View style={[styles.container, containerStyle]}>
      <RNTextView style={styles.title}>
        {textTitle}
        {isRequired ? (
          <RNTextView style={{ color: 'red' }}>*</RNTextView>
        ) : (
          <></>
        )}
      </RNTextView>
      <TextInput
        maxLength={100}
        style={styles.titleInput}
        value={value}
        placeholder={placeholder}
        onChangeText={handleInputChange}
      />
      {isRequired && value.length < 3 ? (
        <AppErrorText>This field is mandatory</AppErrorText>
      ) : (
        <></>
      )}
    </View>
  );
};

export default FormTextInput;

const styles = StyleSheet.create({
  container: {
    marginBottom: 10,
  },
  titleInput: {
    fontFamily: 'Apercu-Regular',
    borderColor: '#DDE0FB',
    borderWidth: 1,
    backgroundColor: '#F8F9FE',
    padding: 15,
    color: colors.textInputBorder,
  },
  title: {
    fontFamily: 'JosefinSans-SemiBold',
    fontSize: 18,
    color: colors.textInputBorder,
    marginBottom: 10,
  },
});
