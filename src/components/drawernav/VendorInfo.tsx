import React from 'react';
import { StyleSheet, View } from 'react-native';
import { useSelector } from 'react-redux';

import colors from '../../configs/colors';
import { RootState } from '../../store';
import RNTextView from '../common/RNTextView';

const VendorInfo = () => {
  const authData = useSelector((state: RootState) => state.auth);

  return (
    <View style={styles.container}>
      <View style={styles.vendorNameWrapper}>
        <RNTextView textType="bold" style={styles.name}>
          {authData.vendor.fullName.substring(0, 2).toUpperCase()}
        </RNTextView>
      </View>
      <View style={styles.contentWrapper}>
        <RNTextView style={styles.fullName} numberOfLines={1}>
          {authData.vendor.fullName}
        </RNTextView>
        <RNTextView style={styles.email} ellipsizeMode="tail">
          {authData.vendor.email}
        </RNTextView>
      </View>
      <View style={styles.divider} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    marginTop: 10,
  },
  vendorNameWrapper: {
    width: 70,
    height: 70,
    borderRadius: 35,
    backgroundColor: colors.textInputBorder,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 20,
  },
  name: {
    fontSize: 20,
    color: colors.white,
  },
  divider: {
    marginTop: 20,
    borderBottomWidth: 1,
    borderColor: colors.textInputBorder,
    opacity: 0.4,
  },
  contentWrapper: {
    paddingLeft: 20,
  },
  fullName: {
    marginTop: 20,
    fontSize: 24,
    color: colors.textInputBorder,
    opacity: 0.9,
    fontWeight: 'bold',
  },
  email: {
    marginTop: 5,
    fontSize: 16,
    color: colors.textInputBorder,
    opacity: 0.6,
    fontWeight: 'bold',
  },
});

export default VendorInfo;
