import React, { useState } from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';
import FastImage from 'react-native-fast-image';

import colors from '../../configs/colors';

interface Props {
  uri: any;
  customStyles: object;
  containerStyle?: object;
  resizeMode?: any;
}

export default function LoadingImage({
  uri,
  customStyles,
  containerStyle,
  resizeMode = 'cover',
}: Props) {
  const [isLoading, setIsLoading] = useState(true);

  const handleOnLoad = () => {
    setIsLoading(false);
  };

  return (
    <View
      style={[
        styles.container,
        !isLoading && { backgroundColor: colors.white },
        containerStyle,
      ]}>
      <FastImage
        resizeMode={resizeMode}
        source={uri}
        style={customStyles}
        onLoad={handleOnLoad}
      />
      <ActivityIndicator
        animating={isLoading}
        color={colors.primary}
        style={styles.activityIndicator}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(0,0,0,0.1)',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
  },
  activityIndicator: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
});
