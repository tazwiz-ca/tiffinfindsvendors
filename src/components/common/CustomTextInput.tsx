import React, { useState, FunctionComponent } from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  TouchableWithoutFeedback,
  KeyboardType,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import * as Animatable from 'react-native-animatable';

import colors from '../../configs/colors';
import AppErrorText from './AppErrorText';
import PasswordErrorTxt from './PasswordErrorTxt';

interface Props {
  onChangeText: (arg0: string) => void;
  customStyles?: object;
  touched: { [key: string]: boolean };
  errors: { [key: string]: string };
  name: string;
  onPress?: () => void;
  value?: string;
  placeholderTxt?: string;
  secureTextEntry?: boolean;
  autoCapitalize?: boolean;
  extraStyle?: object;
  placeholderTextColor?: string;
  keyboardType?: KeyboardType;
  onBlur?: () => void;
  onFocus?: () => void;
}

const CustomTextInput: FunctionComponent<Props> = ({
  onChangeText,
  customStyles,
  touched,
  errors,
  name,
  keyboardType,
  value,
  placeholderTxt,
  secureTextEntry,
  autoCapitalize,
  extraStyle,
}: Props) => {
  const [isLabelVisible, setIsLabelVisible] = useState(false);
  const [isHidePwd, setIsHidePwd] = useState(secureTextEntry);
  const [inputValue, setInputValue] = useState('');
  const [isPasswordFocussed, setIsPasswordFocussed] = useState(false);

  return (
    <View style={customStyles}>
      <View style={styles.container}>
        {isLabelVisible && (
          <Animatable.Text
            style={[
              styles.placeholder,
              touched[name] && errors[name] ? { color: 'red' } : {},
            ]}>
            {placeholderTxt}
          </Animatable.Text>
        )}
        <TextInput
          placeholder={placeholderTxt}
          returnKeyType={'done'}
          style={[
            styles.textInput,
            isLabelVisible && {
              borderColor:
                touched[name] && errors[name] ? 'red' : colors.enabledTxtInput,
            },
            extraStyle,
          ]}
          keyboardType={keyboardType}
          secureTextEntry={isHidePwd}
          autoCapitalize={!autoCapitalize ? 'none' : 'words'}
          onChangeText={(text: string) => {
            //If the text is typed move the placeholder to the top as label
            if (text && text !== '') setIsLabelVisible(true);
            else setIsLabelVisible(false);
            onChangeText && onChangeText(text);
            setInputValue(text);
            if (name && name === 'password') setIsPasswordFocussed(true);
            else setIsPasswordFocussed(false);
          }}
          value={value}
        />
      </View>
      {name === 'password' && (
        <TouchableWithoutFeedback onPress={() => setIsHidePwd(!isHidePwd)}>
          <FastImage
            resizeMode={FastImage.resizeMode.contain}
            source={
              !isHidePwd
                ? require('../../assets/img/eye.png')
                : require('../../assets/img/eye-off.png')
            }
            style={styles.hidePwd}
          />
        </TouchableWithoutFeedback>
      )}
      {touched && errors[name] && name !== 'password' && (
        <AppErrorText>{errors[name]}</AppErrorText>
      )}
      {(isPasswordFocussed || touched[name]) && name === 'password' && (
        <PasswordErrorTxt inputValue={inputValue} />
      )}
    </View>
  );
};

export default CustomTextInput;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  placeholder: {
    fontSize: 16,
    zIndex: 999,
    position: 'absolute',
    color: colors.enabledTxtInput,
    marginTop: 5,
    fontWeight: '500',
    left: 0,
    top: -10,
    backgroundColor: 'white',
  },
  textInput: {
    width: '100%',
    color: 'black',
    borderColor: 'rgba(30, 33, 76,0.1)',
    borderBottomWidth: 1,
    borderRadius: 4,
    paddingVertical: 15,
    paddingHorizontal: 3,
    fontSize: 16,
    marginBottom: 10,
    opacity: 0.8,
    marginTop: 10,
  },
  hidePwd: {
    position: 'absolute',
    right: 10,
    zIndex: 1,
    top: 15,
    width: 25,
    height: 25,
  },
});
