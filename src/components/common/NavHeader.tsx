import React, { useState } from 'react';
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native';
import MaterialCommIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import colors from '../../configs/colors';
import FilterOrders from '../orders/FilterOrders';
import Hamburger from './HamburgerIc';

const MARGIN_TOP = Platform.OS == 'android' ? 0 : 0;

interface Prop {
  title: string;
  isFilter?: boolean;
}

const NavHeader: React.FunctionComponent<Prop> = ({ title, isFilter }) => {
  const [isShowFilter, setShowFilter] = useState(false);
  return (
    <>
      <SafeAreaView style={styles.container} />

      <View style={styles.headerContainer}>
        <Hamburger />
        <Text style={[styles.headerTxt, !isFilter && { marginLeft: -30 }]}>
          {title}
        </Text>
        {isFilter ? (
          <TouchableWithoutFeedback onPress={() => setShowFilter(true)}>
            <MaterialCommIcons
              name="filter-variant"
              size={30}
              color={colors.white}
              style={{ marginRight: 10 }}
            />
          </TouchableWithoutFeedback>
        ) : (
          <Text />
        )}
      </View>
      <FilterOrders
        modalVisible={isShowFilter}
        onClose={() => setShowFilter(false)}
      />
    </>
  );
};

export default NavHeader;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.textInputBorder,
  },
  headerContainer: {
    backgroundColor: colors.textInputBorder,
    flexDirection: 'row',
    paddingVertical: 15,
    justifyContent: 'space-between',
    paddingStart: 10,
    alignItems: 'center',
    marginTop: MARGIN_TOP,
  },
  headerTxt: {
    color: colors.white,
    fontSize: 24,
    fontFamily: 'JosefinSans-Regular',
  },
  btnOutline: {
    borderColor: colors.headerCreateBtnOutline,
    backgroundColor: colors.white,
    textAlign: 'center',
    fontSize: 16,
    borderWidth: 1,
    top: 30,
  },
  btnSpacing: {
    top: 30,
  },
  customTextStyles: {
    color: colors.textInputBorder,
    opacity: 0.4,
  },
  popupText: {
    fontFamily: 'Apercu-Regular',
    fontSize: 16,
  },
  closeImg: {
    width: 24,
    height: 24,
  },
});
