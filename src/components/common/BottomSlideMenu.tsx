import React, { useState } from 'react';
import { View, StyleSheet, Modal } from 'react-native';
import * as Animatable from 'react-native-animatable';

import RNTextView from './RNTextView';
import CustomButton from './CustomButton';
import colors from '../../configs/colors';

interface Props {
  title: string;
  subtitle?: string;
  message?: string;
  visibility: boolean;
  onSuccess: () => void;
  onCancel: () => void;
  btn1Text?: string;
  btn2Text?: string;
}

const BottomSlideMenu: React.FunctionComponent<Props> = ({
  title,
  subtitle,
  visibility,
  onSuccess,
  onCancel,
  btn1Text,
  btn2Text,
}) => {
  const [animation, setAnimation] = useState('slideInUp');

  const onOKClick = () => {
    setAnimation('slideOutDown');
    setTimeout(() => {
      onSuccess && onSuccess();
      setAnimation('slideInUp');
    }, 300);
  };

  const handleDismiss = () => {
    setAnimation('slideOutDown');
    setTimeout(() => {
      onCancel && onCancel();
      setAnimation('slideInUp');
    }, 300);
  };
  return (
    <Modal visible={visibility} transparent>
      <View style={styles.overlay}>
        <Animatable.View
          style={styles.container}
          animation={animation}
          duration={500}>
          <RNTextView style={styles.title}>{title}</RNTextView>
          {subtitle && (
            <RNTextView style={styles.content}>{subtitle}</RNTextView>
          )}

          <CustomButton
            title={btn1Text ? btn1Text : 'Discard'}
            enabledColor="#B90039"
            customStyles={styles.okButton}
            onPress={handleDismiss}
          />
          <CustomButton
            title={btn2Text ? btn2Text : 'Dont Discard'}
            customStyles={styles.btnOutline}
            customTextStyles={styles.customTextStyles}
            onPress={onOKClick}
          />
        </Animatable.View>
      </View>
    </Modal>
  );
};

export default BottomSlideMenu;

const styles = StyleSheet.create({
  overlay: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
  container: {
    backgroundColor: colors.white,
    padding: 20,
  },
  title: {
    fontFamily: 'Apercu-Bold',
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },
  message: {
    color: 'rgba(0,0,0,0.6)',
    fontSize: 16,
    marginVertical: 20,
  },
  okButton: { marginTop: 20 },
  btnOutline: {
    borderColor: colors.headerCreateBtnOutline,
    backgroundColor: colors.white,
    textAlign: 'center',
    fontSize: 16,
  },
  customTextStyles: {
    color: colors.textInputBorder,
    opacity: 0.4,
  },
  content: {
    color: 'rgba(0,0,0,0.6)',
    fontSize: 16,
    marginTop: 10,
    textAlign: 'center',
  },
});
