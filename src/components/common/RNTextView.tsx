import React, { FunctionComponent } from 'react';
import { Text, Platform } from 'react-native';

const platform: string = Platform.OS;

type fontFamily = {
  fontFamily?: string;
  fontWeight?: string;
};
type fontWeight = {
  light: fontFamily;
  regular: fontFamily;
  medium: fontFamily;
  bold: fontFamily;
};
interface Font {
  [key: string]: fontWeight;
}

const fontConfig: Font = {
  android: {
    light: {
      fontFamily: 'SFProText-Light',
    },
    regular: {
      fontFamily: 'SFProText-Regular',
    },
    medium: {
      fontFamily: 'SFProText-Semibold',
    },
    bold: {
      fontFamily: 'SFProText-Heavy',
    },
  },
  ios: {
    light: {
      fontWeight: '300',
    },
    regular: {
      fontWeight: '400',
    },
    medium: {
      fontWeight: '500',
    },
    bold: {
      fontWeight: '700',
    },
  },
};

type textProps = {
  textType?: 'light' | 'regular' | 'bold' | 'medium';
  style?: object;
  numberOfLines?: number;
  props?: any;
  ellipsizeMode?: any;
};

const RNTextView: FunctionComponent<textProps> = ({
  children,
  style,
  textType,
  numberOfLines,
  ellipsizeMode,
  ...props
}) => {
  let textStyle = {};
  switch (textType) {
    case 'light':
      textStyle = fontConfig[platform].light;
      break;
    case 'medium':
      textStyle = fontConfig[platform].medium;
      break;
    case 'bold':
      textStyle = fontConfig[platform].bold;
      break;
    default:
      textStyle = fontConfig[platform].regular;
      break;
  }
  return (
    <Text
      {...props}
      style={[textStyle, style]}
      numberOfLines={numberOfLines}
      ellipsizeMode={ellipsizeMode}>
      {children}
    </Text>
  );
};

export default RNTextView;
