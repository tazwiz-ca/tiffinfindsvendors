import React, { FunctionComponent } from 'react';
import { StyleSheet, ImageBackground, Text } from 'react-native';

import colors from '../../configs/colors';

interface Props {
  text: string;
  focused: boolean;
}

const TabBarLabel: FunctionComponent<Props> = ({ text, focused }) => {
  return (
    <ImageBackground
      source={
        !focused
          ? require('../../assets/img/headerBgUnselected.png')
          : require('../../assets/img/headerBgImg.png')
      }
      resizeMode="contain"
      style={styles.headerIcImg}>
      <Text style={[styles.headerTxt, !focused && { color: '#9394A9' }]}>
        {text}
      </Text>
    </ImageBackground>
  );
};

export default TabBarLabel;

const styles = StyleSheet.create({
  headerIcImg: {
    width: 150,
    height: 40,
    marginBottom: -10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerTxt: {
    fontSize: 14,
    fontFamily: 'Apercu-Bold',
    color: colors.white,
  },
});
