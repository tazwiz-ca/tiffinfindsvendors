import React from 'react';
import { FunctionComponent } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import colors from '../../configs/colors';
import RNTextView from './RNTextView';

interface Props {
  text?: string;
  onPress?: () => void;
}

const FilterText: FunctionComponent<Props> = ({ text, onPress }) => {
  return text ? (
    <TouchableOpacity onPress={onPress} activeOpacity={0.8}>
      <View style={styles.container}>
        <RNTextView style={styles.text}>{text}</RNTextView>
        <FastImage
          source={require('../../assets/img/close-toasts.png')}
          style={styles.closeImg}
        />
      </View>
    </TouchableOpacity>
  ) : (
    <></>
  );
};
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    backgroundColor: '#D9D9D9',
    borderRadius: 20,
    alignSelf: 'flex-end',
    marginRight: 10,
    marginBottom: 10,
  },
  text: {
    fontSize: 16,
    fontFamily: 'Apercu-Regular',
    color: colors.black,
    marginRight: 10,
  },
  closeImg: {
    width: 12,
    height: 12,
  },
});

export default FilterText;
