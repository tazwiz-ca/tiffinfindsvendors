import React, { FunctionComponent } from 'react';

import {
  StyleSheet,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';

import colors from '../../configs/colors';

interface Props {
  title?: string;
  customStyles?: object;
  onPress: () => void;
  disabled?: boolean;
  enabledColor?: string;
  customTextStyles?: object;
  loaderEnabled?: boolean;
}

const CustomButton: FunctionComponent<Props> = ({
  title,
  customStyles,
  onPress,
  disabled,
  enabledColor,
  customTextStyles,
  loaderEnabled = false,
}: Props) => {
  return !disabled ? (
    <TouchableOpacity
      style={[
        styles.enabledBtn,
        customStyles,
        enabledColor ? { backgroundColor: enabledColor } : {},
      ]}
      activeOpacity={0.7}
      onPress={loaderEnabled ? () => {} : onPress}>
      {loaderEnabled ? (
        <ActivityIndicator size={'small'} color="#FFFFFF" />
      ) : (
        <Text style={[styles.enabledBtnTxt, customTextStyles]}>{title}</Text>
      )}
    </TouchableOpacity>
  ) : (
    <TouchableOpacity
      activeOpacity={1}
      style={[styles.disabledBtn, customStyles]}>
      <Text style={styles.disabledTxt}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  enabledBtn: {
    backgroundColor: colors.lightGreen,
    borderRadius: 4,
    marginBottom: 10,
    padding: 15,
  },
  disabledBtn: {
    backgroundColor: colors.disabledBtn,
    borderRadius: 4,
    marginBottom: 10,
    padding: 15,
  },
  disabledTxt: {
    textAlign: 'center',
    color: colors.white,
    fontSize: 16,
  },
  enabledBtnTxt: {
    textAlign: 'center',
    color: colors.white,
    fontSize: 14,
    fontWeight: '500',
    fontStyle: 'normal',
  },
});

export default CustomButton;
