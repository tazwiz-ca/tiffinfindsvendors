import React from 'react';
import { View, StyleSheet, TouchableWithoutFeedback, Text } from 'react-native';

import colors from '../../configs/colors';

interface Props {
  index: number;
  selectedIdx: number;
  text: string;
  onSelection: (arg0: number, arg1: string) => void;
}

const AppRadioButton: React.FunctionComponent<Props> = ({
  index,
  selectedIdx,
  text,
  onSelection,
}: Props) => {
  return (
    <TouchableWithoutFeedback
      onPress={() => onSelection && onSelection(index, text)}>
      <View style={styles.container}>
        <View
          style={[
            styles.outerCircle,
            {
              borderWidth: selectedIdx !== index ? 1 : 0,
              backgroundColor: selectedIdx === index ? '#00C476' : 'white',
            },
          ]}>
          {selectedIdx === index && <View style={styles.innerCircle} />}
        </View>
        <Text style={styles.text}>{text}</Text>
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginVertical: 8,
    alignItems: 'center',
  },
  outerCircle: {
    height: 18,
    width: 18,
    borderRadius: 9,
    borderColor: colors.radioBtnBorder,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.27,
    shadowRadius: 1,
    elevation: 2,
  },
  innerCircle: {
    height: 6,
    width: 6,
    borderRadius: 3,
    backgroundColor: 'white',
  },
  text: {
    fontFamily: 'Apercu-Regular',
    fontSize: 16,
    color: colors.textInputBorder,
    marginLeft: 8,
  },
});

export default AppRadioButton;
