import React, { Props } from 'react';
import { StyleSheet, Text } from 'react-native';
import colors from '../../configs/colors';

export default function AppErrorText(props: Props<any>) {
  return <Text style={styles.text}>{props.children}</Text>;
}
const styles = StyleSheet.create({
  text: {
    fontSize: 12,
    color: colors.textInputErr,
    marginBottom: 5,
  },
});
