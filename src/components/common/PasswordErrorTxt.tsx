import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import FastImage from 'react-native-fast-image';

interface Prop {
  inputValue: any;
}

export default function PasswordErrorTxt({ inputValue }: Prop) {
  const successImg = require('../../assets/img/success-img.png');
  const errorImg = require('../../assets/img/error.png');
  const isUpperCase = (string: string) => /(.*[A-Z].*)/.test(string);
  const isLowerCase = (string: string) => /(.*[a-z].*)/.test(string);
  return (
    <View>
      <Text style={styles.headerTxt}>Password must contain:</Text>

      <View style={{ flexDirection: 'row' }}>
        <FastImage
          source={inputValue.length >= 8 ? successImg : errorImg}
          style={styles.img}
        />
        <Text style={styles.subHeader}>At least 8 characters</Text>
      </View>
      <View style={{ flexDirection: 'row' }}>
        <FastImage
          source={
            inputValue.length > 0 && isUpperCase(inputValue)
              ? successImg
              : errorImg
          }
          style={styles.img}
        />
        <Text style={styles.subHeader}>At least 1 uppercase letter</Text>
      </View>
      <View style={{ flexDirection: 'row' }}>
        <FastImage
          source={
            inputValue.length > 0 && isLowerCase(inputValue)
              ? successImg
              : errorImg
          }
          style={styles.img}
        />
        <Text style={styles.subHeader}>At least 1 lowercase letter</Text>
      </View>
      <View style={{ flexDirection: 'row' }}>
        <FastImage
          source={
            inputValue.match(/^.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?].*$/)
              ? successImg
              : errorImg
          }
          style={styles.img}
        />
        <Text style={styles.subHeader}>
          At least 1 special character(#,%,&)
        </Text>
      </View>
      <View style={{ flexDirection: 'row' }}>
        <FastImage
          source={inputValue.match(/\d/) ? successImg : errorImg}
          style={styles.img}
        />
        <Text style={styles.subHeader}>At least 1 number (0-9)</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  headerTxt: {
    color: 'rgba(30, 33, 76, 0.6)',
  },
  subHeader: {
    color: 'rgba(30, 33, 76, 0.4)',
  },
  img: {
    width: 10,
    height: 10,
    marginTop: 3,
    marginRight: 3,
  },
});
