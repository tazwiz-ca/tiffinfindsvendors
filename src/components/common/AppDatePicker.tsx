import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import DatePicker from 'react-native-datepicker';
import * as Sentry from '@sentry/react-native';
import FastImage from 'react-native-fast-image';

import moment from 'moment';
import colors from '../../configs/colors';
import utils from '../../utils/helpers';

interface Props {
  label?: string;
  defaultDate?: string;
  customStyle?: object;
  onDateSelection?: (arg0: string) => void;
  selectedDay?: () => number;
  containerStyle?: object;
  isPastAllowed?: boolean;
  format?: string;
}

export default function AppDatePicker({
  label,
  defaultDate = '',
  customStyle,
  onDateSelection,
  selectedDay,
  containerStyle,
  isPastAllowed = true,
  format = 'Do MMM,YYYY',
}: Props) {
  const [date, setDate] = useState<any>();

  useEffect(() => {
    try {
      /** If there is a default date selected priorly format it with moment and set it to state */
      if (defaultDate) {
        if (moment(defaultDate, 'YYYYMMDD').isValid()) {
          setDate(moment(defaultDate));
          onDateSelection &&
            onDateSelection(moment(defaultDate).format(format));
        }
      }

      /**
       * Based on the selected day set the date accordingly
       */
      if (!selectedDay) return;
      if (selectedDay() === 1) {
        setDate(utils.getTomorowDate());
      } else if (selectedDay() === 7) {
        setDate(utils.getNextWeekDate());
      } else if (selectedDay() === 30) {
        setDate(utils.getNextMonthDate());
      }
    } catch (error) {
      Sentry.captureException(error);
    }
  }, [selectedDay]);

  const validateDateChange = (date: string) => {
    try {
      setDate(date);
      onDateSelection && onDateSelection(date);
    } catch (error) {
      Sentry.captureException(error);
    }
  };

  const DateIconComp = () => (
    <View style={styles.dateIcContainer}>
      <FastImage
        source={require('../../assets/img/calendar.png')}
        style={styles.calendarImg}
      />
    </View>
  );
  return (
    <View style={[styles.container, containerStyle]}>
      <Text style={styles.appTxt}>{label}</Text>
      <DatePicker
        style={[styles.datePicker, customStyle]}
        mode="date"
        placeholder="Select a date"
        minDate={!isPastAllowed ? moment().toDate() : undefined}
        date={date}
        //The max date should not be less than 13 years for stripe
        maxDate={moment().subtract('years', 13).toDate()}
        format={format}
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        iconComponent={<DateIconComp />}
        customStyles={{
          dateInput: styles.textInput,
          datePicker: {
            backgroundColor: '#d1d3d8',
            justifyContent: 'center',
          },
        }}
        onDateChange={validateDateChange}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginVertical: 15,
  },
  textInput: {
    fontFamily: 'Apercu-Light',
    fontSize: 14,
    padding: 10,
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
    alignItems: 'flex-start',
    color: colors.textInputBorder,
    borderColor: 'rgba(84, 101, 233, 0.2)',
    borderWidth: 1,
    borderRightWidth: 0,
  },
  dateIcContainer: {
    borderWidth: 1,
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
    padding: 9,
    borderColor: 'rgba(84, 101, 233, 0.2)',
  },
  dateIcon: {
    width: 20,
    height: 20,
    marginLeft: -1,
    borderColor: 'rgba(84, 101, 233, 0.2)',
  },
  calendarImg: {
    width: 20,
    height: 20,
  },
  appTxt: {
    fontFamily: 'Apercu-Regular',
    fontSize: 16,
    color: colors.enabledTxtInput,
    marginBottom: 10,
  },
  datePicker: {
    width: '100%',
    textAlign: 'left',
  },
});
