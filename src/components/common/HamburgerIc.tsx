import React from 'react';
import { TouchableWithoutFeedback, StyleSheet } from 'react-native';
import { DrawerActions, useNavigation } from '@react-navigation/core';
import FastImage from 'react-native-fast-image';

const Hamburger = () => {
  const navigation = useNavigation();
  return (
    <TouchableWithoutFeedback
      onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())}>
      <FastImage
        source={require('../../assets/img/menu.png')}
        style={styles.img}
      />
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  img: {
    width: 25,
    height: 25,
  },
});

export default Hamburger;
