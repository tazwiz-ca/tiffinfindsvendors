import React from 'react';
import { View, StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image';

import RNTextView from '../common/RNTextView';
import { FunctionComponent } from 'react';

const IC_MAP = require('../../assets/img/map-preview.png');

const selectionStyle: any = {
  0: {
    color: '#B90039',
    borderColor: '#B90039',
    borderWidth: 2,
    borderRadius: 4,
  },
  1: {
    color: 'rgba(0, 0, 0, 0.4)',
    borderColor: 'rgba(0, 0, 0, 0.4)',
  },
};

interface Props {
  pickUpLoc: string;
  allowPickup: boolean;
  doDelivery: boolean;
  onChange: (arg0: number) => void;
  defaultVal: any;
}

const DeliveryPickup: FunctionComponent<Props> = ({
  pickUpLoc,
  allowPickup,
  doDelivery,
  onChange,
  defaultVal,
}) => {
  const onTabChange = (number: number) => {
    onChange(number);
  };

  return (
    <View
      style={[
        styles.container,
        doDelivery && !allowPickup && { paddingBottom: 10 },
      ]}>
      {allowPickup && doDelivery && (
        <View style={styles.optionContainer}>
          <TouchableOpacity
            onPress={() => {
              onTabChange(0);
            }}>
            <RNTextView
              style={[styles.optionItem, selectionStyle[defaultVal ^ 0]]}
              textType={'medium'}>
              DELIVERY
            </RNTextView>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              onTabChange(1);
            }}>
            <RNTextView
              style={[styles.optionItem, selectionStyle[defaultVal ^ 1]]}
              textType={'medium'}>
              PICKUP
            </RNTextView>
          </TouchableOpacity>
        </View>
      )}
      {/** Check if there is no delivery and only pickup and show the location conditionally */}
      {(defaultVal == 1 || (!doDelivery && allowPickup)) && (
        <View
          style={[
            styles.locationContainer,
            doDelivery && allowPickup && { paddingTop: 20 },
          ]}>
          <FastImage source={IC_MAP} style={styles.locationImg} />
          <View style={styles.locationTextContainer}>
            <RNTextView style={styles.locationHeader} textType={'medium'}>
              Pickup Location:
            </RNTextView>
            <RNTextView style={styles.locationValue} textType={'regular'}>
              {pickUpLoc}
            </RNTextView>
          </View>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 30,
    paddingBottom: 30,
    paddingTop: 10,
    alignItems: 'flex-start',
  },
  optionContainer: {
    flexDirection: 'row',
    borderRadius: 6,
    borderWidth: 2,
    borderColor: 'rgba(0, 0, 0, 0.16)',
  },
  optionItem: {
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderRadius: 2,
    backgroundColor: '#FFFFFF',
    fontWeight: '700',
  },
  locationContainer: {
    flexDirection: 'row',
  },
  locationImg: {
    width: 48,
    height: 48,
  },
  locationTextContainer: {
    flexDirection: 'column',
    marginHorizontal: 16,
    justifyContent: 'space-around',
  },
  locationHeader: {
    fontWeight: '700',
  },
  locationValue: {
    fontWeight: '400',
    fontSize: 14,
  },
});

export default DeliveryPickup;
