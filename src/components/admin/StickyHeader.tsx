import React from 'react';
import { StyleSheet, Platform, SafeAreaView } from 'react-native';

import RNTextView from '../common/RNTextView';
import utils from '../../utils/helpers';
import colors from '../../configs/colors';

export default function StickyHeader({
  providerName,
}: {
  [key: string]: string;
}) {
  return (
    <SafeAreaView
      style={[styles.container, Platform.OS === 'android' && { height: 83 }]}>
      <RNTextView style={styles.title}>
        {utils.capitalizeFirstLetter(providerName)}
      </RNTextView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'flex-end',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 10,
    paddingBottom: 20,
  },
});
