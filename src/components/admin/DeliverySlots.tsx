import React from 'react';
import { View, StyleSheet, Text, FlatList } from 'react-native';
import FastImage from 'react-native-fast-image';
import moment from 'moment';

import RNTextView from '../common/RNTextView';
import SlotCard from './SlotCard';

const IC_CLOCK = require('../../assets/img/ic_clock.png');

interface Props {
  deliveryData: any;
}

export default function DeliverySlots({ deliveryData }: Props) {
  return (
    <View style={styles.container}>
      {deliveryData.length == 1 ? (
        <View style={styles.singeSlotContainer}>
          <FastImage source={IC_CLOCK} style={styles.singleSlotImg} />
          <View style={styles.singleSlotTextContainer}>
            <RNTextView style={styles.signleSlotHeader} textType={'medium'}>
              Delivery slot
            </RNTextView>
            <RNTextView style={styles.signleSlotValue} textType={'regular'}>
              {deliveryData[0]}
            </RNTextView>
          </View>
        </View>
      ) : (
        <View>
          <Text style={styles.titleTxt}>Available Delivery Slots</Text>
          <FlatList
            data={deliveryData}
            numColumns={2}
            columnWrapperStyle={styles.wrapper}
            renderItem={({ item }) => <SlotCard slot={item} />}
            listKey={moment().valueOf().toString()}
            keyExtractor={(_, index) => index.toString()}
          />
        </View>
      )}
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 30,
  },
  titleTxt: {
    fontWeight: 'bold',
    fontSize: 20,
    marginTop: 40,
    marginBottom: 24,
  },
  wrapper: {
    justifyContent: 'space-between',
    paddingBottom: 12,
  },
  singeSlotContainer: {
    flexDirection: 'row',
    marginTop: 20,
  },
  singleSlotImg: {
    width: 24,
    height: 24,
    marginTop: 8,
  },
  singleSlotTextContainer: {
    paddingHorizontal: 18,
  },
  signleSlotHeader: {
    fontWeight: '700',
    fontSize: 16,
  },
  signleSlotValue: {
    fontWeight: '400',
    marginTop: 8,
    color: 'rgba(0, 0, 0, 0.4)',
  },
});
