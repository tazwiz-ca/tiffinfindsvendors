import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

interface Props {
  plan: string;
  price: number;
  nos: number;
}

export default function PlanCard({ plan, price, nos }: Props) {
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.planTxt}>
          {plan.toUpperCase()} {`(${nos}${nos > 1 ? ' Meals' : ' Meal'})`}
        </Text>
        <Text style={styles.priceTxt}>$ {price}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderColor: '#ECECEC',
    borderRadius: 4,
    padding: 19,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 8,
    marginHorizontal: 15,
  },
  planTxt: {
    fontSize: 12,
    fontWeight: '600',
    marginBottom: 12,
  },
  priceTxt: {
    fontWeight: 'bold',
    opacity: 0.4,
    fontSize: 16,
  },
  recommendedContainer: {
    backgroundColor: 'rgba(0, 185, 112, 0.2)',
    borderRadius: 2,
    paddingHorizontal: 7,
    paddingVertical: 5,
    position: 'absolute',
    top: 2,
    right: 2,
  },
  recommendedTxt: {
    fontWeight: '500',
    fontSize: 8,
    color: '#00B970',
  },
});
