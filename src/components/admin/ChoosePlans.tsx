import React, { useEffect, useState } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import FastImage from 'react-native-fast-image';
import moment from 'moment';

import RNTextView from '../common/RNTextView';
import PlanCard from './PlanCard';
import utils from '../../utils/helpers';

const IC_CALENDAR = require('../../assets/img/ic_calendar.png');

interface Props {
  mealData: any;
  defaultVal: any;
}

export default function ChoosePlans({ mealData, defaultVal }: Props) {
  const [data, setData] = useState<any>([]);

  useEffect(() => {
    let planData = [];
    for (let i = 0; i < mealData.price.length; i++) {
      planData.push({
        plan: mealData.price[i]?.frequency,
        price: mealData.price[i]?.value,
        nos: mealData.price[i]?.nos,
        isSelected: i === defaultVal,
      });
    }
    setData(planData);
  }, [mealData, defaultVal]);

  return (
    <View style={styles.container}>
      {data.length == 1 ? (
        <View style={styles.singlePlanContainer}>
          <FastImage source={IC_CALENDAR} style={styles.singlePlanImg} />
          <View style={styles.singlePlanTextContainer}>
            <RNTextView style={styles.singlePlanTextHeader} textType={'medium'}>
              Frequency
            </RNTextView>
            <View style={styles.singlePlanTextValueContainer}>
              <RNTextView style={styles.singlePlanTextFood} textType={'medium'}>
                {utils.capitalizeFirstLetter(data[0].plan)}
              </RNTextView>
            </View>
          </View>
          <View style={styles.singlePlanPriceContainer}>
            <RNTextView style={styles.singlePlanPrice} textType={'bold'}>
              {'$' + data[0].price}
            </RNTextView>
          </View>
        </View>
      ) : (
        <View>
          <RNTextView style={styles.titleTxt} textType="bold">
            Available Plans
          </RNTextView>
          <View style={{ paddingHorizontal: 17 }}>
            <FlatList
              data={data}
              style={{ marginBottom: 8 }}
              renderItem={({ item }) => (
                <PlanCard plan={item.plan} price={item.price} nos={item.nos} />
              )}
              listKey={moment().valueOf().toString()}
              keyExtractor={(_, index) => index.toString()}
            />
          </View>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  titleTxt: {
    fontWeight: 'bold',
    fontSize: 20,
    marginTop: 40,
    marginLeft: 31,
    marginBottom: 24,
  },
  singlePlanContainer: {
    flexDirection: 'row',
    paddingHorizontal: 34,
    paddingVertical: 20,
  },
  singlePlanImg: {
    marginTop: 8,
    width: 24,
    height: 24,
  },
  singlePlanTextContainer: {
    paddingHorizontal: 18,
  },
  singlePlanTextHeader: {
    paddingTop: 0,
    fontWeight: '700',
    fontSize: 16,
  },
  singlePlanTextValueContainer: {
    flexDirection: 'row',
    marginTop: 4,
  },
  singlePlanTextFood: {
    fontWeight: '600',
    fontSize: 14,
    marginEnd: 4,
  },
  singlePlanTextDays: {
    fontWeight: '400',
    fontSize: 14,
    color: 'rgba(0, 0, 0, 0.4)',
  },
  singlePlanPriceContainer: {
    alignSelf: 'center',
    position: 'absolute',
    top: 36,
    right: 30,
  },
  singlePlanPrice: {
    color: 'rgba(0, 0, 0, 0.4)',
    fontSize: 18,
  },
});
