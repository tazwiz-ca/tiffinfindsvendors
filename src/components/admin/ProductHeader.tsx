import React from 'react';
import { View, StyleSheet } from 'react-native';
import FastImage from 'react-native-fast-image';

import RNTextView from '../common/RNTextView';
import utils from '../../utils/helpers';

export default function ProductHeader({ product }: any) {
  return (
    <View style={styles.container}>
      <FastImage
        source={{ uri: product.images[0] }}
        style={styles.imageBackground}
      />
      <View style={styles.productDetailContainer}>
        <View style={styles.titleRow}>
          <RNTextView
            style={styles.productTitle}
            textType={'bold'}
            props={{ numberOfLines: 1, ellipsizeMode: 'tail' }}>
            {utils.capitalizeFirstLetter(product.providerName)}
          </RNTextView>
        </View>
        <View style={styles.productSubTitleContainer}>
          {Object.values(product.menu.menu).map((meal: any, idx) => (
            <View
              style={{ alignItems: 'center', flexDirection: 'row' }}
              key={idx}>
              <RNTextView style={styles.bulletStyle}>{'\u2B24'}</RNTextView>
              <RNTextView style={styles.productSubTitle} textType={'regular'}>
                {utils.capitalizeFirstLetter(meal.type)}
              </RNTextView>
            </View>
          ))}
          {product.menu?.mealCuisines &&
            product.menu?.mealCuisines?.map((cusine: any, idx: number) => (
              <View
                style={{ alignItems: 'center', flexDirection: 'row' }}
                key={idx}>
                <RNTextView style={styles.bulletStyle} textType={'regular'}>
                  {'\u2B24'}
                </RNTextView>
                <RNTextView style={styles.productSubTitle} textType={'regular'}>
                  {utils.capitalizeFirstLetter(cusine)}
                </RNTextView>
              </View>
            ))}
        </View>
        <View style={styles.productSubTitleContainer}>
          {product.doDelivery && (
            <>
              <RNTextView style={styles.bulletStyle} textType={'regular'}>
                {'\u2B24'}
              </RNTextView>
              <RNTextView style={styles.productSubTitle} textType={'regular'}>
                Delivery
              </RNTextView>
            </>
          )}
          {product.allowPickup && (
            <>
              <RNTextView style={styles.bulletStyle} textType={'regular'}>
                {'\u2B24'}
              </RNTextView>
              <RNTextView style={styles.productSubTitle} textType={'regular'}>
                Pickup
              </RNTextView>
            </>
          )}
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  imageBackground: {
    width: '100%',
    height: 228,
    resizeMode: 'cover',
  },
  imageStyle: {
    resizeMode: 'cover',
  },
  productDetailContainer: {
    width: '90%',
    justifyContent: 'space-around',
    padding: 16,
    marginHorizontal: 16,
    marginTop: -54,
    backgroundColor: '#FFFFFF',
    borderRadius: 4,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 5,
    shadowOpacity: 0.2,
    elevation: 1,
  },
  productTitle: {
    fontSize: 20,
    maxWidth: 250,
  },
  productSubTitleContainer: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 12,
  },

  productSubTitle: {
    color: 'rgba(0,0,0,0.6)',
    marginRight: 4,
  },
  bulletStyle: {
    color: 'rgba(0,0,0,0.6)',
    fontSize: 4,
    marginRight: 4,
  },
  titleRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
