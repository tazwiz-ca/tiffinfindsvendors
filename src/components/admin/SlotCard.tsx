import React from 'react';
import { View, StyleSheet } from 'react-native';
import colors from '../../configs/colors';

import RNTextView from '../common/RNTextView';

interface Props {
  slot: string;
}

export default function SlotCard({ slot }: Props) {
  return (
    <View style={styles.container}>
      <RNTextView style={styles.slotTxt} textType={'regular'}>
        {slot}
      </RNTextView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderColor: '#ECECEC',
    borderRadius: 4,
    paddingVertical: 12,
    width: '48%',
    backgroundColor: colors.white,
  },
  slotTxt: {
    fontWeight: 'bold',
    color: colors.black,
    textAlign: 'center',
    opacity: 0.6,
  },
});
