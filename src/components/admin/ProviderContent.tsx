import React, { FunctionComponent } from 'react';
import { View, StyleSheet, TouchableWithoutFeedback } from 'react-native';

import colors from '../../configs/colors';
import CustomButton from '../common/CustomButton';
import LoadingImage from '../common/LoadingImage';
import RNTextView from '../common/RNTextView';
import { useNavigation } from '@react-navigation/core';
import routes from '../../navigations/routes';

interface Props {
  providerName: string;
  providerId: string;
  isMenuAdded: boolean;
  created: string;
  isPaymentInfoAdded?: boolean;
  uri: object;
  onApprove: (arg0: string, arg1: string) => void;
  onDecline: (arg0: string, arg1: string) => void;
  isVerified: boolean;
}

const ProviderContent: FunctionComponent<Props> = ({
  providerName,
  providerId,
  isMenuAdded,
  isPaymentInfoAdded,
  created,
  uri,
  onApprove,
  onDecline,
  isVerified,
}) => {
  const navigation = useNavigation();
  return (
    <TouchableWithoutFeedback
      onPress={() =>
        navigation.navigate(routes.PROVIDER_DETAILS, { providerId })
      }>
      <View style={styles.container}>
        <LoadingImage uri={uri} customStyles={styles.img} />
        <View style={styles.contentWrapper}>
          <RNTextView
            style={styles.providerName}
            numberOfLines={1}
            ellipsizeMode="tail">
            {providerName}
          </RNTextView>

          <RNTextView
            style={styles.subtitleTxt}
            numberOfLines={1}
            ellipsizeMode="tail">
            Menu Added: {isMenuAdded ? 'Yes' : 'No'}
          </RNTextView>
          <RNTextView
            style={styles.subtitleTxt}
            numberOfLines={1}
            ellipsizeMode="tail">
            Payment Info Added: {isPaymentInfoAdded ? 'Yes' : 'No'}
          </RNTextView>

          <RNTextView
            style={styles.createdTxt}
            numberOfLines={1}
            ellipsizeMode="tail">
            Created {created}
          </RNTextView>

          {!isVerified ? (
            <CustomButton
              title="Approve"
              onPress={() => onApprove(providerName, providerId)}
              customStyles={styles.approveBtn}
            />
          ) : (
            <CustomButton
              title="Decline"
              onPress={() => onDecline(providerName, providerId)}
              customStyles={styles.declineBtn}
            />
          )}
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    marginHorizontal: 10,
    flexDirection: 'row',
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 3,
      height: 2,
    },
    shadowOpacity: 0.2,
    shadowRadius: 2.84,
    elevation: 5,
  },
  img: {
    width: 132,
    height: 160,
    marginRight: 10,
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
  },
  contentWrapper: {
    padding: 5,
    justifyContent: 'space-between',
  },
  subtitleTxt: {
    fontFamily: 'Apercu-Regular',
    color: colors.textInputBorder,
  },
  providerName: {
    fontFamily: 'Apercu-Regular',
    fontSize: 16,
    fontWeight: '500',
    color: colors.textInputBorder,
    maxWidth: 200,
  },
  createdTxt: {
    fontFamily: 'Apercu-Regular',
    fontSize: 12,
    color: colors.textInputBorder,
  },
  approveBtn: {
    width: 80,
    height: 30,
    padding: 5,
  },
  declineBtn: {
    width: 80,
    height: 30,
    padding: 5,
    backgroundColor: colors.primary,
  },
});

export default ProviderContent;
