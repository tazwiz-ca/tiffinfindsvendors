import React from 'react';
import { View, StyleSheet } from 'react-native';

import RNTextView from '../common/RNTextView';
import utils from '../../utils/helpers';

interface Props {
  planContents: any;
  index?: number;
  onPress?: (arg0: number) => void;
  isSingleMenu?: boolean;
}

export default function MealCard({ planContents }: Props) {
  const RenderContent: any = ({ item }: any) => {
    let contents = [];
    for (let key in item) {
      contents.push(
        <View style={styles.mealContent} key={key}>
          <RNTextView style={styles.dayTxt}>{key.toUpperCase()}:</RNTextView>
          <RNTextView style={styles.itemTxt} textType={'regular'}>
            {utils.capitalizeFirstLetter(item[key])}
          </RNTextView>
        </View>,
      );
    }
    return contents;
  };
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <RNTextView style={styles.title} textType={'medium'}>
          {utils.capitalizeFirstLetter(planContents.type)} Plan
        </RNTextView>
      </View>
      <View style={styles.content}>
        <RenderContent item={planContents.days} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0.6,
      height: 0.2,
    },
    shadowRadius: 2,
    shadowOpacity: 0.3,
    borderRadius: 4,
    borderColor: '#ECECEC',
    borderWidth: 2,
  },
  selectedContainer: {
    borderColor: '#00B970',
    borderWidth: 2,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 0,
    borderRadius: 4,
  },
  header: {
    flexDirection: 'row',
    paddingVertical: 18,
    paddingLeft: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ECECEC',
    alignItems: 'center',
  },
  title: {
    fontSize: 16,
    color: '#00B970',
  },
  content: {
    paddingVertical: 10,
    paddingHorizontal: 16,
  },
  mealContent: {
    flexDirection: 'row',
    marginVertical: 10,
    alignItems: 'center',
  },
  dayTxt: {
    fontWeight: '600',
    fontSize: 12,
    marginEnd: 4,
    width: 110,
  },
  itemTxt: {
    flex: 1,
    flexWrap: 'wrap',
    fontSize: 14,
    opacity: 0.6,
  },
});
