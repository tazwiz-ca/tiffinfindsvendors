import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import * as Animatable from 'react-native-animatable';

import RNTextView from '../common/RNTextView';
import MealCard from './MealCard';
import colors from '../../configs/colors';

interface Props {
  mealData: Array<any>;
  defaultVal?: number;
}

export default function MealPlans({ mealData = [], defaultVal = 0 }: Props) {
  const [activeIndex, setActiveIndex] = useState(defaultVal);
  const [planContents, setPlanContents] = useState(mealData);

  useEffect(() => {
    if (mealData.length <= 1) return;
    const planData = [...mealData];
    planData.map((plan, idx) => {
      if (idx !== defaultVal) plan.isSelected = false;
      else plan.isSelected = true;
    });
    setPlanContents(planData);
    setActiveIndex(defaultVal);
  }, [defaultVal]);

  const onIndexChanged = (index: number) => {
    setActiveIndex(index);
  };

  return (
    <Animatable.View
      animation={'slideInUp'}
      duration={500}
      style={[styles.container, mealData.length < 2 && { marginBottom: 30 }]}>
      {mealData.length !== 0 && (
        <RNTextView style={styles.titleTxt} textType={'bold'}>
          Available Meal plans
        </RNTextView>
      )}

      {mealData.length > 1 ? (
        <Carousel
          currentScrollPosition={defaultVal}
          data={planContents}
          layout={'stack'}
          layoutCardOffset={18}
          renderItem={({ item }: any) => <MealCard planContents={item} />}
          sliderWidth={Dimensions.get('window').width}
          itemWidth={Dimensions.get('window').width - 60}
          activeDotIndex={activeIndex}
          dotsLength={planContents.length}
          onSnapToItem={onIndexChanged}
        />
      ) : (
        mealData?.length > 0 && (
          <View style={{ paddingHorizontal: 29 }}>
            <MealCard
              planContents={Object.values(mealData)[0]}
              isSingleMenu={true}
            />
          </View>
        )
      )}
      <Pagination
        dotsLength={planContents.length}
        activeDotIndex={activeIndex}
        dotStyle={{
          width: 10,
          height: 10,
          borderRadius: 5,
          backgroundColor: '#00B970',
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    </Animatable.View>
  );
}

const styles = StyleSheet.create({
  container: {},
  titleTxt: {
    fontSize: 20,
    marginLeft: 31,
    marginBottom: 24,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  dot: {
    margin: 4,
    width: 8,
    height: 8,
    borderRadius: 4,
  },
  bgDark: {
    backgroundColor: colors.primary,
  },
  bgLight: {
    backgroundColor: colors.primary,
    opacity: 0.3,
  },
});
