import React from 'react';
import { Linking, StyleSheet, TouchableOpacity, View } from 'react-native';
import * as Sentry from '@sentry/react-native';
import { useDispatch, useSelector } from 'react-redux';
import FastImage from 'react-native-fast-image';

import colors from '../../configs/colors';
import RNTextView from '../common/RNTextView';
import { RootState } from '../../store';
import { startLoader, stopLoader } from '../../store/reducers/common/loader';
import { getSupportCtNumber } from '../../network/MiscellaneousService';

export default function WhatsappChat() {
  const dispatch = useDispatch();
  const token = useSelector((state: RootState) => state.auth.token);
  let whatsAppMsg =
    'Hi, I need help with my order in tiffin finds..Can you help me?';

  const handleChatClick = async () => {
    try {
      dispatch({ type: startLoader.type });
      const { contact } = await getSupportCtNumber(token);
      dispatch({ type: stopLoader.type });
      let url = 'whatsapp://send?text=' + whatsAppMsg + '&phone=+1' + contact;
      Linking.openURL(url).catch(() => alert('Check if whatsapp is installed'));
    } catch (error) {
      dispatch({ type: stopLoader.type });
      console.log(error);
      Sentry.captureException(error);
    }
  };

  return (
    <TouchableOpacity onPress={handleChatClick} activeOpacity={0.7}>
      <View style={styles.container}>
        <RNTextView style={styles.text}>Chat with us</RNTextView>
        <FastImage
          source={require('../../assets/img/whatsapp.jpeg')}
          style={styles.img}
        />
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 15,
  },
  text: {
    fontSize: 20,
    fontWeight: '500',
    color: colors.lightGreen,
    marginRight: 5,
    fontFamily: 'Apercu-Regular',
  },
  img: {
    width: 30,
    height: 30,
  },
});
