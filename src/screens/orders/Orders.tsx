import React, { useEffect, useState } from 'react';
import { View, StyleSheet, FlatList, StatusBar, Platform } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';

import NavHeader from '../../components/common/NavHeader';
import RNTextView from '../../components/common/RNTextView';
import BottomDeclineView from '../../components/orders/BottomDeclineView';
import { RootState } from '../../store';
import CustomButton from '../../components/common/CustomButton';
import {
  OrderState,
  fetchOrders,
  Customer,
  mutateOrderStatus,
  resetOrderStatus,
} from '../../store/reducers/orders/orders';
import colors from '../../configs/colors';
import helpers from '../../utils/helpers';
import FilterText from '../../components/common/FilterText';
import ViewMenu from '../../components/orders/ViewMenu';
import EmptyView from '../../components/orders/EmptyView';

const Orders: React.FunctionComponent = () => {
  const [isFetching, setIsFetching] = useState(true);
  const [declineModalVisibility, setDeclineModalVisibility] = useState(false);
  const [menuVisibility, setMenuVisibility] = useState(false);
  const [selectedOrder, setSelectedOrder] = useState<OrderState>();

  const dispatch = useDispatch();
  const orders = useSelector((state: RootState) => state.orders.filteredOrders);
  const filterStatus = useSelector(
    (state: RootState) => state.orders.filterOrderStatus,
  );
  const token = useSelector((state: RootState) => state.auth.token);

  useEffect(() => {
    if (Platform.OS == 'android') {
      StatusBar.setTranslucent(false);
    }
    dispatch(fetchOrders(token));
  }, []);

  useEffect(() => {
    setIsFetching(false);
  }, [orders]);

  const getLetter = (customer: Customer) => {
    if (customer && customer.fullName) {
      if (customer.fullName.length > 0) {
        return customer.fullName.charAt(0).toUpperCase();
      }
    }

    return 'U';
  };

  const getStatus = (status: string) => {
    if (status === 'Pending') {
      return {
        backgroundColor: '#FFA400',
        color: '#fff',
      };
    } else if (status === 'Completed') {
      return {
        backgroundColor: '#00B970',
        color: '#fff',
      };
    } else if (status === 'Active') {
      return {
        backgroundColor: '#0069B9',
        color: '#fff',
      };
    } else if (status === 'Cancelled') {
      return {
        backgroundColor: 'rgba(0,0,0,0.2)',
        color: '#fff',
      };
    } else if (status === 'Declined') {
      return {
        backgroundColor: '#B90039',
        color: '#fff',
      };
    }
  };

  const isPending = (status: string) => status === 'Pending';

  const onControlAction = (status: boolean, order: OrderState) => {
    setSelectedOrder(order);
    status
      ? updateOrderStatus(true, order, '')
      : setDeclineModalVisibility(true);
  };

  const onDeclineReasonReceived = (reason: string) => {
    setDeclineModalVisibility(false);
    if (!selectedOrder) return;
    updateOrderStatus(false, selectedOrder, reason);
  };

  const onMenuCloseHandler = () => {
    setMenuVisibility(false);
  };

  const updateOrderStatus = (
    status: boolean,
    order: OrderState,
    description: string,
  ) => {
    dispatch(mutateOrderStatus(token, order._id, status, description));
  };

  type OrderItemProps = {
    title: string;
    value: any;
  };

  const OrderItemContainer = ({ title, value }: OrderItemProps) => (
    <View style={styles.orderItemDetailContainer}>
      <RNTextView style={styles.orderItemDetailTitle} textType="medium">
        {title}:
      </RNTextView>
      <RNTextView style={styles.orderItemDetailValue} textType="light">
        {value}
      </RNTextView>
    </View>
  );

  const OrderItem = (orderItem: OrderState) => {
    const customer = orderItem.customer;

    return (
      <View>
        <View style={styles.orderItem}>
          <View style={styles.orderItemLeft}>
            <View
              style={[styles.nameWrapper, getStatus(orderItem.orderStatus)]}>
              <RNTextView style={styles.orderItemLeftLetter} textType="bold">
                {getLetter(customer)}
              </RNTextView>
            </View>
            <View style={styles.orderItemCustName}>
              <RNTextView style={styles.orderItemDetailTitle} textType="bold">
                {customer ? customer.fullName.toUpperCase() : 'N/A'}
              </RNTextView>
              <OrderItemContainer
                title="Order ID"
                value={`#${orderItem.orderId}`}
              />
              <OrderItemContainer title="Quantity" value={orderItem.quantity} />
              <OrderItemContainer
                title="Meal Rate"
                value={orderItem.receipt.total}
              />
              <OrderItemContainer
                title="Meal Plan"
                value={helpers.capitalizeFirstLetter(orderItem.menu.mealPlan)}
              />
              <OrderItemContainer
                title="Menu Frequency"
                value={helpers.capitalizeFirstLetter(orderItem.mealFrequency)}
              />
              {/** Show delivery time only when delivery is selected */}
              {orderItem.deliverySelected && (
                <OrderItemContainer
                  title="Delivery Time"
                  value={orderItem.deliveryTime}
                />
              )}
              <OrderItemContainer
                title="Delivery / PickUp"
                value={orderItem.deliverySelected ? 'Delivery' : 'Pickup'}
              />
              <OrderItemContainer
                title="Start Date"
                value={moment(orderItem.startDate).format('MMMM Do YYYY')}
              />
              {/** Show address only when delivery is selected */}
              {orderItem.deliverySelected && (
                <OrderItemContainer
                  title="Address"
                  value={orderItem.deliveryLocation.fullAddress}
                />
              )}
            </View>
          </View>
          {isPending(orderItem.orderStatus) ? (
            <></>
          ) : (
            <View style={styles.orderItemRight}>
              <RNTextView
                style={[
                  styles.orderRightText,
                  getStatus(orderItem.orderStatus),
                ]}
                textType="medium">
                {orderItem.orderStatus}
              </RNTextView>
            </View>
          )}
        </View>
        <CustomButton
          title="View Menu"
          customStyles={styles.viewMenuBtn}
          onPress={() => {
            setSelectedOrder(orderItem);
            setMenuVisibility(true);
          }}
        />

        {isPending(orderItem.orderStatus) ? (
          <View style={styles.orderControlsContainer}>
            <CustomButton
              title="Accept"
              enabledColor="#1B5E20"
              customStyles={styles.acceptBtn}
              onPress={() => onControlAction(true, orderItem)}
            />
            <CustomButton
              title="Decline"
              enabledColor="#BF360C"
              customStyles={styles.acceptBtn}
              onPress={() => onControlAction(false, orderItem)}
            />
          </View>
        ) : (
          <></>
        )}
      </View>
    );
  };

  return (
    <>
      <StatusBar barStyle="light-content" />
      <View style={styles.container}>
        <NavHeader title="Orders" isFilter={orders && orders.length > 0} />
        {orders && orders.length > 0 ? (
          <View>
            <View style={styles.orderItemHeader}>
              <RNTextView
                style={[
                  styles.orderItemHeaderText,
                  { flex: 0.7, textAlign: 'left', marginLeft: 12 },
                ]}
                textType="bold">
                Name
              </RNTextView>
              <RNTextView
                style={[styles.orderItemHeaderText, { flex: 0.3 }]}
                textType="bold">
                Status
              </RNTextView>
            </View>
            <FilterText
              text={filterStatus}
              onPress={() =>
                dispatch({
                  type: resetOrderStatus.type,
                })
              }
            />
          </View>
        ) : (
          <></>
        )}

        <View style={styles.orderList}>
          <FlatList
            data={orders}
            refreshing={isFetching}
            onRefresh={() => dispatch(fetchOrders(token))}
            renderItem={({ item }) => OrderItem(item)}
            ItemSeparatorComponent={() => (
              <View style={styles.orderItemUnderline} />
            )}
            ListFooterComponent={() => {
              return orders && orders.length > 0 ? (
                <View
                  style={[styles.orderItemUnderline, { marginBottom: 30 }]}
                />
              ) : (
                <></>
              );
            }}
            keyExtractor={(_, index) => index.toString()}
            ListEmptyComponent={() => {
              return <EmptyView />;
            }}
            contentContainerStyle={styles.contentContainerStyle}
          />
        </View>
        <BottomDeclineView
          visibility={declineModalVisibility}
          onOKHandle={onDeclineReasonReceived}
          onCancel={() => setDeclineModalVisibility(false)}
        />
        {menuVisibility ? (
          <ViewMenu
            visibility={menuVisibility}
            days={selectedOrder?.menu.days}
            onCloseClicked={onMenuCloseHandler}
          />
        ) : (
          <></>
        )}
      </View>
    </>
  );
};

export default Orders;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  navHeader: {
    marginTop: 10,
  },
  orderList: {
    flex: 1,
  },
  contentContainerStyle: {
    flexGrow: 1,
  },
  orderItemHeader: {
    flexDirection: 'row',
    padding: 4,
    borderRadius: 4,
    elevation: 1,
    borderBottomWidth: 1,
    borderColor: '#e4e4e4',
    backgroundColor: colors.white,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0.6,
      height: 0.2,
    },
    shadowRadius: 2,
    shadowOpacity: 0.3,
    marginBottom: 10,
  },
  orderItemHeaderText: {
    textAlign: 'center',
    fontSize: 20,
    paddingVertical: 10,
    fontFamily: 'JosefinSans-Bold',
    color: colors.black,
    opacity: 0.8,
  },
  orderItem: {
    flex: 1,
    flexDirection: 'row',
    padding: 4,
    paddingVertical: 8,
    marginHorizontal: 10,
  },
  orderItemLeft: {
    flex: 0.7,
    flexDirection: 'row',
    paddingVertical: 10,
  },
  orderItemLeftLetter: {
    textAlign: 'center',
    textAlignVertical: 'center',
    color: '#FFFFFF',
  },
  nameWrapper: {
    width: 30,
    height: 30,
    borderRadius: 15,
    backgroundColor: '#4D4D4D',
    justifyContent: 'center',
    alignItems: 'center',
  },
  orderItemLeftText: {
    marginLeft: 8,
  },
  orderItemRight: {
    flex: 0.3,
    paddingVertical: 10,
    alignItems: 'center',
  },
  orderItemViewDetails: {
    backgroundColor: '#0D47A1',
    color: '#FFFFFF',
    fontSize: 12,
    width: 100,
    borderRadius: 8,
    marginVertical: 8,
    marginRight: 16,
    paddingVertical: 6,
    textAlign: 'center',
    alignSelf: 'center',
    marginBottom: 16,
  },
  orderRightText: {
    width: 100,
    backgroundColor: '#2E7D32',
    borderRadius: 16,
    fontSize: 16,
    paddingVertical: 4,
    color: '#FFFFFF',
    textAlign: 'center',
    fontFamily: 'Skia',
  },
  orderItemDetailContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  orderItemCustName: {
    paddingHorizontal: 6,
  },
  orderItemDetailTitle: {
    paddingVertical: 4,
    fontFamily: 'Apercu-Bold',
    fontSize: 16,
  },
  orderItemDetailValue: {
    paddingVertical: 4,
    marginStart: 4,
    flexWrap: 'wrap',
    maxWidth: 250,
    fontFamily: 'Apercu-Mono',
    fontSize: 14,
  },
  orderControlsContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingBottom: 16,
  },
  orderControlsAccept: {
    backgroundColor: '#1B5E20',
    color: '#FFFFFF',
    width: 100,
    textAlign: 'center',
    paddingVertical: 4,
    borderRadius: 6,
    marginEnd: 8,
  },
  orderControlsDecline: {
    marginStart: 8,
    backgroundColor: '#BF360C',
    color: '#FFFFFF',
    width: 100,
    textAlign: 'center',
    paddingVertical: 6,
    borderRadius: 8,
  },
  orderItemUnderline: {
    flex: 1,
    height: 1.5,
    marginHorizontal: 14,
    backgroundColor: '#e4e4e4',
  },
  viewMenuBtn: {
    padding: 7,
    width: 100,
    height: 30,
    alignSelf: 'center',
    marginBottom: 20,
  },
  acceptBtn: {
    padding: 7,
    width: 100,
    height: 30,
  },
  declineBtn: {
    padding: 7,
    width: 100,
    height: 30,
  },
});
