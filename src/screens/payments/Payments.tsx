import React, { useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  KeyboardType,
  Platform,
  StatusBar,
} from 'react-native';
import * as yup from 'yup';
import { Formik } from 'formik';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import * as Sentry from '@sentry/react-native';

import NavHeader from '../../components/common/NavHeader';
import CustomTextInput from '../../components/common/CustomTextInput';
import CustomLocationSearch from '../../components/menu/CustomLocationSearch';
import colors from '../../configs/colors';
import AppDatePicker from '../../components/common/AppDatePicker';
import CustomButton from '../../components/common/CustomButton';
import RNTextView from '../../components/common/RNTextView';
import { RootState } from '../../store';
import { updateAddress } from '../../store/reducers/payments/payments';
import { errorToast, successToast } from '../../store/reducers/common/toaster';
import utils from '../../utils/helpers';
import { savePaymentInfo } from '../../network/PaymentService';
import { startLoader, stopLoader } from '../../store/reducers/common/loader';

const Payments: React.FunctionComponent = () => {
  const [isDisabled, setIsDisabled] = useState<boolean>(true);
  const paymentData = useSelector((state: RootState) => state.payments);
  const token = useSelector((state: RootState) => state.auth.token);

  const dispatch = useDispatch();

  const data = [
    {
      key: 1,
      placeholder: 'Full Name',
      name: 'fullName',
    },
    {
      key: 2,
      placeholder: 'Date of Birth',
      name: 'dob',
    },
    {
      key: 3,
      placeholder: 'Social insurance number',
      name: 'sin',
      secureEntry: true,
      keyboardType: 'number-pad',
    },
    {
      key: 4,
      placeholder: 'Transit number',
      name: 'transitNo',
      keyboardType: 'number-pad',
    },
    {
      key: 5,
      placeholder: 'Inst. number',
      name: 'instNo',
      keyboardType: 'number-pad',
    },
    {
      key: 6,
      placeholder: 'Account number',
      name: 'accNo',
      secureEntry: true,
      keyboardType: 'number-pad',
    },
  ];

  useEffect(() => {
    if (Platform.OS == 'android') {
      StatusBar.setTranslucent(false);
    }
    if (!paymentData.address.address) return;
    setIsDisabled(false);
  }, [paymentData]);

  useEffect(() => {
    if (paymentData.isPaymentVerified) {
      dispatch({
        type: successToast.type,
        payload: { message: 'We have your payment info' },
      });
    }
  }, [paymentData]);

  /**
   *
   * @param arg0 {"address": "1 MSGR Henkey Terrace, 1 MSGR Henkey Terrace, Hamilton, ON, Canada", "city": "Hamilton", "coordinates": {"coordinates": [-79.9077166, 43.2021392]}, "fullAddress": "1 MSGR Henkey Terrace, Hamilton, ON, Canada", "postalCode": "L9B 2M2", "province": "ON"} {"latitude": 43.2021392, "latitudeDelta": 0.001, "longitude": -79.9077166, "longitudeDelta": 0.001}
   * @param arg1
   */
  const handleLocationSearch = (arg0: string, _: any) => {
    dispatch({ type: updateAddress.type, payload: { address: arg0 } });
  };

  type FormikVals = {
    accNo: string;
    dob: string;
    fullName: string;
    instNo: string;
    sin: string;
    transitNo: string;
  };

  const handleSubmit = async (values: FormikVals) => {
    try {
      dispatch({ type: startLoader.type });
      let paymentObj = {
        fullName: values.fullName,
        routingNumber: `${values.transitNo}-${values.instNo}`,
        accountNumber: values.accNo,
        sin: values.sin,
        dobDay: moment(values.dob, 'Do MMM,YYYY').day().toString(),
        dobMonth: moment(values.dob, 'Do MMM,YYYY').month().toString(),
        dobYear: moment(values.dob, 'Do MMM,YYYY').year().toString(),
        address: paymentData.address,
      };
      await savePaymentInfo(token, paymentObj);

      dispatch({
        type: successToast.type,
        payload: {
          message: 'Your payment information is saved successfully',
        },
      });
    } catch (error) {
      try {
        dispatch({
          type: errorToast.type,
          payload: { message: JSON.parse(error)?.error },
        });
      } catch (error) {
        Sentry.captureException(error);
        console.log(error);
      }
    }
    dispatch({ type: stopLoader.type });
  };

  return (
    <>
      <StatusBar barStyle="light-content" />
      <NavHeader title="Payments" />
      <View style={styles.wrapper}>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="always"
          nestedScrollEnabled={true}>
          <View style={styles.container}>
            <RNTextView style={styles.titleTxt}>Direct Deposit Info</RNTextView>
            <RNTextView style={styles.subtitleTxt}>
              All Fields are required
              <RNTextView style={{ color: 'red' }}>*</RNTextView>
            </RNTextView>
            <Formik
              onSubmit={() => {}}
              initialValues={{
                fullName: paymentData.fullName,
                dob: '',
                sin: paymentData.sin ? paymentData.sin : '',
                transitNo: paymentData.routingNumber
                  ? paymentData.routingNumber.split('-')[0]
                  : '',
                instNo: paymentData.routingNumber
                  ? paymentData.routingNumber.split('-')[1]
                  : '',
                accNo: paymentData.accountNumber
                  ? `${paymentData.accountNumber}`
                  : '',
              }}
              validationSchema={yup.object().shape({
                fullName: yup.string().label('Full Name').min(5).required(),
                dob: yup.string().label('Date of birth').required(),
                sin: yup
                  .string()
                  .required()
                  .label('Social insurance number')
                  .matches(/^[0-9]+$/, 'Must be only digits')
                  .min(9, 'Must be minimum 9 digits')
                  .max(11, 'Must be maximum 11 digits'),
                transitNo: yup
                  .string()
                  .label('Transit number')
                  .matches(/^[0-9]{5}$/, 'Must be exactly 5 digits')
                  .required(),
                instNo: yup
                  .string()
                  .label('Inst number')
                  .matches(/^[0-9]{3}$/, 'Must be exactly 3 digits')
                  .required(),
                accNo: yup
                  .string()
                  .label('Account number')
                  .matches(/^[0-9]{12}$/, 'Must be exactly 12 digits')
                  .required(),
              })}>
              {({
                values,
                handleChange,
                errors,
                setFieldTouched,
                touched,
                isValid,
                validateForm,
                dirty,
              }: any) => (
                <>
                  {data.map(item => (
                    <>
                      {item.key !== 2 && item.key !== 4 && item.key !== 5 && (
                        <CustomTextInput
                          keyboardType={
                            item.keyboardType
                              ? (item.keyboardType as KeyboardType)
                              : 'default'
                          }
                          key={item.key.toString()}
                          onBlur={() => setFieldTouched(item.name)}
                          onFocus={() => validateForm()}
                          onChangeText={handleChange(item.name)}
                          placeholderTxt={item.placeholder}
                          touched={touched}
                          secureTextEntry={item.secureEntry}
                          name={item.name}
                          errors={errors}
                          customStyles={{ marginBottom: 20 }}
                          value={values[item.name]}
                        />
                      )}
                      {item.key === 2 && (
                        <AppDatePicker
                          containerStyle={{
                            marginVertical: 0,
                            marginBottom: 20,
                          }}
                          defaultDate={
                            paymentData.dobDay
                              ? `${paymentData.dobYear}${utils.zeroPad(
                                  paymentData.dobMonth,
                                  2,
                                )}${utils.zeroPad(paymentData.dobDay, 2)}`
                              : ''
                          }
                          label="Date of Birth"
                          customStyle={{
                            backgroundColor: 'rgba(84, 101, 233, 0.04)',
                          }}
                          onDateSelection={handleChange(item.name)}
                        />
                      )}
                    </>
                  ))}
                  <View style={styles.depositRowContainer}>
                    {data.map(
                      item =>
                        (item.key === 4 || item.key === 5) && (
                          <CustomTextInput
                            keyboardType={
                              item.keyboardType
                                ? (item.keyboardType as KeyboardType)
                                : 'default'
                            }
                            key={item.key.toString()}
                            onBlur={() => setFieldTouched(item.name)}
                            onFocus={() => validateForm()}
                            onChangeText={handleChange(item.name)}
                            placeholderTxt={item.placeholder}
                            touched={touched}
                            secureTextEntry={item.secureEntry}
                            name={item.name}
                            errors={errors}
                            customStyles={
                              item.key === 4
                                ? { marginRight: 10, marginBottom: 20 }
                                : { marginBottom: 20 }
                            }
                            value={values[item.name]}
                          />
                        ),
                    )}
                  </View>
                  <RNTextView style={styles.appTxt}>Billing Address</RNTextView>
                  <CustomLocationSearch
                    onSearch={handleLocationSearch}
                    address={{ fullAddress: paymentData.address.address }}
                    disableMap={true}
                  />
                  <CustomButton
                    title="SUBMIT"
                    disabled={!isValid || !dirty || isDisabled}
                    onPress={() => handleSubmit(values)}
                    customStyles={styles.btn}
                    enabledColor={colors.primary}
                  />
                </>
              )}
            </Formik>
          </View>
        </KeyboardAwareScrollView>
      </View>
    </>
  );
};

export default Payments;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: colors.white,
  },
  container: {
    flex: 1,
    padding: 20,
  },
  depositRowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: Dimensions.get('window').width - 215,
  },
  btn: {
    marginVertical: 20,
  },
  appTxt: {
    fontFamily: 'Apercu-Regular',
    fontSize: 16,
    color: colors.enabledTxtInput,
    marginBottom: -5,
  },
  titleTxt: {
    fontFamily: 'Apercu-Bold',
    fontWeight: '500',
    fontSize: 24,
    color: colors.textInputBorder,
  },
  subtitleTxt: {
    fontSize: 13,
    opacity: 0.6,
    marginBottom: 20,
  },
});
