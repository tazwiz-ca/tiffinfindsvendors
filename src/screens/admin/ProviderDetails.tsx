import React, { useState, useLayoutEffect } from 'react';
import { View, StyleSheet, StatusBar, Platform } from 'react-native';
import { useDispatch } from 'react-redux';
import _ from 'lodash';
import * as Sentry from '@sentry/react-native';
// @ts-ignore: This module does'nt have type definitions
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import { useFocusEffect } from '@react-navigation/native';
import { useRoute, useNavigation } from '@react-navigation/core';

import DeliveryPickup from '../../components/admin/DeliveryPickup';
import ChoosePlans from '../../components/admin/ChoosePlans';
import DeliverySlots from '../../components/admin/DeliverySlots';
import MealPlans from '../../components/admin/MealPlans';
import ProductHeader from '../../components/admin/ProductHeader';
import CustomBackButton from '../../components/admin/CustomBackButton';
import StickyHeader from '../../components/admin/StickyHeader';
import { startLoader, stopLoader } from '../../store/reducers/common/loader';
import { fetchProviderById } from '../../network/AdminService';

type LocationData = {
  address: string;
  fullAddress: string;
  coordinates: {
    type: string;
    coordinates: Array<number>;
  };
};

type PriceData = {
  frequency: string;
  value: string;
  nos: number;
};

type IndivMenuData = {
  type: string;
  price: Array<PriceData>;
  days: {
    [key: string]: string;
  };
};

type MenuData = {
  mealCuisines: Array<string>;
  menu: {
    [key: string]: IndivMenuData;
  };
};

type DetailsModel = {
  deliveryFrequency: Array<string>;
  deliveryTimes: Array<string>;
  images: Array<string>;
  providerName: string;
  typeOfProvider: string;
  location: LocationData;
  doDelivery: boolean;
  allowPickup: boolean;
  deliveryRadius: number;
  menu: MenuData;
};

export default function ProviderDetails() {
  const route = useRoute();
  if (!route.params) return <></>;
  const navigation = useNavigation();
  const { providerId } = route?.params as { [key: string]: string };

  const dispatch = useDispatch();
  const [details, setDetails] = useState<DetailsModel>();
  const [isDelivery, setIsDelivery] = useState(true);

  useFocusEffect(
    React.useCallback(() => {
      if (Platform.OS === 'android') {
        StatusBar.setTranslucent(true);
        StatusBar.setBackgroundColor('transparent');
      }
      StatusBar.setBarStyle('dark-content', true);
    }, []),
  );

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => <CustomBackButton />,
    });
    fetchProductDetails();
  }, []);

  const fetchProductDetails = async () => {
    try {
      dispatch({ type: startLoader.type });
      const request = {
        providerId,
      };
      const data = await fetchProviderById(request);
      //Check if the provider only has pickup and no delivery
      if (!data.doDelivery && data.allowPickup) {
        setIsDelivery(false);
      }
      setDetails(data);
    } catch (error) {
      Sentry.captureException(error);
      console.log(error);
    }
    dispatch({ type: stopLoader.type });
  };

  const onDeliveryTabChange = (num: number) => {
    setIsDelivery(num === 0);
  };

  if (_.isEmpty(details)) {
    return <></>;
  }
  return (
    <>
      <StatusBar
        backgroundColor="transparent"
        translucent
        barStyle="dark-content"
      />
      <View style={styles.container}>
        <ParallaxScrollView
          bounces={false}
          scrollIndicatorInsets={{ right: 1 }}
          backgroundColor="white"
          contentBackgroundColor="white"
          stickyHeaderHeight={103}
          parallaxHeaderHeight={Platform.OS === 'android' ? 300 : 320}
          renderStickyHeader={() => (
            <StickyHeader providerName={details!.providerName} />
          )}
          renderForeground={() => <ProductHeader product={details} />}>
          <>
            <View>
              <DeliveryPickup
                defaultVal={isDelivery ? 0 : 1}
                pickUpLoc={details!.location?.fullAddress}
                allowPickup={details!.allowPickup}
                doDelivery={details!.doDelivery}
                onChange={onDeliveryTabChange}
              />
              <MealPlans mealData={Object.values(details!.menu?.menu)} />
              {details?.doDelivery ? (
                <DeliverySlots deliveryData={details.deliveryTimes} />
              ) : (
                <></>
              )}
              <ChoosePlans
                defaultVal={0}
                mealData={Object.values(details!.menu?.menu)[0]}
              />
            </View>
          </>
        </ParallaxScrollView>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingBottom: 30,
  },
});
