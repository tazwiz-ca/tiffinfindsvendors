import React, { FunctionComponent, useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  StatusBar,
  TouchableWithoutFeedback,
  FlatList,
} from 'react-native';
import { useNavigation } from '@react-navigation/core';
import MaterialCommIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useDispatch, useSelector } from 'react-redux';

import colors from '../../configs/colors';
import { logout } from '../../store/reducers/auth';
import helpers from '../../utils/helpers';
import routes from '../../navigations/routes';
import BottomSlideMenu from '../../components/common/BottomSlideMenu';
import { RootState } from '../../store';
import ProviderContent from '../../components/admin/ProviderContent';
import {
  approveVendor,
  fetchAllVendors,
  resetFilter,
} from '../../store/reducers/admin/admin';
import FilterVendors from '../../components/admin/FilterVendors';
import FilterText from '../../components/common/FilterText';
import {
  getInitialNotification,
  notificationForegroundHandler,
  onMessageClicked,
  unsubscribe,
} from '../../notification/NotificationService';

const AdminHome: FunctionComponent = () => {
  const providerData = useSelector(
    (state: RootState) => state.admin.filteredData,
  );
  const filterStatus = useSelector(
    (state: RootState) => state.admin.filterStatus,
  );
  const id = useSelector((state: RootState) => state.auth.vendor.id);
  const token = useSelector((state: RootState) => state.auth.token);
  const [isShowFilter, setShowFilter] = useState(false);
  const [providerName, setProviderName] = useState<string>('');
  const [providerId, setProviderId] = useState<string>('');
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isApprovePop, setApprovePop] = useState(false);
  const [isDeclinePop, setDeclinePop] = useState(false);

  const [isFetching, setIsFetching] = useState(false);
  const navigation = useNavigation();
  const dispatch = useDispatch();

  useEffect(() => {
    onMessageClicked(() => {
      navigation.navigate(routes.ORDERS);
      dispatch(fetchAllVendors(token));
    });
    getInitialNotification().then(() => dispatch(fetchAllVendors(token)));
    dispatch(notificationForegroundHandler(token));
  }, []);

  useEffect(() => {
    setIsFetching(false);
  }, [providerData]);

  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableWithoutFeedback onPress={() => setIsModalVisible(true)}>
          <MaterialCommIcons
            name="logout"
            size={30}
            color={colors.white}
            style={{ marginLeft: 15 }}
          />
        </TouchableWithoutFeedback>
      ),
      headerRight: () => (
        <TouchableWithoutFeedback onPress={() => setShowFilter(true)}>
          <MaterialCommIcons
            name="filter-variant"
            size={30}
            color={colors.white}
            style={{ marginRight: 10 }}
          />
        </TouchableWithoutFeedback>
      ),
    });
  }, []);

  const onApproveClick = (name: string, id: string) => {
    setProviderName(name);
    setProviderId(id);
    setApprovePop(true);
  };

  const handleApprovePop = async () => {
    setApprovePop(false);
    dispatch(approveVendor(token, providerId, 'Y'));
  };

  const onDeclineClick = (name: string, id: string) => {
    setProviderName(name);
    setProviderId(id);
    setDeclinePop(true);
  };

  const handleDeclinePop = async () => {
    setDeclinePop(false);
    dispatch(approveVendor(token, providerId, 'N'));
  };

  const handleLogout = async () => {
    setIsModalVisible(false);
    //Unsubscribe the user from getting notifications for this channel
    await unsubscribe(id);
    dispatch({ type: logout.type });
    await helpers.removeData('vendor');
    navigation.reset({
      index: 0,
      routes: [{ name: routes.AUTH_STACK_NAV }],
    });
  };

  return (
    <>
      <StatusBar barStyle="light-content" />
      <FilterVendors
        modalVisible={isShowFilter}
        onClose={() => setShowFilter(false)}
      />
      <View style={styles.container}>
        <BottomSlideMenu
          visibility={isModalVisible}
          onCancel={handleLogout}
          onSuccess={() => setIsModalVisible(false)}
          title="Logout?"
          subtitle="Are you sure you want to logout?"
          btn1Text="LOGOUT"
          btn2Text="CANCEL"
        />
        <BottomSlideMenu
          visibility={isApprovePop}
          onCancel={handleApprovePop}
          onSuccess={() => setApprovePop(false)}
          title={`Approve ${providerName} Vendor?`}
          subtitle="Are you sure you want to Approve this provider?"
          btn1Text="APPROVE"
          btn2Text="CANCEL"
        />
        <BottomSlideMenu
          visibility={isDeclinePop}
          onCancel={handleDeclinePop}
          onSuccess={() => setDeclinePop(false)}
          title={`Decline ${providerName} Vendor?`}
          subtitle="Are you sure you want to Decline this provider?"
          btn1Text="DECLINE"
          btn2Text="CANCEL"
        />
        <FilterText
          text={filterStatus}
          onPress={() =>
            dispatch({
              type: resetFilter.type,
            })
          }
        />
        <FlatList
          data={providerData}
          refreshing={isFetching}
          onRefresh={() => dispatch(fetchAllVendors(token))}
          renderItem={({ item }: any) => (
            <ProviderContent
              providerId={item.providerId}
              providerName={item.providerName}
              created={item.created}
              uri={{ uri: item.imageUrl }}
              onApprove={onApproveClick}
              onDecline={onDeclineClick}
              isVerified={item.isVerified}
              isMenuAdded={item.isMenuAdded}
              isPaymentInfoAdded={item.isPaymentInfoAdded}
            />
          )}
          ItemSeparatorComponent={() => <View style={styles.divider} />}
          keyExtractor={(_, index) => index.toString()}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    paddingVertical: 20,
  },
  divider: {
    marginBottom: 20,
  },
});
export default AdminHome;
