import {
  View,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  FlatList,
} from 'react-native';
import { useNavigationState } from '@react-navigation/core';
import React, { useEffect, useState, FunctionComponent } from 'react';
import { useNavigation } from '@react-navigation/native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import * as Sentry from '@sentry/react-native';
import { useDispatch, useSelector } from 'react-redux';
import {
  ImagePickerResponse,
  launchImageLibrary,
  MediaType,
} from 'react-native-image-picker';
import FastImage from 'react-native-fast-image';
import Icon from 'react-native-vector-icons/MaterialIcons';

import TabBarLabel from '../../components/common/TabBarLabel';
import routes from '../../navigations/routes';
import colors from '../../configs/colors';
import MealOptions from '../../components/menu/MealOptions';
import MenuFooter from '../../components/menu/MenuFooter';
import Menus from '../../components/menu/Menus';
import { RootState } from '../../store';
import {
  updateMealCuisines,
  updateMealOfferings,
  updateMealRestrictions,
  uploadImage,
  removeImage,
  updateAllergy,
} from '../../store/reducers/menus/menus';
import helpers from '../../utils/helpers';
import RNTextView from '../../components/common/RNTextView';
import { startLoader, stopLoader } from '../../store/reducers/common/loader';
import AppRadioButton from '../../components/common/AppRadioButton';
import utils from '../../utils/helpers';

const MenuInfo: FunctionComponent = () => {
  const state = useNavigationState(state => state);
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const mealOfferings = useSelector((state: RootState) => state.menus)
    .mealOfferings;
  const mealCuisines = useSelector((state: RootState) => state.menus)
    .mealCuisines;
  const mealRestrictions = useSelector((state: RootState) => state.menus)
    .mealRestrictions;
  const image = useSelector((state: RootState) => state.menus.image);
  const isAllergy = useSelector(
    (state: RootState) => state.menus.isAllergyAccomodate,
  );

  const [isDisabled, setIsDisabled] = useState<boolean>(true);
  const [errorText, setErrorText] = useState<string>('');

  useEffect(() => {
    const routeName = state.routeNames[state.index];
    navigation.setOptions({
      tabBarLabel: () => (
        <TabBarLabel
          text="Menu Info"
          focused={routeName === routes.MENU_INFO}
        />
      ),
    });
  }, [state.routeNames[state.index]]);

  //Enable the Next button only if atleast one meal cuisine and meal offering is selected and image uploaded
  useEffect(() => {
    if (
      mealCuisines.filter(item => item.selected).length === 0 ||
      mealOfferings.filter(item => item.selected).length === 0 ||
      Object.keys(image).length === 0
    ) {
      if (Object.keys(image).length === 0)
        setErrorText('Upload an image for your restaurant');
      return setIsDisabled(true);
    }
    setErrorText('');
    setIsDisabled(false);
  }, [mealOfferings, mealCuisines, image]);

  type RemoveImgProps = {
    onPress: () => void;
  };

  const RemoveImage = ({ onPress }: RemoveImgProps) => (
    <View style={styles.removeImg}>
      <TouchableWithoutFeedback onPress={onPress}>
        <FastImage
          source={require('../../assets/img/img-remove.png')}
          style={{ width: 30, height: 30 }}
        />
      </TouchableWithoutFeedback>
    </View>
  );

  type SelectedOfferingProps = {
    text: string;
  };

  const SelectedOfferings = ({ text }: SelectedOfferingProps) => (
    <View style={styles.selectedLangContainer}>
      <RNTextView style={styles.langTxt}>{text}</RNTextView>
      <View style={styles.icContainer}>
        <TouchableWithoutFeedback onPress={() => handleEditMealOffering(text)}>
          <Icon name="edit" size={20} color="grey" />
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() => handleRemoveMealOffering(text)}>
          <Icon name="delete" size={20} color="grey" style={styles.trashImg} />
        </TouchableWithoutFeedback>
      </View>
    </View>
  );

  const handleRemoveMealOffering = (text: string): void => {
    dispatch({ type: updateMealOfferings.type, payload: { text } });
  };

  const handleEditMealOffering = (text: string): void => {
    const selectedItem = mealOfferings.find(item => item.text === text);
    //show popup
    if (selectedItem?.selected) {
      navigation.navigate(routes.ADD_MENUS, {
        popupTitle: 'Update Menu',
        Component: Menus,
        props: {
          offering: text,
        },
      });
    }
  };

  const handleMealOfferingChange = (text: string): void => {
    dispatch({ type: updateMealOfferings.type, payload: { text } });

    const selectedItem = mealOfferings.find(item => item.text === text);
    //show popup
    if (!selectedItem?.selected) {
      navigation.navigate(routes.ADD_MENUS, {
        popupTitle: 'Add Menu',
        Component: Menus,
        props: {
          offering: text,
        },
      });
    }
  };

  const handleCuisineChange = (text: string): void => {
    //dispatch action to update cuisines for menu
    dispatch({ type: updateMealCuisines.type, payload: { text } });
  };

  const handleRestrictionChange = (text: string): void => {
    //dispatch action to update meal restrictions for menu
    dispatch({ type: updateMealRestrictions.type, payload: { text } });
  };

  const handleUploadBtnPress = () => {
    const options = {
      mediaType: 'photo' as MediaType,
    };

    launchImageLibrary(options, async (response: ImagePickerResponse) => {
      try {
        dispatch({ type: startLoader.type });
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.errorMessage) {
          console.log('ImagePicker Error: ', response.errorMessage);
        } else {
          const source = {
            uri: await helpers.getBase64Path(response.uri, response.type),
            url: await helpers.getBase64Path(response.uri, response.type),
          };
          dispatch({ type: uploadImage.type, payload: { image: source } });
        }
      } catch (error) {
        Sentry.captureException(error);
        console.log(error);
      }
      dispatch({ type: stopLoader.type });
    });
  };

  const handleRemoveImage = (): void => {
    dispatch({ type: removeImage.type });
  };

  const handleAllergyUpdate = (selectedIdx: number): void => {
    dispatch({
      type: updateAllergy.type,
      payload: { isAllergy: selectedIdx === 0 },
    });
  };

  const goBack = (): void => {
    navigation.navigate(routes.GENERAL_INFO);
  };

  const handleNext = (): void => {
    navigation.navigate(routes.DELIVERY_INFO);
  };

  return (
    <>
      <View style={styles.wrapper}>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="always"
          nestedScrollEnabled={true}>
          <View style={styles.container}>
            <MealOptions
              isRequired={true}
              titleText="Meal offerings"
              data={mealOfferings}
              onChange={handleMealOfferingChange}
            />

            {/** Meal Offerings */}
            {mealOfferings.filter(item => item.selected).length > 0 && (
              <FlatList
                nestedScrollEnabled={true}
                data={mealOfferings.filter(item => item.selected)}
                ListHeaderComponent={() => (
                  <View>
                    <RNTextView
                      style={styles.selectedMealsTitle}
                      textType="medium">
                      Selected Meal offerings
                    </RNTextView>
                  </View>
                )}
                renderItem={({ item }) => (
                  <SelectedOfferings text={item.text} />
                )}
                keyExtractor={(_, index) => index.toString()}
              />
            )}

            <MealOptions
              isRequired={true}
              titleText="Meal Cuisines"
              data={mealCuisines.map(item => ({
                ...item,
                text: utils.capitalizeFirstLetter(item.text),
              }))}
              onChange={handleCuisineChange}
            />

            <MealOptions
              titleText="Meal Restrictions"
              data={mealRestrictions}
              onChange={handleRestrictionChange}
            />

            {/** Image Upload */}
            <View>
              <RNTextView style={styles.title}>
                Image
                <RNTextView style={{ color: 'red' }}>*</RNTextView>
              </RNTextView>
              <View
                style={[
                  styles.uploadImgContainer,
                  Object.keys(image).length !== 0 && { borderStyle: 'solid' },
                ]}>
                {Object.keys(image).length === 0 ? (
                  <TouchableOpacity
                    style={styles.uploadBtn}
                    onPress={handleUploadBtnPress}>
                    <RNTextView style={styles.uploadTxt}>Upload</RNTextView>
                  </TouchableOpacity>
                ) : (
                  <View>
                    <FastImage source={image} style={styles.uploadImg}>
                      <RemoveImage onPress={handleRemoveImage} />
                    </FastImage>
                  </View>
                )}
              </View>
            </View>

            {/** Food Allergies */}
            <View>
              <RNTextView style={styles.title}>
                Do you accomodate for allergies?
              </RNTextView>
              <AppRadioButton
                index={0}
                text="Yes"
                onSelection={handleAllergyUpdate}
                selectedIdx={isAllergy ? 0 : 1}
              />
              <AppRadioButton
                index={1}
                text="No"
                onSelection={handleAllergyUpdate}
                selectedIdx={isAllergy ? 0 : 1}
              />
            </View>
          </View>
        </KeyboardAwareScrollView>
        <MenuFooter
          handlePress={handleNext}
          goBack={goBack}
          disabled={isDisabled}
          errorText={errorText}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: colors.white,
  },
  container: {
    flex: 1,
    padding: 20,
  },
  titleContainer: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  title: {
    marginTop: 15,
    fontFamily: 'JosefinSans-SemiBold',
    fontSize: 18,
    color: colors.textInputBorder,
  },
  subtitleTxt: {
    fontFamily: 'Apercu-Regular',
    color: colors.textInputBorder,
    opacity: 0.4,
    fontSize: 14,
  },
  footerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingVertical: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
  },
  removeImg: {
    position: 'absolute',
    top: -5,
    right: -5,
  },
  uploadImgContainer: {
    flex: 1,
    justifyContent: 'space-around',
    height: 150,
    borderWidth: 1,
    borderRadius: 4,
    borderStyle: 'dashed',
    borderColor: 'rgba(84, 101, 233, 0.2)',
    marginTop: 10,
  },
  uploadBtn: {
    alignSelf: 'center',
    borderWidth: 2,
    borderColor: '#5465E9',
    borderRadius: 4,
    paddingHorizontal: 25,
    paddingVertical: 10,
  },
  uploadImg: {
    width: '100%',
    height: '100%',
    borderRadius: 4,
  },
  uploadTxt: {
    color: '#5465E9',
    fontSize: 14,
    fontFamily: 'Apercu-Medium',
  },
  langTxt: {
    fontFamily: 'Apercu-Regular',
    fontSize: 18,
    color: colors.textInputBorder,
  },
  trashImg: {
    marginLeft: 20,
  },
  editImg: {
    width: 20,
    height: 20,
  },
  selectedLangContainer: {
    borderWidth: 1,
    borderColor: colors.taskerPrimary,
    paddingVertical: 5,
    paddingHorizontal: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  icContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  selectedMealsTitle: {
    fontFamily: 'Apercu-Bold',
    fontSize: 20,
    color: colors.textInputBorder,
    textAlign: 'center',
    marginBottom: 10,
  },
});

export default MenuInfo;
