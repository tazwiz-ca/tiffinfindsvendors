import { useNavigationState } from '@react-navigation/core';
import React, { useEffect, useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import { View, StyleSheet } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useDispatch, useSelector } from 'react-redux';
import * as Sentry from '@sentry/react-native';

import TabBarLabel from '../../components/common/TabBarLabel';
import routes from '../../navigations/routes';
import MenuFooter from '../../components/menu/MenuFooter';
import colors from '../../configs/colors';
import { RootState } from '../../store';
import {
  updateAdditionalMessages,
  updateDeliverySlots,
  updateKitchenCapacity,
  updateMenuCustomization,
} from '../../store/reducers/menus/additional';
import RNTextView from '../../components/common/RNTextView';
import AppRadioButton from '../../components/common/AppRadioButton';
import MealOptions from '../../components/menu/MealOptions';
import MealPrice from '../../components/menu/MealPrice';
import FormTextInput from '../../components/menu/FormTextInput';
import { frameMenuRequestBody } from '../../store/reducers/helpers/reducerUtil';
import { createOrUpdateMenu } from '../../network/MenuService';
import { startLoader, stopLoader } from '../../store/reducers/common/loader';
import { errorToast, successToast } from '../../store/reducers/common/toaster';

const AdditionalInfo = () => {
  const state = useNavigationState(state => state);
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const additionalData = useSelector((state: RootState) => state.additional);
  const deliveryData = useSelector((state: RootState) => state.delivery);
  const generalData = useSelector((state: RootState) => state.general);
  const menuData = useSelector((state: RootState) => state.menus);
  const authData = useSelector((state: RootState) => state.auth);

  const [isDisabled, setIsDisabled] = useState<boolean>(true);
  const [errorText, setErrorText] = useState<string>('');

  useEffect(() => {
    const routeName = state.routeNames[state.index];
    navigation.setOptions({
      tabBarLabel: () => (
        <TabBarLabel
          text="Additional Info"
          focused={routeName === routes.ADDITIONAL_INFO}
        />
      ),
    });
  }, [state.routeNames[state.index]]);

  {
    /** Validation before enabling the submit button */
  }
  useEffect(() => {
    //Check if delivery time slots are chosen when delivery preference selected is yes
    if (
      additionalData.deliverySlots.filter(item => item.selected).length === 0 &&
      deliveryData.isDelivery
    ) {
      setErrorText(
        'Select atleast one delivery slot when you have chosen to deliver in preference',
      );
      return setIsDisabled(true);
    }

    //Check kitchen capacity
    if (additionalData.kitchenCapacity === 0) {
      setErrorText('Kitchen capacity cannot be 0');
      return setIsDisabled(true);
    }

    setIsDisabled(false);
    setErrorText('');
  }, [additionalData]);

  const handleMenuCustomization = (index: number): void => {
    dispatch({
      type: updateMenuCustomization.type,
      payload: { isMenuCustomize: index === 0 },
    });
  };

  const handleDeliverySlotChange = (text: string): void => {
    dispatch({
      type: updateDeliverySlots.type,
      payload: { slot: text },
    });
  };

  const handleKitchenCapacityChange = (text: string): void => {
    dispatch({
      type: updateKitchenCapacity.type,
      payload: { capacity: text ? parseFloat(text) : 0 },
    });
  };

  const handleAditionalMsgChange = (text: string): void => {
    dispatch({
      type: updateAdditionalMessages.type,
      payload: { message: text },
    });
  };
  const handleSubmit = async () => {
    try {
      dispatch({ type: startLoader.type });
      //Frame the request
      const request = frameMenuRequestBody(
        menuData,
        generalData,
        deliveryData,
        additionalData,
        authData,
      );

      //call api and update
      if (request && request.menuImages) {
        const data = await createOrUpdateMenu(authData.token, request);
        if (data) {
          dispatch({
            type: successToast.type,
            payload: {
              message: data.msg,
            },
          });
        }
      }
    } catch (error) {
      Sentry.captureException(error);
      dispatch({
        type: errorToast.type,
        payload: { message: error?.message ? error.message : error.toString() },
      });

      console.log(error);
    }
    dispatch({ type: stopLoader.type });
  };

  const goBack = () => {
    navigation.navigate(routes.DELIVERY_INFO);
  };
  return (
    <>
      <View style={styles.wrapper}>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="always"
          nestedScrollEnabled={true}>
          <View style={styles.container}>
            {/** Menu customizable preference */}
            <RNTextView style={styles.title}>
              Is your Menu customizable?
            </RNTextView>
            <AppRadioButton
              index={0}
              text="Yes"
              onSelection={handleMenuCustomization}
              selectedIdx={additionalData.isMenuCustomizable ? 0 : 1}
            />
            <AppRadioButton
              index={1}
              text="No"
              onSelection={handleMenuCustomization}
              selectedIdx={additionalData.isMenuCustomizable ? 0 : 1}
            />

            {/** Delivery time slot */}
            {deliveryData.isDelivery && (
              <MealOptions
                isRequired={true}
                titleText="Delivery Time slots"
                data={additionalData.deliverySlots}
                onChange={handleDeliverySlotChange}
                containerStyle={{ marginVertical: 20 }}
              />
            )}

            {/** Kitchen capacity */}
            <MealPrice
              title="Kitchen capacity"
              value={additionalData.kitchenCapacity}
              onChange={handleKitchenCapacityChange}
              customTitleStyle={[
                styles.title,
                { textAlign: 'left', marginTop: 0 },
              ]}
              isRequired={true}
              containerStyle={{ marginVertical: 20 }}
            />

            {/** Additional messages */}
            <FormTextInput
              value={additionalData.additionalMessages}
              isRequired={false}
              textTitle="Additional messages"
              placeholder="Enter message for the customer"
              onChange={handleAditionalMsgChange}
              containerStyle={{ marginVertical: 10 }}
            />
          </View>
        </KeyboardAwareScrollView>
      </View>
      <MenuFooter
        firstBtnText="Submit"
        handlePress={handleSubmit}
        goBack={goBack}
        disabled={isDisabled}
        errorText={errorText}
      />
    </>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: colors.white,
  },
  container: {
    flex: 1,
    padding: 20,
  },
  titleContainer: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  title: {
    marginTop: 15,
    fontFamily: 'JosefinSans-SemiBold',
    fontSize: 18,
    color: colors.textInputBorder,
  },
});

export default AdditionalInfo;
