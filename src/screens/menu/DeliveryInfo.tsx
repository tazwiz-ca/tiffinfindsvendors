import { View, StyleSheet } from 'react-native';
import { useNavigationState } from '@react-navigation/core';
import React, { useEffect, useState } from 'react';
import { useNavigation } from '@react-navigation/native';

import TabBarLabel from '../../components/common/TabBarLabel';
import routes from '../../navigations/routes';
import MenuFooter from '../../components/menu/MenuFooter';
import colors from '../../configs/colors';
import RNTextView from '../../components/common/RNTextView';
import AppRadioButton from '../../components/common/AppRadioButton';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import MealOptions from '../../components/menu/MealOptions';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../store';
import {
  updateDeliveryFrequency,
  updateDeliveryPreference,
  updateDeliveryRadius,
  updatePickupPreference,
} from '../../store/reducers/menus/delivery';
import MealPrice from '../../components/menu/MealPrice';
import utils from '../../utils/helpers';

const DeliveryInfo = () => {
  const state = useNavigationState(state => state);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const deliveryData = useSelector((state: RootState) => state.delivery);

  const [isDisabled, setIsDisabled] = useState<boolean>(true);
  const [errorText, setErrorText] = useState<string>('');

  useEffect(() => {
    const routeName = state.routeNames[state.index];
    navigation.setOptions({
      tabBarLabel: () => (
        <TabBarLabel
          text="Delivery Info"
          focused={routeName === routes.DELIVERY_INFO}
        />
      ),
    });
  }, [state.routeNames[state.index]]);

  {
    /** Validations for enabling the next button */
  }
  useEffect(() => {
    if (
      deliveryData.deliveryFrequency.filter((item: any) => item.selected)
        .length === 0 &&
      deliveryData.isDelivery
    ) {
      setErrorText('Atleast One day should be selected for delivery frequency');
      return setIsDisabled(true);
    }

    //Check the delivery radius
    if (deliveryData.deliveryRadius === 0 && deliveryData.isDelivery) {
      setErrorText('Delivery Radius cannot be 0');
      return setIsDisabled(true);
    }

    //Check if both delivery and pickup are marked as no
    if (!deliveryData.isDelivery && !deliveryData.isPickup) {
      setErrorText('Both delivery and pickup cannot be set as No');
      return setIsDisabled(true);
    }
    setErrorText('');
    setIsDisabled(false);
  }, [deliveryData]);

  const handleDeliveryChange = (index: number): void => {
    dispatch({
      type: updateDeliveryPreference.type,
      payload: { deliveryPreference: index === 0 },
    });
  };

  const handleDeliveryFreqChange = (text: string) => {
    dispatch({
      type: updateDeliveryFrequency.type,
      payload: { day: text },
    });
  };

  const handleDeliveryRadiusChange = (text: string) => {
    dispatch({
      type: updateDeliveryRadius.type,
      payload: { radius: text ? parseFloat(text) : 0 },
    });
  };

  const handlePickupChange = (index: number): void => {
    dispatch({
      type: updatePickupPreference.type,
      payload: { pickupPreference: index === 0 },
    });
  };

  const goBack = () => {
    navigation.navigate(routes.MENU_INFO);
  };

  const handleNext = () => {
    navigation.navigate(routes.ADDITIONAL_INFO);
  };

  return (
    <>
      <View style={styles.wrapper}>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="always"
          nestedScrollEnabled={true}>
          <View style={styles.container}>
            {/** Delivery preference */}
            <RNTextView style={styles.title}>
              Do you deliver to the customer?
            </RNTextView>
            <AppRadioButton
              index={0}
              text="Yes"
              onSelection={handleDeliveryChange}
              selectedIdx={deliveryData.isDelivery ? 0 : 1}
            />
            <AppRadioButton
              index={1}
              text="No"
              onSelection={handleDeliveryChange}
              selectedIdx={deliveryData.isDelivery ? 0 : 1}
            />

            {/** Delivery frequency */}
            {deliveryData.isDelivery && (
              <>
                <MealOptions
                  isRequired={true}
                  titleText="Delivery Frequency"
                  data={deliveryData.deliveryFrequency.map(item => ({
                    ...item,
                    text: utils.capitalizeFirstLetter(item.text),
                  }))}
                  onChange={handleDeliveryFreqChange}
                />

                {/** Delivery Radius */}
                <MealPrice
                  title="Delivery Radius"
                  value={deliveryData.deliveryRadius}
                  onChange={handleDeliveryRadiusChange}
                  customTitleStyle={[
                    styles.title,
                    { textAlign: 'left', marginTop: 0 },
                  ]}
                  isRequired={true}
                />
              </>
            )}

            {/** Pickup preference */}
            <RNTextView style={[styles.title, { marginTop: 20 }]}>
              Do you offer to pickup the food?
            </RNTextView>
            <AppRadioButton
              index={0}
              text="Yes"
              onSelection={handlePickupChange}
              selectedIdx={deliveryData.isPickup ? 0 : 1}
            />
            <AppRadioButton
              index={1}
              text="No"
              onSelection={handlePickupChange}
              selectedIdx={deliveryData.isPickup ? 0 : 1}
            />
          </View>
        </KeyboardAwareScrollView>
      </View>
      <MenuFooter
        handlePress={handleNext}
        goBack={goBack}
        disabled={isDisabled}
        errorText={errorText}
      />
    </>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: colors.white,
  },
  container: {
    flex: 1,
    padding: 20,
  },
  titleContainer: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  title: {
    marginTop: 15,
    fontFamily: 'JosefinSans-SemiBold',
    fontSize: 18,
    color: colors.textInputBorder,
  },
});

export default DeliveryInfo;
