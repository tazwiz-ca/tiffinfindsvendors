import { useNavigationState } from '@react-navigation/core';
import React, { useEffect, useState } from 'react';
import { StyleSheet, View, StatusBar, Platform } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useDispatch, useSelector } from 'react-redux';

import TabBarLabel from '../../components/common/TabBarLabel';
import routes from '../../navigations/routes';
import RNTextView from '../../components/common/RNTextView';
import colors from '../../configs/colors';
import CustomLocationSearch from '../../components/menu/CustomLocationSearch';
import FormTextInput from '../../components/menu/FormTextInput';
import MenuFooter from '../../components/menu/MenuFooter';
import { RootState } from '../../store';
import {
  updateRestaurantName,
  updateResturantLocation,
  updateIsCertified,
  updateIsGoodStanding,
} from '../../store/reducers/menus/general';
import AppRadioButton from '../../components/common/AppRadioButton';

const GeneralInfo = () => {
  const state = useNavigationState(state => state);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const generalData = useSelector((state: RootState) => state.general);

  const [isDisabled, setIsDisabled] = useState<boolean>(true);
  const [errorText, setErrorText] = useState<string>('');

  useEffect(() => {
    if (Platform.OS == 'android') {
      StatusBar.setTranslucent(false);
    }

    const routeName = state.routeNames[state.index];
    navigation.setOptions({
      tabBarLabel: () => (
        <TabBarLabel
          text="General Info"
          focused={routeName === routes.GENERAL_INFO}
        />
      ),
    });
  }, [state.routeNames[state.index]]);

  {
    /** Validations for enabling next button */
  }
  useEffect(() => {
    //Name check
    if (!generalData.restaurantName) {
      setErrorText('Name of the restaurant is mandatory');
      return setIsDisabled(true);
    }
    //Location check
    if (!generalData.location?.address) {
      setErrorText('Choose the location of your restaurant');
      return setIsDisabled(true);
    }
    setIsDisabled(false);
    setErrorText('');
  }, [generalData]);

  const handleRestrntNameChange = (name: string) => {
    dispatch({
      type: updateRestaurantName.type,
      payload: { name },
    });
  };

  const handleLocationSearch = (locationTxt: string, locationParams: any) => {
    dispatch({
      type: updateResturantLocation.type,
      payload: {
        address: locationTxt,
        coords: locationParams,
      },
    });
  };

  const handleCertified = (index: number) => {
    dispatch({
      type: updateIsCertified.type,
      payload: {
        isCertified: index === 0,
      },
    });
  };

  const handleGoodStanding = (index: number) => {
    dispatch({
      type: updateIsGoodStanding.type,
      payload: {
        isGoodStanding: index === 0,
      },
    });
  };

  const handleNext = () => {
    navigation.navigate(routes.MENU_INFO);
  };

  const LocationViewContents = () => (
    <>
      <View style={styles.titleContainer}>
        <RNTextView style={styles.title}>
          Location
          <RNTextView style={{ color: 'red' }}>*</RNTextView>
        </RNTextView>
      </View>
      <CustomLocationSearch
        onSearch={handleLocationSearch}
        address={generalData.location.address}
      />
    </>
  );

  return (
    <>
      <StatusBar barStyle="light-content" />
      <View style={styles.wrapper}>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="always"
          nestedScrollEnabled={true}>
          <View style={styles.container}>
            <FormTextInput
              value={generalData.restaurantName}
              isRequired={true}
              textTitle="Restaurant Name"
              placeholder="Enter your restaurant name"
              onChange={handleRestrntNameChange}
            />

            {LocationViewContents()}

            {/** Is certified */}
            <View style={styles.radioBtnContainer}>
              <RNTextView style={styles.title}>
                Are you certified and licensed by the provincial to handle food?
              </RNTextView>
              <AppRadioButton
                index={0}
                text="Yes"
                onSelection={handleCertified}
                selectedIdx={generalData.isCertified ? 0 : 1}
              />
              <AppRadioButton
                index={1}
                text="No"
                onSelection={handleCertified}
                selectedIdx={generalData.isCertified ? 0 : 1}
              />
            </View>

            {/** In good standing  */}
            <RNTextView style={styles.title}>
              Are you in good standing with the local public health department?
            </RNTextView>
            <AppRadioButton
              index={0}
              text="Yes"
              onSelection={handleGoodStanding}
              selectedIdx={generalData.isGoodStanding ? 0 : 1}
            />
            <AppRadioButton
              index={1}
              text="No"
              onSelection={handleGoodStanding}
              selectedIdx={generalData.isGoodStanding ? 0 : 1}
            />
          </View>
        </KeyboardAwareScrollView>
      </View>
      <MenuFooter
        handlePress={handleNext}
        disabled={isDisabled}
        errorText={errorText}
      />
    </>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: colors.white,
  },
  container: {
    flex: 1,
    padding: 20,
  },
  titleContainer: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  radioBtnContainer: {
    marginVertical: 20,
  },
  title: {
    fontFamily: 'JosefinSans-SemiBold',
    fontSize: 18,
    color: colors.textInputBorder,
  },
  subtitleTxt: {
    fontFamily: 'Apercu-Regular',
    color: colors.textInputBorder,
    opacity: 0.4,
    fontSize: 14,
  },
});

export default GeneralInfo;
