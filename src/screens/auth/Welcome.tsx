import React, { useEffect, FunctionComponent } from 'react';
import {
  StatusBar,
  StyleSheet,
  View,
  Text,
  Linking,
  Platform,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import * as Sentry from '@sentry/react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import CustomButton from '../../components/common/CustomButton';
import routes from '../../navigations/routes';
import { RootState } from '../../store';

import RNTextView from '../../components/common/RNTextView';
import colors from '../../configs/colors';
import { errorToast } from '../../store/reducers/common/toaster';
import { onNavigateFromMail } from '../../store/reducers/auth';

const Welcome: FunctionComponent = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const resetPwd = useSelector((state: RootState) => state.auth.resetPwd);

  useFocusEffect(
    React.useCallback(() => {
      if (Platform.OS === 'android') {
        StatusBar.setTranslucent(true);
        StatusBar.setBackgroundColor('transparent');
      }
      StatusBar.setBarStyle('light-content', true);
    }, []),
  );
  /** When the vendor opens the email from his mobile and clicks on link
   * and when the store is updated with email and token
   * Navigate him to reset password screen */
  useEffect(() => {
    if (resetPwd.email && resetPwd.token) {
      navigation.navigate(routes.RESET_PWD);
    }
  }, [resetPwd]);

  useEffect(() => {
    //If the initial URL when the app opens for the first time is from linking navigate to pre confirmation screen based on the url params
    (async function () {
      try {
        const url = await Linking.getInitialURL();
        if (url) handleOpenUrl({ url });
      } catch (error) {
        Sentry.captureException(error);
        console.log(error);
      }
    })();
    // Add event listener if the app is in foreground
    Linking.addEventListener('url', handleOpenUrl);
    //Remove the subscription during unmount
    return () => {
      Linking.removeEventListener('url', handleOpenUrl);
    };
  }, []);

  type URL = {
    url: string;
  };

  const handleOpenUrl = ({ url }: URL) => {
    if (resetPwd.email) return;
    try {
      if (!url) return;
      //tiffinfindvendors://reset/:email/:token
      const uri = url.split('//')[1];
      if (!uri) return;

      //reset/:email/:token
      const email = uri.split('/')[1];
      const token = uri.split('/')[2];
      // After verification the user will be in the same screen to login
      if (email && token) {
        dispatch({ type: onNavigateFromMail.type, payload: { email, token } });
      } else {
        dispatch({
          type: errorToast.type,
          payload: { message: 'email/token not found in the url' },
        });
      }
    } catch (error) {
      Sentry.captureException(error);
      dispatch({
        type: errorToast.type,
        payload: { message: error?.toString() },
      });
    }
  };
  const navigateTo = (screen: string) => () => {
    navigation.navigate(screen);
  };

  const LogoContainer: FunctionComponent = () => (
    <View>
      <View style={styles.logoContainer}>
        <FastImage
          source={require('../../assets/img/logo.png')}
          style={styles.logoImg}
        />
      </View>
      <RNTextView style={styles.logoTxt} textType={'medium'}>
        TiffinFinds
      </RNTextView>
    </View>
  );

  const ButtonGroups: FunctionComponent = () => (
    <View>
      <CustomButton
        title="CREATE AN ACCOUNT"
        enabledColor="#B90039"
        customStyles={styles.signupBtn}
        onPress={navigateTo(routes.SIGN_UP)}
      />
      <CustomButton
        title="I ALREADY HAVE AN ACCOUNT"
        enabledColor="transparent"
        customStyles={styles.loginBtn}
        customTextStyles={{
          textShadowColor: 'rgba(0, 0, 0, 0.75)',
          textShadowOffset: { width: -1, height: 1 },
          textShadowRadius: 4,
        }}
        onPress={navigateTo(routes.SIGN_IN)}
      />
    </View>
  );

  return (
    <>
      <StatusBar
        backgroundColor="transparent"
        translucent
        barStyle="light-content"
      />
      <View style={styles.screenContainer}>
        <FastImage
          source={require('../../assets/img/welcome_screen.png')}
          style={styles.imgBg}>
          <View style={styles.imgBgContainer}>
            <LogoContainer />
            <Text style={styles.welcomeTxt}>
              Welcome to TiffinFinds Vendors
            </Text>
            <ButtonGroups />
          </View>
        </FastImage>
      </View>
    </>
  );
};

export default Welcome;

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    //backgroundColor: Colors.background,
  },
  imgBg: {
    width: '100%',
    height: '100%',
  },
  logoContainer: {
    backgroundColor: 'black',
    width: 70,
    height: 70,
    borderRadius: 35,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoImg: {
    width: 30,
    height: 40,
    alignSelf: 'center',
  },
  logoTxt: {
    fontWeight: '600',
    color: colors.white,
    marginTop: 5,
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 4,
  },
  imgBgContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  signupBtn: {
    width: 325,
  },
  loginBtn: {
    backgroundColor: 'transparent',
    borderWidth: 2,
    borderColor: 'white',
    paddingHorizontal: 20,
    fontSize: 14,
    fontWeight: '500',
  },
  skipBtn: {
    backgroundColor: 'transparent',
    width: 325,
  },
  welcomeTxt: {
    fontSize: 24,
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
    textShadowRadius: 16,
    textShadowColor: 'rgba(0, 0, 0, 0.56)',
  },
});
