import React from 'react';
import * as yup from 'yup';
import { Formik } from 'formik';
import { View, StyleSheet, KeyboardType } from 'react-native';
import FastImage from 'react-native-fast-image';
import * as Sentry from '@sentry/react-native';
import { useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/core';

import RNTextView from '../../components/common/RNTextView';
import colors from '../../configs/colors';
import CustomTextInput from '../../components/common/CustomTextInput';
import CustomButton from '../../components/common/CustomButton';
import routes from '../../navigations/routes';
import { startLoader, stopLoader } from '../../store/reducers/common/loader';
import { errorToast } from '../../store/reducers/common/toaster';
import { sendEmailForgotPwd } from '../../network/AuthService';

export default function ForgotPassword() {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const data = [
    {
      key: 1,
      placeholder: 'E-mail',
      name: 'email',
      keyboardType: 'email-address',
    },
  ];

  type ResetLink = {
    email: string;
  };

  const handleResetLink = async ({ email }: ResetLink) => {
    try {
      dispatch({ type: startLoader.type });
      await sendEmailForgotPwd(email);
      dispatch({ type: stopLoader.type });
      navigation.navigate(routes.FORGOT_PWD_SUCCESS, { email });
    } catch (error) {
      dispatch({ type: stopLoader.type });
      Sentry.captureException(error);
      dispatch({
        type: errorToast.type,
        payload: { message: error?.toString() },
      });
    }
  };

  return (
    <View style={styles.container}>
      <RNTextView style={styles.titleTxt}>
        Enter your registration email address to receive your reset password
        link
      </RNTextView>
      <FastImage
        source={require('../../assets/img/forgot-pwd.png')}
        style={styles.img}
      />
      <Formik
        onSubmit={() => {}}
        enableReinitialize={true}
        initialValues={{
          email: '',
        }}
        validationSchema={yup.object().shape({
          email: yup.string().email().label('Email').required(),
        })}>
        {({
          values,
          handleChange,
          errors,
          setFieldTouched,
          touched,
          isValid,
          validateForm,
          dirty,
        }: any) => (
          <>
            {data.map(item => (
              <CustomTextInput
                keyboardType={
                  item.keyboardType
                    ? (item.keyboardType as KeyboardType)
                    : 'default'
                }
                key={item.key.toString()}
                onBlur={() => setFieldTouched(item.name)}
                onFocus={() => validateForm()}
                onChangeText={handleChange(item.name)}
                placeholderTxt={item.placeholder}
                touched={touched}
                name={item.name}
                errors={errors}
                customStyles={{ marginBottom: 10 }}
                value={values[item.name]}
              />
            ))}
            <CustomButton
              title="GET RESET LINK"
              enabledColor="#B90039"
              onPress={() => handleResetLink(values)}
              disabled={!isValid || !dirty}
              customStyles={{ marginTop: 20 }}
            />
          </>
        )}
      </Formik>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    padding: 20,
    paddingTop: 40,
  },
  titleTxt: {
    fontSize: 16,
    opacity: 0.8,
    textAlign: 'center',
  },
  img: {
    width: 164,
    height: 164,
    alignSelf: 'center',
    marginTop: 43,
    marginBottom: 32,
  },
});
