import React from 'react';
import { Formik } from 'formik';
import * as yup from 'yup';
import { View, StyleSheet, KeyboardType } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import FastImage from 'react-native-fast-image';
import * as Sentry from '@sentry/react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/core';

import RNTextView from '../../components/common/RNTextView';
import CustomButton from '../../components/common/CustomButton';
import CustomTextInput from '../../components/common/CustomTextInput';
import colors from '../../configs/colors';
import routes from '../../navigations/routes';
import { RootState } from '../../store';
import { startLoader, stopLoader } from '../../store/reducers/common/loader';
import { errorToast } from '../../store/reducers/common/toaster';
import { resetPassword } from '../../network/AuthService';

type ResetPasswordType = {
  password: string;
};

export default function ResetPassword() {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const resetPwd = useSelector((state: RootState) => state.auth.resetPwd);
  const data = [
    {
      key: 1,
      placeholder: 'Password',
      name: 'password',
      secureEntry: true,
      keyboardType: 'default',
    },
    {
      key: 2,
      placeholder: 'Confirm Password',
      name: 'confirmPwd',
      secureEntry: true,
      keyboardType: 'default',
    },
  ];

  const handleResetPwd = async ({ password }: ResetPasswordType) => {
    try {
      const { email, token } = resetPwd;
      dispatch({ type: startLoader.type });
      await resetPassword(email, token, password);
      dispatch({ type: stopLoader.type });
      navigation.navigate(routes.RESET_PWD_SUCCESS);
    } catch (error) {
      Sentry.captureException(error);
      dispatch({
        type: errorToast.type,
        payload: { message: error?.toString() },
      });
      dispatch({ type: stopLoader.type });
    }
  };
  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView>
        <RNTextView style={styles.titleTxt}>
          Enter your registration email address to receive your reset password
          link
        </RNTextView>
        <FastImage
          source={require('../../assets/img/reset-pwd.png')}
          style={styles.img}
        />
        <Formik
          onSubmit={() => {}}
          enableReinitialize={true}
          initialValues={{
            password: '',
            confirmPwd: '',
          }}
          validationSchema={yup.object().shape({
            password: yup
              .string()
              .label('Password')
              .matches(
                /^(?=.*\d)(?=.*[@$!%*#?&_])[A-Za-z\d@$!%*#?&_]{8,}$/,
                'Must Contain 8 Characters, One Number and one special case Character',
              )
              .required(),
            confirmPwd: yup
              .string()
              .label('Confirm Password')
              .oneOf([yup.ref('password'), null], 'Passwords must match')
              .required(),
          })}>
          {({
            values,
            handleChange,
            errors,
            setFieldTouched,
            touched,
            isValid,
            validateForm,
            dirty,
          }: any) => (
            <>
              {data.map(item => (
                <CustomTextInput
                  keyboardType={
                    item.keyboardType
                      ? (item.keyboardType as KeyboardType)
                      : 'default'
                  }
                  key={item.key.toString()}
                  onBlur={() => setFieldTouched(item.name)}
                  onFocus={() => validateForm()}
                  onChangeText={handleChange(item.name)}
                  placeholderTxt={item.placeholder}
                  touched={touched}
                  secureTextEntry={item.secureEntry}
                  name={item.name}
                  errors={errors}
                  customStyles={{ marginBottom: 10 }}
                  value={values[item.name]}
                />
              ))}
              <CustomButton
                title="RESET PASSWORD"
                enabledColor="#B90039"
                onPress={() => handleResetPwd(values)}
                disabled={!isValid || !dirty}
                customStyles={{ marginTop: 20 }}
              />
            </>
          )}
        </Formik>
      </KeyboardAwareScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    padding: 20,
    paddingTop: 40,
  },
  titleTxt: {
    fontSize: 16,
    opacity: 0.8,
    textAlign: 'center',
  },
  img: {
    width: 164,
    height: 164,
    alignSelf: 'center',
    marginTop: 43,
    marginBottom: 32,
  },
});
