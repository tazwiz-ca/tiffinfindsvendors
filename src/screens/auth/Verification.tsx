import React, { useState } from 'react';
import { useEffect } from 'react';
import { StatusBar, StyleSheet, View, Text } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import * as Sentry from '@sentry/react-native';
import { useNavigation } from '@react-navigation/core';

import OTP from '../../components/auth/OTP';
import utils from '../../utils/helpers';
import routes from '../../navigations/routes';
import { RootState } from '../../store';
import { errorToast, successToast } from '../../store/reducers/common/toaster';
import { startLoader, stopLoader } from '../../store/reducers/common/loader';
import { generateOTP, verifyOTP } from '../../network/AuthService';

const Verification = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  let counter = 30;
  const token = useSelector((state: RootState) => state.auth.token);

  const [resendCounter, setResendCounter] = useState(
    '00:' + ('0' + counter).slice(-2),
  );
  const [isCounterExpired, setCounterExpired] = useState(false);
  const [isValidPinCount, setIsValidPinCount] = useState(false);
  const [pin, setPin] = useState<Array<number>>([]);

  const phoneNumber = useSelector(
    (state: RootState) => state.auth.vendor.phoneNum,
  );

  const startTimer = () => {
    const interval = setInterval(() => {
      counter = counter - 1;
      if (counter <= 0) {
        clearInterval(interval);
        setCounterExpired(true);
      }
      setResendCounter('00:' + ('0' + counter).slice(-2));
    }, 1000);
  };

  useEffect(() => {
    if (!isCounterExpired) {
      startTimer();
    }
  }, []);

  const updateVerifyButton = (isValid: boolean, pin: Array<number>) => {
    setIsValidPinCount(isValid);
    setPin(pin);
  };

  const onVerifyClick = async () => {
    try {
      dispatch({ type: startLoader.type });
      const data = await verifyOTP(token, pin?.join(''));
      if (data.msg) {
        //store the phone number in async storage
        let data = await utils.getData('vendor');
        data.vendor.phoneNum = phoneNumber;
        utils.storeData('vendor', data);
        dispatch({
          type: successToast.type,
          payload: { message: 'OTP verification success' },
        });

        dispatch({ type: stopLoader.type });
        navigation.reset({
          index: 0,
          routes: [{ name: routes.APP_STACK_NAV }],
        });
      }
    } catch (error) {
      Sentry.captureException(error);
      dispatch({
        type: errorToast.type,
        payload: { message: error?.toString() },
      });
      dispatch({ type: stopLoader.type });
    }
  };

  const onResendOTP = async () => {
    try {
      dispatch({ type: startLoader.type });
      const data = await generateOTP(token, phoneNumber);
      if (data.msg) {
        dispatch({
          type: successToast.type,
          payload: { message: 'OTP sent successfully!!' },
        });
      }
      dispatch({ type: stopLoader.type });
    } catch (error) {
      Sentry.captureException(error);
      dispatch({
        type: errorToast.type,
        payload: { message: error?.toString() },
      });
      dispatch({ type: stopLoader.type });
    }
  };

  return (
    <>
      <StatusBar backgroundColor="#FFFFFF" barStyle="dark-content"></StatusBar>
      <View style={styles.container}>
        <Text style={styles.title}>Verification Code</Text>
        <Text style={styles.subTitle}>
          Please enter the 4 digit OTP sent to {phoneNumber}
        </Text>
        <OTP style={styles.otpBox} onValidCount={updateVerifyButton} />
        {/* {invalidCode && (
          <Text style={styles.errorText}>Try again. Incorrect code</Text>
        )} */}
        <Text style={styles.receiveCode}>Didin’t receive code?</Text>
        {!isCounterExpired && (
          <Text style={styles.resendCounter}>
            Resend code in {resendCounter}
          </Text>
        )}
        {isCounterExpired && (
          <TouchableOpacity onPress={onResendOTP}>
            <Text style={styles.resend}>Resend OTP</Text>
          </TouchableOpacity>
        )}

        <TouchableOpacity onPress={onVerifyClick}>
          <Text
            style={[
              styles.verify,
              { backgroundColor: isValidPinCount ? '#B90039' : '#CBCBCB' },
            ]}>
            VERIFY
          </Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontWeight: '600',
    fontSize: 24,
  },
  subTitle: {
    fontWeight: '200',
    marginHorizontal: 50,
    marginTop: 28,
    textAlign: 'center',
    color: '#000000',
    opacity: 0.6,
    fontSize: 13,
  },
  otpBox: {
    marginTop: 40,
  },
  errorText: {
    color: '#FF5562',
    alignSelf: 'flex-start',
    marginStart: 65,
    marginTop: 6,
    fontWeight: '400',
    fontSize: 12,
  },
  receiveCode: {
    color: '#000000',
    opacity: 0.4,
    marginTop: 38,
    fontSize: 14,
  },
  resendCounter: {
    color: '#000000',
    opacity: 0.4,
    fontWeight: '700',
    fontSize: 14,
    paddingVertical: 6,
  },
  resend: {
    color: '#00B970',
    fontWeight: '700',
    fontSize: 14,
    paddingVertical: 6,
  },
  verifyContainer: {
    width: '100%',
    backgroundColor: '#000000',
  },
  verify: {
    textAlign: 'center',
    paddingVertical: 16,
    backgroundColor: '#CBCBCB',
    borderRadius: 4,
    fontSize: 14,
    fontWeight: '500',
    color: '#FFFFFF',
    paddingHorizontal: 130,
    marginTop: 32,
    marginBottom: 35,
  },
});

export default Verification;
