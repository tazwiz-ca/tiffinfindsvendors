import React, { useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, Linking } from 'react-native';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import * as Sentry from '@sentry/react-native';

import CustomButton from '../../components/common/CustomButton';
import RNTextView from '../../components/common/RNTextView';
import colors from '../../configs/colors';
import routes from '../../navigations/routes';
import { useNavigation, useRoute } from '@react-navigation/core';
import { RootState } from '../../store';
import { startLoader, stopLoader } from '../../store/reducers/common/loader';
import { errorToast, successToast } from '../../store/reducers/common/toaster';
import { sendEmailForgotPwd } from '../../network/AuthService';
import { onNavigateFromMail } from '../../store/reducers/auth';
//import {navigateResetPwd} from '../../store/actions/authActions';

export default function ForgotPwdSuccess() {
  const navigation = useNavigation();
  const token = useSelector((state: RootState) => state.auth.token);
  const resetPwd = useSelector((state: RootState) => state.auth.resetPwd);
  const route = useRoute();
  const dispatch = useDispatch();

  /** When the vendor opens the email from his mobile and clicks on link
   * and when the store is updated with email and token
   * Navigate him to reset password screen */
  useEffect(() => {
    if (token) return;
    if (resetPwd.email && resetPwd.token) {
      navigation.navigate(routes.RESET_PWD);
    }
  }, [resetPwd]);

  const onResend = async () => {
    try {
      if (!route?.params) return;
      const params = route?.params as { email: string };
      dispatch({ type: startLoader.type });
      await sendEmailForgotPwd(params?.email);
      dispatch({
        type: successToast.type,
        payload: { message: 'Email link resent!!' },
      });
      dispatch({ type: stopLoader.type });
    } catch (error) {
      Sentry.captureException(error);
      dispatch({
        type: errorToast.type,
        payload: { message: error?.toString() },
      });
      dispatch({ type: stopLoader.type });
    }
  };

  useEffect(() => {
    //If the initial URL when the app opens for the first time is from linking navigate to pre confirmation screen based on the url params
    (async function () {
      try {
        const url = await Linking.getInitialURL();
        if (url) handleOpenURL({ url });
      } catch (error) {
        Sentry.captureException(error);
        console.log(error);
      }
    })();

    // Add event listener if the app is in foreground
    Linking.addEventListener('url', handleOpenURL);

    //Remove the subscription during unmount
    return () => {
      Linking.removeEventListener('url', handleOpenURL);
    };
  }, []);

  type URL = {
    url: string;
  };

  const handleOpenURL = ({ url }: URL) => {
    if (resetPwd.email) return;
    try {
      if (!url) return;
      //tiffinfindvendors://reset/:email/:token
      const uri = url.split('//')[1];
      if (!uri) return;

      //reset/:email/:token
      const email = uri.split('/')[1];
      const token = uri.split('/')[2];
      // After verification the user will be in the same screen to login
      if (email && token) {
        dispatch({ type: onNavigateFromMail.type, payload: { email, token } });
      } else {
        dispatch({
          type: errorToast.type,
          payload: { message: 'email/token not found in the url' },
        });
      }
    } catch (error) {
      Sentry.captureException(error);
      dispatch({
        type: errorToast.type,
        payload: { message: error?.toString() },
      });
    }
  };
  return (
    <View style={styles.container}>
      <RNTextView style={styles.titleTxt}>
        We have sent you an email of the reset password link
      </RNTextView>
      <FastImage
        source={require('../../assets/img/sent-mail.png')}
        style={styles.img}
      />
      <View style={styles.resendWrapper}>
        <RNTextView style={styles.didntTxt}>Didn't receieve email? </RNTextView>
        <TouchableOpacity onPress={onResend}>
          <RNTextView style={styles.resendTxt}>Resend </RNTextView>
        </TouchableOpacity>
      </View>
      <CustomButton
        title="BACK TO SIGN IN"
        enabledColor="rgba(0,0,0,0.1)"
        customTextStyles={styles.btnTxt}
        customStyles={{ marginTop: 32 }}
        onPress={() => navigation.navigate(routes.SIGN_IN)}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    padding: 20,
    paddingTop: 40,
  },
  titleTxt: {
    fontSize: 16,
    opacity: 0.8,
    textAlign: 'center',
  },
  img: {
    width: 164,
    height: 164,
    alignSelf: 'center',
    marginTop: 43,
    marginBottom: 32,
  },
  resendWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  didntTxt: {
    fontSize: 16,
    opacity: 0.4,
  },
  resendTxt: {
    color: '#0069B9',
    fontSize: 16,
    textDecorationLine: 'underline',
  },
  btnTxt: {
    color: 'black',
    fontWeight: '500',
  },
});
