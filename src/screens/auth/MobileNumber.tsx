import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Text, StatusBar, TextInput } from 'react-native';
import { AsYouType, parsePhoneNumber } from 'libphonenumber-js';
import FastImage from 'react-native-fast-image';
import * as Sentry from '@sentry/react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/core';

import CustomButton from '../../components/common/CustomButton';
import colors from '../../configs/colors';
import { RootState } from '../../store';
import { errorToast } from '../../store/reducers/common/toaster';
import { updateMobileNumber } from '../../store/reducers/auth';
import routes from '../../navigations/routes';

export default function MobileNumber() {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const token = useSelector((state: RootState) => state.auth.token);
  const vendor = useSelector((state: RootState) => state.auth.vendor);
  const [value, setValue] = useState('');
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);

  useEffect(() => {
    if (vendor.phoneNum) {
      navigation.navigate(routes.VERIFICATION);
    }
  }, [vendor]);

  const onGetCode = async () => {
    try {
      //parse the phone number
      const filteredNumber = parsePhoneNumber(value, 'CA');
      dispatch(
        updateMobileNumber(
          token,
          vendor.email,
          filteredNumber.nationalNumber.toString(),
        ),
      );
    } catch (error) {
      Sentry.captureException(error);
      dispatch({
        type: errorToast.type,
        payload: { message: error?.toString() },
      });
    }
  };

  const onChangeInput = (text: string) => {
    //Format the phone number as the user types
    if (text.length > 5) setValue(new AsYouType('CA').input(text));
    else setValue(text);

    setIsButtonDisabled(text.length !== 14);
  };
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <View style={styles.container}>
        <Text style={styles.titleTxt}>Enter Mobile Number</Text>
        <Text style={styles.subtitleTxt}>
          Please enter your mobile number to get 4 digit OTP
        </Text>
        <View style={styles.phoneInpContainer}>
          <FastImage
            source={require('../../assets/img/canada.png')}
            style={styles.canadaFlag}
          />
          <Text style={styles.countryCode}>+1</Text>

          <TextInput
            onChangeText={onChangeInput}
            autoFocus={true}
            placeholder={'Phone Number'}
            value={value}
            returnKeyType="done"
            keyboardType={'number-pad'}
            style={styles.phoneInp}
            maxLength={14}
          />
        </View>
        <CustomButton
          title="GET CODE"
          enabledColor="#B90039"
          onPress={onGetCode}
          disabled={isButtonDisabled}
          customStyles={{ marginVertical: 20 }}
        />
      </View>
    </>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    padding: 20,
  },
  titleTxt: {
    marginTop: 40,
    fontWeight: '500',
    fontSize: 23,
    textAlign: 'center',
  },
  subtitleTxt: {
    marginTop: 28,
    fontWeight: 'normal',
    fontSize: 14,
    textAlign: 'center',
    alignSelf: 'center',
    width: '78%',
    opacity: 0.6,
  },
  phoneInpContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 40,
    marginBottom: 50,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.4)',
    marginHorizontal: 30,
  },
  canadaFlag: {
    width: 24,
    height: 24,
  },
  countryCode: {
    marginLeft: 5,
    fontSize: 16,
  },
  phoneInp: {
    fontSize: 16,
    opacity: 0.8,
    marginLeft: 5,
  },
});
