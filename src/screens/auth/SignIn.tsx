import React, { useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  SafeAreaView,
  StatusBar,
  KeyboardType,
} from 'react-native';
import * as yup from 'yup';
import { Formik } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useNavigation } from '@react-navigation/core';

import CustomTextInput from '../../components/common/CustomTextInput';
import CustomButton from '../../components/common/CustomButton';
import colors from '../../configs/colors';
import RNTextView from '../../components/common/RNTextView';
import routes from '../../navigations/routes';
import { loginVendor } from '../../store/reducers/auth';
import { RootState } from '../../store';
import CheckBox from '../../components/menu/CheckBox';
import { subscribe } from '../../notification/NotificationService';

type LoginType = {
  email: string;
  pwd: string;
};
export default function SignIn() {
  const [isAdmin, setAdmin] = useState(false);
  const authData = useSelector((state: RootState) => state.auth);
  const navigation = useNavigation();
  const data = [
    {
      key: 1,
      placeholder: 'Email Address',
      name: 'email',
      keyboardType: 'email-address',
    },
    {
      key: 2,
      placeholder: 'Password',
      name: 'pwd',
      secureEntry: true,
    },
  ];
  const dispatch = useDispatch();

  useEffect(() => {
    if (!authData.token) return;

    if (!authData.vendor.phoneNum && authData.errorCode === 103) {
      // Phone number not provided
      return navigation.navigate(routes.MOBILE_NUMBER);
    } else if (authData.vendor.phoneNum && authData.errorCode === 101) {
      // Phone number not verified
      return navigation.navigate(routes.VERIFICATION);
    }

    //Subscribe for notifications
    subscribe(authData.vendor.id);
    // Navigate to the vendor screen only if the credentials are of vendor's else to admin
    if (authData.vendor.typeOfUser === 'tiffinProvider') {
      navigation.reset({
        index: 0,
        routes: [{ name: routes.APP_STACK_NAV }],
      });
    } else if (authData.vendor.typeOfUser === 'admin') {
      navigation.reset({
        index: 0,
        routes: [{ name: routes.ADMIN_STACK_NAV }],
      });
    }
  }, [authData]);

  const handleLogin = ({ email, pwd: password }: LoginType) =>
    dispatch(
      loginVendor(email, password, isAdmin ? 'admin' : 'tiffinProvider'),
    );

  return (
    <>
      <SafeAreaView style={{ backgroundColor: colors.white }} />
      <StatusBar barStyle="dark-content" />
      <View testID="signInContainer" style={styles.container}>
        <KeyboardAwareScrollView
          style={{ padding: 30 }}
          nestedScrollEnabled={true}>
          <Formik
            onSubmit={() => {}}
            enableReinitialize={true}
            initialValues={{
              email: '',
              pwd: '',
            }}
            validationSchema={yup.object().shape({
              email: yup.string().email().label('Email').required(),
              pwd: yup.string().label('Password').required(),
            })}>
            {({
              values,
              handleChange,
              errors,
              setFieldTouched,
              touched,
              isValid,
              validateForm,
              dirty,
            }: any) => (
              <>
                {data.map(item => (
                  <CustomTextInput
                    keyboardType={
                      item.keyboardType
                        ? (item.keyboardType as KeyboardType)
                        : 'default'
                    }
                    key={item.key.toString()}
                    onBlur={() => setFieldTouched(item.name)}
                    onFocus={() => validateForm()}
                    onChangeText={handleChange(item.name)}
                    placeholderTxt={item.placeholder}
                    touched={touched}
                    secureTextEntry={item.secureEntry}
                    name={item.name}
                    errors={errors}
                    customStyles={{ marginBottom: 10 }}
                    value={values[item.name]}
                  />
                ))}
                <TouchableOpacity
                  //style={styles.forgotPwdContainer}
                  onPress={() => navigation.navigate(routes.FORGOT_PWD)}>
                  <Text style={styles.forgotpwdTxt}>Forgot Password?</Text>
                </TouchableOpacity>

                {/** Admin only */}
                <View>
                  <CheckBox
                    selected={isAdmin}
                    onPress={() => setAdmin(val => !val)}
                    text={'I am an Admin'}
                    style={{
                      flexDirection: 'row',
                      marginTop: 20,
                      alignItems: 'center',
                    }}
                    textStyle={styles.checkboxTxt}
                    size={35}
                  />
                </View>

                <CustomButton
                  title="LOGIN"
                  enabledColor="#B90039"
                  onPress={() => handleLogin(values)}
                  disabled={!isValid || !dirty}
                  customStyles={{ marginTop: 20 }}
                />
              </>
            )}
          </Formik>
          <TouchableWithoutFeedback
            onPress={() => navigation.navigate('TermsConditions')}>
            <View style={styles.footer}>
              <Text style={styles.footerText}>
                By signing in, you accepts our{' '}
                <Text style={[styles.footerText, styles.footerLink]}>
                  Terms & Conditions
                </Text>
                <Text style={styles.footerText}> and </Text>
                <Text style={[styles.footerText, styles.footerLink]}>
                  Privacy Policy
                </Text>
              </Text>
            </View>
          </TouchableWithoutFeedback>
          {/** Navigate to sign up screen */}
          <View style={styles.footerContainer}>
            <RNTextView style={styles.dontHavTxt}>
              Don’t have an account?{' '}
            </RNTextView>
            <TouchableOpacity
              onPress={() => navigation.navigate(routes.SIGN_UP)}>
              <RNTextView style={styles.createAcctTxt}>
                Create Account
              </RNTextView>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: colors.white,
  },
  orContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  orText: {
    marginVertical: 20,
    opacity: 0.4,
    fontWeight: '600',
  },
  verticalLine: {
    borderWidth: 0.8,
    width: 74,
    opacity: 0.2,
  },
  rememberMeTxt: {
    marginLeft: 10,
    marginTop: 8,
    fontSize: 14,
    color: colors.textInputBorder,
    opacity: 0.4,
  },
  forgotpwdTxt: {
    fontSize: 14,
    opacity: 0.4,
    marginTop: -10,
  },
  footer: {
    width: '100%',
    alignItems: 'center',
    opacity: 0.4,
  },
  footerText: {
    fontWeight: '300',
    fontSize: 13,
  },
  footerLink: {
    fontWeight: '400',
    textDecorationLine: 'underline',
  },
  footerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
  },
  dontHavTxt: {
    opacity: 0.7,
  },
  createAcctTxt: {
    color: '#00DD86',
    fontWeight: 'bold',
  },
  checkboxTxt: {
    fontFamily: 'Apercu-Regular',
    fontSize: 16,
    color: colors.textInputBorder,
    opacity: 0.7,
    marginLeft: 10,
    marginTop: -2,
  },
});
