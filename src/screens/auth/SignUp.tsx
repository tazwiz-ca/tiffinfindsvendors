import React, { FunctionComponent, useEffect } from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import * as yup from 'yup';
import { Formik } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/core';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import CustomTextInput from '../../components/common/CustomTextInput';
import CustomButton from '../../components/common/CustomButton';
import colors from '../../configs/colors';
import routes from '../../navigations/routes';
import RNTextView from '../../components/common/RNTextView';
import { registerVendor } from '../../store/reducers/auth';
import { RootState } from '../../store';

type RegisterType = {
  fullName: string;
  email: string;
  password: string;
};

const SignUp: FunctionComponent = () => {
  const vendor = useSelector((state: RootState) => state.auth.vendor);
  const token = useSelector((state: RootState) => state.auth.token);
  const navigation = useNavigation();
  const data = [
    { key: 1, placeholder: 'Full Name', name: 'fullName' },
    {
      key: 2,
      placeholder: 'Email',
      name: 'email',
      keyboardType: 'email-address',
    },
    {
      key: 3,
      placeholder: 'Create Password',
      name: 'password',
      secureEntry: true,
    },
    {
      key: 4,
      placeholder: 'Confirm Password',
      name: 'confirmPwd',
      secureEntry: true,
    },
  ];
  const dispatch = useDispatch();

  //Once the user is successfulluy registered, navigate the vendor to mobile number screen
  useEffect(() => {
    if (!token) return;
    if (!vendor.phoneNum) {
      navigation.navigate(routes.MOBILE_NUMBER);
    }
  }, [vendor]);

  const handleRegister = async ({
    fullName,
    email,
    password,
  }: RegisterType) => {
    dispatch(registerVendor(fullName, email, password));
  };

  return (
    <>
      <SafeAreaView style={{ backgroundColor: colors.white }} />
      <StatusBar barStyle="dark-content" />
      <View style={styles.container}>
        <KeyboardAwareScrollView
          style={{ paddingHorizontal: 20, paddingTop: 30 }}>
          <Formik
            onSubmit={() => {}}
            initialValues={{
              fullName: '',
              email: '',
              password: '',
              confirmPwd: '',
            }}
            validationSchema={yup.object().shape({
              fullName: yup.string().label('Full Name').required(),
              email: yup.string().email().label('Email').required(),
              password: yup
                .string()
                .label('Password')
                .matches(
                  /^(?=.*\d)(?=.*[@$!%*#?&_])[A-Za-z\d@$!%*#?&_]{8,}$/,
                  'Must Contain 8 Characters, One Number and one special case Character',
                )
                .required(),
              confirmPwd: yup
                .string()
                .label('Confirm Password')
                .oneOf([yup.ref('password'), null], 'Passwords must match')
                .required(),
            })}>
            {({
              values,
              handleChange,
              errors,
              setFieldTouched,
              touched,
              isValid,
              validateForm,
              dirty,
            }) => (
              <>
                {data.map(item => (
                  <CustomTextInput
                    keyboardType={item.keyboardType}
                    key={item.key.toString()}
                    onBlur={() => setFieldTouched(item.name)}
                    onFocus={() => validateForm()}
                    onChangeText={handleChange(item.name)}
                    placeholderTxt={item.placeholder}
                    touched={touched}
                    secureTextEntry={item.secureEntry}
                    name={item.name}
                    errors={errors}
                    customStyles={{ marginBottom: 10 }}
                  />
                ))}
                <CustomButton
                  title="REGISTER"
                  enabledColor="#B90039"
                  onPress={() => handleRegister(values)}
                  disabled={!isValid || !dirty}
                  customStyles={{ marginTop: 20 }}
                />
              </>
            )}
          </Formik>
          {/** Navigate to sign up screen */}
          <View style={styles.footerContainer}>
            <RNTextView style={styles.dontHavTxt}>
              Already have an account?{' '}
            </RNTextView>
            <TouchableOpacity
              onPress={() => navigation.navigate(routes.SIGN_IN)}>
              <RNTextView style={styles.createAcctTxt}>Sign In</RNTextView>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      </View>
    </>
  );
};

export default SignUp;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: colors.white,
  },
  orText: {
    textAlign: 'center',
    marginVertical: 20,
  },
  privacyTxt: {
    fontSize: 14,
    opacity: 0.4,
    color: colors.textInputBorder,
  },
  boldTxt: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  footerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 70,
  },
  dontHavTxt: {
    opacity: 0.7,
  },
  createAcctTxt: {
    color: '#00DD86',
    fontWeight: 'bold',
  },
});
