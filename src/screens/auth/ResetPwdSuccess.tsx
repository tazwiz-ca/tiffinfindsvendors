import React from 'react';
import { View, StyleSheet } from 'react-native';
import RNTextView from '../../components/common/RNTextView';
import FastImage from 'react-native-fast-image';
import { useNavigation } from '@react-navigation/core';

import CustomButton from '../../components/common/CustomButton';
import colors from '../../configs/colors';
import routes from '../../navigations/routes';

export default function ResetPwdSuccess() {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <RNTextView style={styles.titleTxt}>
        Your password has been successfully reset login to your accont with your
        new password
      </RNTextView>
      <FastImage
        source={require('../../assets/img/reset-pwd-success.png')}
        style={styles.img}
      />
      <CustomButton
        title="SIGN IN"
        enabledColor="rgba(0,0,0,0.1)"
        customTextStyles={styles.btnTxt}
        customStyles={{ marginTop: 32 }}
        onPress={() => navigation.navigate(routes.SIGN_IN)}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    padding: 20,
    paddingTop: 40,
  },
  titleTxt: {
    fontSize: 16,
    opacity: 0.8,
    textAlign: 'center',
  },
  img: {
    width: 164,
    height: 164,
    alignSelf: 'center',
    marginTop: 43,
    marginBottom: 32,
  },
  resendWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  didntTxt: {
    fontSize: 16,
    opacity: 0.4,
  },
  resendTxt: {
    color: '#0069B9',
    fontSize: 16,
    textDecorationLine: 'underline',
  },
  btnTxt: {
    color: 'black',
    fontWeight: '500',
  },
});
