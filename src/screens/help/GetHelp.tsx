import React, { useState } from 'react';
import { View, StyleSheet, StatusBar, TextInput } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useDispatch, useSelector } from 'react-redux';
import * as Sentry from '@sentry/react-native';
import { useNavigation } from '@react-navigation/core';

import RNTextView from '../../components/common/RNTextView';
import colors from '../../configs/colors';
import CustomButton from '../../components/common/CustomButton';
import AppErrorText from '../../components/common/AppErrorText';
import WhatsappChat from '../../components/help/WhatsappChat';
import { RootState } from '../../store';
import { handleHelp } from '../../network/MiscellaneousService';
import { errorToast, successToast } from '../../store/reducers/common/toaster';
import { startLoader, stopLoader } from '../../store/reducers/common/loader';
import NavHeader from '../../components/common/NavHeader';

export default function GetHelp() {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [details, setDetails] = useState('');
  const token = useSelector((state: RootState) => state.auth.token);
  const RedAsterik = () => <RNTextView style={styles.asterik}>*</RNTextView>;

  const handleSendEmail = async () => {
    try {
      dispatch({ type: startLoader.type });
      const { message } = await handleHelp(token, details);
      dispatch({ type: successToast.type, payload: { message } });
      dispatch({ type: stopLoader.type });
      navigation.goBack();
    } catch (error) {
      Sentry.captureException(error);
      dispatch({ type: stopLoader.type });
      dispatch({ type: errorToast.type, payload: { message: error.message } });
    }
  };

  return (
    <>
      <StatusBar barStyle="light-content" />
      <NavHeader title="Get Help" />
      <View style={styles.wrapper}>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="always"
          nestedScrollEnabled={true}>
          <View style={styles.container}>
            <RNTextView style={styles.titleTxt}>
              Our customer service team will get in touch with you if you can
              briefly explain the issue
            </RNTextView>
            <RNTextView style={styles.label}>
              Describe in detail
              <RedAsterik />
            </RNTextView>
            <TextInput
              multiline
              style={styles.textInp}
              maxLength={1000}
              value={details}
              onChangeText={text => setDetails(text)}
            />
            <View style={styles.errorContainer}>
              {details.length < 10 && (
                <AppErrorText>Minimum 10 characters</AppErrorText>
              )}
              <RNTextView style={styles.detailsLength}>
                {details.length}/1000
              </RNTextView>
            </View>
            <CustomButton
              title="SEND EMAIL"
              enabledColor={colors.primary}
              disabled={details.length < 10}
              customStyles={{ marginTop: 46 }}
              onPress={handleSendEmail}
            />
            <RNTextView style={styles.orTxt}>OR</RNTextView>
            <WhatsappChat />
          </View>
        </KeyboardAwareScrollView>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: colors.white,
  },
  container: {
    flex: 1,
    padding: 20,
  },
  asterik: {
    color: 'red',
  },
  titleTxt: {
    fontFamily: 'JosefinSans-Bold',
    fontWeight: '500',
    fontSize: 18,
    color: colors.textInputBorder,
  },
  label: {
    fontFamily: 'Apercu-Bold',
    fontWeight: '500',
    marginTop: 20,
    fontSize: 16,
    color: colors.textInputBorder,
  },
  textInp: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: colors.borderColor,
    height: 197,
    marginTop: 7,
  },
  errorContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 4,
  },
  detailsLength: {
    opacity: 0.4,
    textAlign: 'right',
    fontSize: 14,
    position: 'absolute',
    right: 0,
  },
  orTxt: {
    fontFamily: 'Apercu-Regular',
    fontSize: 16,
    textAlign: 'center',
    marginTop: 10,
    fontWeight: '600',
  },
});
