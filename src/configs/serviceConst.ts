export default {
  BASE_URL: 'https://tiffin-finds-api-prod.herokuapp.com/',
  FETCH_LOGIN_API_URI: 'auth/login/',
  REGISTER_API_URI: 'auth/register',
  UPDATE_PHONE_API: 'auth/changeContactNumber/',
  FORGOT_PWD_API_URI: 'auth/forgot/',
  RESEND_API_URI: 'auth/resend/',
  RESET_API_URI: 'auth/resetPassword',
  OTP_GENERATE_URI: 'auth/generateOTP/',
  VERIFY_OTP: 'auth/verifyOTP/',
  USER_PROFILE: 'auth/getUserProfile',

  //Menus
  CREATE_MENU: 'provider/createOrUpdateMenu',
  FETCH_MENU: 'provider/getMenu/',
  DELETE_MENU: 'provider/deleteMenu/',

  //Payments
  SAVE_PAYMENT: 'payments/savePaymentInfo',

  //ADMIN
  GET_PROVIDER_INFOS: 'admin/getAllProviderInfos',
  APPROVE_PROVIDER: 'admin/approveOrDeclineProvider/',
  FETCH_PROVIDER_DETAILS: 'provider/getProviderMenuById',

  //Miscellaneous
  UPDATE_NOTIFICATIONS: 'miscellaneous/updateNotifications',
  FETCH_NOTIFICATIONS: 'miscellaneous/fetchNotifications',
  GET_HELP_URI: 'miscellaneous/getHelp',
  CUSTOMIZE_MENU: 'miscellaneous/customizeMenu',
  MOBILE_APP_VERSIONS: 'miscellaneous/getMobileAppVersions/',
  WHATSAPP_CHAT_NUMBER: 'miscellaneous/getCSMobileNumber',

  //Order
  GET_ORDERS: 'orders/fetchOrdersByUser',
  UPDATE_ORDER_STATUS: 'orders/acceptOrRejectOrder/',
};
