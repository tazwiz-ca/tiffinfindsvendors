import { createSlice } from '@reduxjs/toolkit';

import { apiCallBegan } from '../actions/api';
import {
  fetchLoginFormDetails,
  registerUser,
  updatePhoneNumber,
} from '../../network/AuthService';
import { store } from '../index';
import helpers from '../../utils/helpers';

type Vendor = {
  phoneNum: string;
  email: string;
  fullName: string;
  id: string;
  location: Array<any>;
  iat: any;
  typeOfUser: string;
};
export type AuthState = {
  vendor: Vendor;
  token: string;
  notifications: object;
  resetPwd: object;
  errorCode?: number;
};

let initialState = {
  vendor: {
    phoneNum: '',
    email: '',
    fullName: '',
    id: '',
    location: [],
    iat: '',
    typeOfUser: '',
  },
  token: '',
  notifications: {},
  resetPwd: { email: '', token: '' },
  errorCode: 0,
};

const slice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    loadVendor: (state: AuthState, action) => {
      const { token, vendor } = action.payload;
      state.vendor = vendor;
      state.token = token;
    },
    login: (state: AuthState, action) => {
      const { data } = action.payload;
      let vendor = helpers.decodeToken(data.token) as Vendor;
      state.vendor = vendor;
      state.token = data.token;
      if (data.code) {
        state.errorCode = data.code;
      }
    },
    mobileVerify: (state: AuthState, action) => {
      const { args } = action.payload;
      state.vendor.phoneNum = args[2];
    },
    logout: (state: AuthState, _) => {
      state.vendor = initialState.vendor;
      state.token = initialState.token;
      state.errorCode = 0;
    },
    onNavigateFromMail: (state: AuthState, action) => {
      const { email, token } = action.payload;
      state.resetPwd = { email, token };
    },
  },
});

export const {
  loadVendor,
  login,
  mobileVerify,
  logout,
  onNavigateFromMail,
} = slice.actions;

export default slice.reducer;

export const loginVendor = (
  email: string,
  password: string,
  typeOfUser: string,
) => (dispatch: typeof store.dispatch) => {
  dispatch({
    type: apiCallBegan.type,
    payload: {
      apiMethod: fetchLoginFormDetails,
      args: [email, password, typeOfUser],
      onSucess: [login.type],
      loader: true,
    },
  });
};

export const registerVendor = (
  fullName: string,
  email: string,
  password: string,
) => (dispatch: typeof store.dispatch) => {
  const vendorObj = {
    fullName,
    email,
    password,
    typeOfUser: 'tiffinProvider',
  };
  dispatch({
    type: apiCallBegan.type,
    payload: {
      apiMethod: registerUser,
      args: [vendorObj],
      onSucess: [login.type],
      loader: true,
    },
  });
};

export const updateMobileNumber = (
  token: string,
  email: string,
  mobileNumber: string,
) => (dispatch: typeof store.dispatch) => {
  dispatch({
    type: apiCallBegan.type,
    payload: {
      apiMethod: updatePhoneNumber,
      args: [token, email, mobileNumber],
      onSucess: [mobileVerify.type],
      loader: true,
    },
  });
};
