import * as Sentry from '@sentry/react-native';

import { StateProps as MenuStateProps } from '../menus/menus';
import { StateProps as GeneralStateProps } from '../menus/general';
import { StateProps as DeliveryStateProps } from '../menus/delivery';
import { StateProps as AdditionalStateProps } from '../menus/additional';
import { AuthState as AuthStateProps } from '../auth';

export const structureMenuObject = (state: MenuStateProps, data: any) => {
  try {
    //meal offerings
    if (data.resObject?.menus?.menu) {
      //If the menus have values, then the boolean has to be set as true
      state.isUpdate = true;
      let mealTypes = Object.keys(data.resObject.menus.menu);
      for (let mealType of mealTypes) {
        //get the adequate meal offering
        let mealOffering = state.mealOfferings.find(
          item => item.id === mealType,
        );
        if (!mealOffering) continue;
        //mark the meal offering as selected
        mealOffering.selected = true;

        //Check if days are already added
        let days = data.resObject.menus.menu[mealType]?.days;
        if (days) {
          for (let day in days) {
            let mealDay = mealOffering.dayMenus.find(
              item => item.text.toLowerCase() === day.toLowerCase(),
            );
            if (!mealDay) continue;
            mealDay.menu = days[day];
          }
        }
        //Check if price is already added
        let prices = data.resObject.menus.menu[mealType]?.price;
        if (prices) {
          for (let mealPrice of prices) {
            let price = mealOffering.frequency.find(
              item => item.id === mealPrice.nos,
            );
            if (!price) continue;
            if (mealPrice.value) {
              price.selected = true;
              price.price = parseInt(mealPrice.value);
            }
          }
        }
      }
    }
    // Meal cuisines
    let cuisines: Array<string> = data.resObject?.menus?.mealCuisines;
    if (cuisines && cuisines.length > 0) {
      //Mark the already added cuisine's selected bool as true
      for (let cuisine of cuisines) {
        let selectedCuisine = state.mealCuisines.find(
          item => item.text.toLowerCase() === cuisine.toLowerCase(),
        );
        if (!selectedCuisine) continue;
        selectedCuisine.selected = true;
      }
    }
    // Meal restrictions
    let restrictions: Array<string> = data.resObject?.menus?.restrictions;
    if (restrictions && restrictions.length > 0) {
      //Mark the already added meal restriction's selected bool as true
      for (let restriction of restrictions) {
        let selectedRestriction = state.mealRestrictions.find(
          item => item.text.toLowerCase() === restriction.toLowerCase(),
        );
        if (!selectedRestriction) continue;
        selectedRestriction.selected = true;
      }
    }
    // Images
    let images: Array<{ [key: string]: string }> = data.resObject?.plan?.images;
    if (images && images.length > 0) {
      state.image = {
        uri: images[0].s3Url,
        name: images[0].name,
      };
    }
    // Allergies
    state.isAllergyAccomodate = data.resObject?.menus?.accomodateAllergies;
  } catch (error) {
    Sentry.captureException(error);
    console.log(error);
  }
};

export const structureGeneralObject = (state: GeneralStateProps, data: any) => {
  try {
    if (!data?.resObject?.plan) return;
    // Restaurant name
    let restaurantName = data.resObject.plan.providerName;
    if (restaurantName) {
      state.restaurantName = restaurantName;
    }
    // PlanId
    state.planId = data.resObject.plan._id;
    //Location
    let location = data.resObject.plan.location;
    if (location) {
      state.location.address = location;
      state.location.coords = {
        latitude: location.coordinates.coordinates[1],
        longitude: location.coordinates.coordinates[0],
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      };
    }
    // Is certified
    state.isCertified = data.resObject.plan.isCertified;
    // Is good standing
    state.isGoodStanding = data.resObject.plan.isGoodStanding;
  } catch (error) {
    Sentry.captureException(error);
    console.log(error);
  }
};

export const structureDeliveryObject = (
  state: DeliveryStateProps,
  data: any,
) => {
  try {
    if (!data?.resObject?.plan) return;
    // Is delivery preference
    if (data.resObject.plan.doDelivery)
      state.isDelivery = data.resObject.plan.doDelivery;
    // Is pickup preference
    if (data.resObject.plan.allowPickup)
      state.isPickup = data.resObject.plan.allowPickup;
    // Delivery Radius
    if (data.resObject.plan.deliveryRadius)
      state.deliveryRadius = data.resObject.plan.deliveryRadius;
    // Delivery frequency
    let deliveryFreq: Array<string> = data.resObject.plan.deliveryFrequency;
    if (deliveryFreq && deliveryFreq.length > 0) {
      for (let freq of deliveryFreq) {
        let selectedFreq = state.deliveryFrequency.find(
          item => item.text.toLowerCase() === freq.toLowerCase(),
        );
        if (!selectedFreq) continue;
        selectedFreq.selected = true;
      }
    }
  } catch (error) {
    Sentry.captureException(error);
    console.log(error);
  }
};

export const structureAdditionalObject = (
  state: AdditionalStateProps,
  data: any,
) => {
  try {
    if (!data?.resObject?.plan) return;
    //Kitchen capacity
    let kitchenCapacity: string = data.resObject.plan.kitchenCapacity;
    if (kitchenCapacity) {
      state.kitchenCapacity = parseInt(kitchenCapacity);
    }
    // Menu customization
    let isMenuCustomizable: boolean = data.resObject.plan.menuCustomizable;
    state.isMenuCustomizable = isMenuCustomizable;

    // Additional message
    let addnlMsg: string = data.resObject.plan.additionalMessages;
    state.additionalMessages = addnlMsg;

    //Delivery slots
    let deliverySlots: Array<string> = data.resObject.plan.deliveryTimes;
    if (deliverySlots && deliverySlots.length > 0) {
      for (let slot of deliverySlots) {
        let selectedSlot = state.deliverySlots.find(
          item =>
            item.text.toLowerCase().replace(' ', '') ===
            slot.toLowerCase().replace(' ', ''),
        );
        if (!selectedSlot) continue;
        selectedSlot.selected = true;
      }
    }
  } catch (error) {
    Sentry.captureException(error);
    console.log(error);
  }
};

type PriceBody = {
  frequency: string;
  value: string;
  nos: number;
};
type DaysBody = {
  [key: string]: string;
};
type MenuBody = {
  [key: string]: {
    price: Array<PriceBody>;
    type: string;
    days: DaysBody;
  };
};

const frameMenuBody = (menuState: MenuStateProps): MenuBody => {
  try {
    if (menuState.mealOfferings.length === 0) return {};

    let mealObj: MenuBody | undefined = {};

    for (let mealOffering of menuState.mealOfferings) {
      if (!mealOffering.selected) continue;
      mealObj[mealOffering.id] = {
        type: mealOffering.id,
        price: mealOffering.frequency
          .filter(item => item.selected)
          .map(item => ({
            frequency: item.key,
            value: item.price.toString(),
            nos: item.id,
          })),
        days: mealOffering.dayMenus
          .filter(item => item.menu !== '')
          .reduce(
            (obj, item: { [key: string]: string }) => ({
              ...obj,
              [item.text]: item.menu,
            }),
            {},
          ),
      };
    }
    return mealObj;
  } catch (error) {
    Sentry.captureException(error);
    console.log(error);
    return {};
  }
};

type LocationProps = {
  address: string;
  fullAddress: string;
  coordinates: {
    coordinates: Array<number>;
  };
};

export type MenuRequestBody = {
  user: string;
  isUpdate: boolean;
  planId?: string;
  providerName: string;
  typeOfProvider: string;
  contactName: string;
  email: string;
  contactNumber: string;
  location: LocationProps;
  isCertified: boolean;
  isGoodStanding: boolean;
  accomodateAllergies: boolean;
  restrictions: Array<string>;
  doDelivery: boolean;
  allowPickup: boolean;
  deliveryFrequency: Array<string>;
  menuCustomizable: boolean;
  deliveryTimes: Array<string>;
  deliveryRadius: number;
  kitchenCapacity: string;
  additionalMessages: string;
  vendorAgreement: boolean;
  mealCuisines: Array<string>;
  menuImages?: Array<{ [key: string]: string }>;
  menu: MenuBody;
};

// Request to create or update the menu for the vendor
export const frameMenuRequestBody = (
  menuState: MenuStateProps,
  generalState: GeneralStateProps,
  deliveryState: DeliveryStateProps,
  additionalState: AdditionalStateProps,
  authState: AuthStateProps,
): MenuRequestBody | undefined => {
  try {
    let request: MenuRequestBody = {
      user: authState.vendor.id,
      isUpdate: menuState.isUpdate,
      providerName: generalState.restaurantName,
      typeOfProvider: 'restaurant',
      contactName: authState.vendor.fullName,
      email: authState.vendor.email,
      contactNumber: authState.vendor.phoneNum,
      location: {
        address: generalState.location.address.fullAddress,
        fullAddress: generalState.location.address.fullAddress,
        coordinates: {
          coordinates: [
            generalState.location.coords.longitude,
            generalState.location.coords.latitude,
          ],
        },
      },
      isCertified: generalState.isCertified,
      isGoodStanding: generalState.isGoodStanding,
      accomodateAllergies: menuState.isAllergyAccomodate,
      restrictions: menuState.mealRestrictions
        .filter(item => item.selected)
        .map(item => item.text),
      doDelivery: deliveryState.isDelivery,
      deliveryFrequency: deliveryState.deliveryFrequency
        .filter(item => item.selected)
        .map(item => item.text),
      allowPickup: deliveryState.isPickup,
      menuCustomizable: additionalState.isMenuCustomizable,
      deliveryTimes: additionalState.deliverySlots
        .filter(item => item.selected)
        .map(item => item.text),
      deliveryRadius: deliveryState.deliveryRadius,
      kitchenCapacity: additionalState.kitchenCapacity.toString(),
      additionalMessages: additionalState.additionalMessages,
      vendorAgreement: true,
      mealCuisines: menuState.mealCuisines
        .filter(item => item.selected)
        .map(item => item.text),
      menuImages: [
        {
          name: menuState.image.name
            ? menuState.image.name
            : 'temp' + new Date().toString().replace(/\s/g, '_'),
          uri: menuState.image.uri,
          url: menuState.image.uri,
        },
      ],
      menu: frameMenuBody(menuState),
    };
    //Add the planid only if updating
    if (menuState.isUpdate) {
      request.planId = generalState.planId;
    }

    return request;
  } catch (error) {
    Sentry.captureException(error);
    console.log(error);
  }
};
