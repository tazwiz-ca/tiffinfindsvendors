import { createSlice } from '@reduxjs/toolkit';

import { apiCallBegan } from '../../actions/api';
import { store } from '../../index';
import { getUserProfile } from '../../../network/AuthService';

type AddressProps = {
  city: string;
  address: string;
  postalCode: string;
  province: string;
};

export type PaymentState = {
  fullName: string;
  routingNumber: string;
  accountNumber: string;
  sin: string;
  dobDay: string;
  dobMonth: string;
  dobYear: string;
  address: AddressProps;
  isPaymentVerified?: boolean;
};

let initialState = {
  fullName: '',
  routingNumber: '',
  accountNumber: '',
  sin: '',
  dobDay: '',
  dobMonth: '',
  dobYear: '',
  address: { city: '', province: '', postalCode: '', address: '' },
  isPaymentVerified: false,
};

const slice = createSlice({
  name: 'payments',
  initialState,
  reducers: {
    loadPaymentInfo: (state: PaymentState, action) => {
      const { data } = action.payload;
      if (
        data &&
        data.paymentInfo &&
        data.paymentInfo.external_accounts &&
        data.paymentInfo.external_accounts.data[0]
      ) {
        state.isPaymentVerified = true;
        const { external_accounts } = data.paymentInfo;
        const { individual } = data.paymentInfo;
        state.sin = '1234';
        state.fullName = external_accounts.data[0].account_holder_name;
        state.accountNumber = external_accounts.data[0].last4;
        state.routingNumber = external_accounts.data[0].routing_number;

        state.address = {
          city: individual.address.city,
          province: '',
          address: individual.address.line1,
          postalCode: individual.address.postal_code,
        };

        state.dobDay = individual.dob.day;
        state.dobMonth = individual.dob.month;
        state.dobYear = individual.dob.year;
      }
    },
    updateAddress: (state: PaymentState, action) => {
      const { address } = action.payload;
      state.address.address = address.fullAddress;
      state.address.city = address.city;
      state.address.postalCode = address.postalCode;
      state.address.province = address.province;
    },
  },
});

export const { loadPaymentInfo, updateAddress } = slice.actions;

export default slice.reducer;

export const fetchVendorProfile = (token: string) => (
  dispatch: typeof store.dispatch,
) => {
  dispatch({
    type: apiCallBegan.type,
    payload: {
      apiMethod: getUserProfile,
      args: [token],
      onSucess: [loadPaymentInfo.type],
      loader: false,
    },
  });
};
