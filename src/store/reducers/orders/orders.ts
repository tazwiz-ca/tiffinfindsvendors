import { createSlice } from '@reduxjs/toolkit';

import { getOrders, updateOrderStatus } from '../../../network/OrderService';
import { apiCallBegan } from '../../actions/api';
import { store } from '../../index';

export type Customer = {
  _id: string;
  fullName: string;
  contactNumber: number;
};

type DeliveryLocation = {
  address: string;
  city: string;
  province: string;
  postalCode: string;
  fullAddress: string;
};
type Receipt = {
  total: string;
  orderTotal: string;
};

type TiffinProvider = {
  _id: string;
  user: string;
  providerName: string;
  image: string;
};

type Menu = {
  _id: string;
  mealPlan: string;
  days: { [key: string]: string };
};

export type OrderState = {
  _id: string;
  customer: Customer;
  providerAccepted: boolean;
  providerRejected: boolean;
  quantity: number;
  isCancelled: boolean;
  receipt: Receipt;
  mealFrequency: string;
  noOfDays: number;
  deliverySelected: boolean;
  deliveryLocation: DeliveryLocation;
  mealInstructions: string;
  deliveryTime: string;
  tiffinProvider: TiffinProvider;
  menu: Menu;
  startDate: string;
  orderStatus: string;
  orderId: string;
};

type Orders = {
  orders: Array<OrderState>;
  filteredOrders: Array<OrderState>;
  filterOrderStatus: string;
};

let initialState = {
  orders: [],
  filteredOrders: [],
  filterOrderStatus: '',
};

const slice = createSlice({
  name: 'orders',
  initialState,
  reducers: {
    loadOrders: (state: Orders, action) => {
      state.filterOrderStatus = '';
      const { data } = action.payload;
      state.orders = data;
      state.filteredOrders = data;
    },
    updateOrder: (state: Orders, action) => {
      const { args } = action.payload;
      const orderId = args[1];
      const status = args[2];
      //Update in actual orders first
      let selectedOrder = state.orders.find(order => order._id === orderId);
      if (!selectedOrder) return;
      selectedOrder.orderStatus = status ? 'Active' : 'Declined';
      //Update the same in filtered orders as well
      let selectedFilOrder = state.filteredOrders.find(
        order => order._id === orderId,
      );
      if (!selectedFilOrder) return;
      selectedFilOrder.orderStatus = status ? 'Active' : 'Declined';
    },
    filterOrderByStatus: (state: Orders, action) => {
      const { status } = action.payload;
      state.filteredOrders = state.orders.filter(
        order => order.orderStatus === status,
      );
      state.filterOrderStatus = status;
    },
    resetOrderStatus: (state: Orders, _) => {
      state.filteredOrders = [...state.orders];
      state.filterOrderStatus = '';
    },
  },
});

export const {
  loadOrders,
  updateOrder,
  filterOrderByStatus,
  resetOrderStatus,
} = slice.actions;

export default slice.reducer;

export const fetchOrders = (token: string) => (
  dispatch: typeof store.dispatch,
) => {
  dispatch({
    type: apiCallBegan.type,
    payload: {
      apiMethod: getOrders,
      args: [token],
      onSucess: [loadOrders.type],
      loader: true,
    },
  });
};

export const mutateOrderStatus = (
  token: string,
  orderId: string,
  status: boolean,
  description: string,
) => (dispatch: typeof store.dispatch) => {
  dispatch({
    type: apiCallBegan.type,
    payload: {
      apiMethod: updateOrderStatus,
      args: [token, orderId, status, description],
      onSucess: [updateOrder.type],
      loader: true,
    },
  });
};
