import { createSlice } from '@reduxjs/toolkit';

import { store } from '../..';
import { apiCallBegan } from '../../actions/api';
import {
  fetchAllProviderInfos,
  verifyProvider,
} from '../../../network/AdminService';

type ResponseProps = {
  imageUrl: string;
  providerId: string;
  providerName: string;
  isPaymentInfoAdded: boolean;
  isMenuAdded: boolean;
  isVerified: boolean;
  created: string;
};

type State = {
  providerData: Array<ResponseProps>;
  filteredData: Array<ResponseProps>;
  filterStatus: string;
};

const slice = createSlice({
  name: 'admin',
  initialState: {
    providerData: [],
    filteredData: [],
    filterStatus: '',
  },
  reducers: {
    fetchProviders: (state: State, action) => {
      state.filterStatus = '';
      const { data } = action.payload;
      state.providerData = data;
      state.filteredData = data;
    },
    approveOrDeclineProvider: (state: State, action) => {
      const { args } = action.payload;
      const providerId = args[1];

      const selectedProvider = state.providerData.find(
        provider => provider.providerId === providerId,
      );
      if (!selectedProvider) return;
      selectedProvider.isVerified = !selectedProvider.isVerified;
      state.filteredData = [...state.providerData];
      state.filterStatus = '';
    },
    filterByVerified: (state: State, action) => {
      const { isVerified } = action.payload;
      state.filteredData = state.providerData.filter(
        item => item.isVerified === isVerified,
      );
      state.filterStatus = isVerified ? 'Verified' : 'Unverified';
    },
    resetFilter: (state: State, _) => {
      state.filteredData = [...state.providerData];
      state.filterStatus = '';
    },
  },
});

export const {
  fetchProviders,
  approveOrDeclineProvider,
  filterByVerified,
  resetFilter,
} = slice.actions;

export default slice.reducer;

export const fetchAllVendors = (token: string) => (
  dispatch: typeof store.dispatch,
) => {
  dispatch({
    type: apiCallBegan.type,
    payload: {
      apiMethod: fetchAllProviderInfos,
      args: [token],
      onSucess: [fetchProviders.type],
      loader: true,
    },
  });
};

export const approveVendor = (
  token: string,
  providerId: string,
  isApprove: string,
) => (dispatch: typeof store.dispatch) => {
  dispatch({
    type: apiCallBegan.type,
    payload: {
      apiMethod: verifyProvider,
      args: [token, providerId, isApprove],
      onSucess: [approveOrDeclineProvider.type],
      loader: true,
    },
  });
};
