import { createSlice } from '@reduxjs/toolkit';
import { store } from '../../index';

import commons from '../../../configs/commons';
import { fetchMenu } from '../../../network/MenuService';
import { apiCallBegan } from '../../actions/api';
import { structureMenuObject } from '../helpers/reducerUtil';
import { fetchAndStructureGeneral } from './general';
import { fetchAndStructureDelivery } from './delivery';
import { fetchAndStructureAdditional } from './additional';

type MenuFrequencyProps = {
  text: string;
  key: string;
  id: number;
  price: number;
  selected: boolean;
};

type DayMenusProps = {
  text: string;
  menu: string;
};

type MealOfferingProps = {
  text: string;
  id: string;
  selected: boolean;
  frequency: Array<MenuFrequencyProps>;
  dayMenus: Array<DayMenusProps>;
};

type MealProps = {
  text: string;
  selected: boolean;
};

export type StateProps = {
  isUpdate: boolean;
  mealOfferings: Array<MealOfferingProps>;
  mealCuisines: Array<MealProps>;
  mealRestrictions: Array<MealProps>;
  image: { [key: string]: string };
  isAllergyAccomodate: boolean;
  hasMenuUpdated: boolean;
};

const slice = createSlice({
  name: 'menus',
  initialState: {
    isUpdate: false,
    mealOfferings: [
      {
        text: 'Vegetarian',
        id: 'veg',
        selected: false,
        frequency: commons.mealFrequency,
        dayMenus: commons.menuData,
      },
      {
        text: 'Vegan',
        id: 'vegan',
        selected: false,
        frequency: commons.mealFrequency,
        dayMenus: commons.menuData,
      },
      {
        text: 'Non-Vegetarian',
        id: 'nonVeg',
        selected: false,
        frequency: commons.mealFrequency,
        dayMenus: commons.menuData,
      },
      {
        text: 'Veg & Non-veg Combined',
        id: 'combined',
        selected: false,
        frequency: commons.mealFrequency,
        dayMenus: commons.menuData,
      },
    ],
    mealCuisines: [
      { text: 'gujarati', selected: false },
      { text: 'punjabi', selected: false },
      { text: 'southIndian', selected: false },
      { text: 'pakistani', selected: false },
    ],
    mealRestrictions: [
      { text: 'Jain', selected: false },
      { text: 'Gluten Free', selected: false },
      { text: 'Halal', selected: false },
    ],
    image: {},
    isAllergyAccomodate: false,
    hasMenuUpdated: false,
  },
  reducers: {
    fetchAndStructureMenus: (state: StateProps, action) => {
      const { data } = action.payload;
      structureMenuObject(state, data);
      state.hasMenuUpdated = true;
    },
    updateMenuDays: (state: StateProps, action) => {
      const { offering, name, text } = action.payload;
      const selectedOffering = state.mealOfferings.find(
        item => item.text === offering,
      );
      if (!selectedOffering) return;
      const selectedDay = selectedOffering.dayMenus.find(
        item => item.text === name,
      );
      if (!selectedDay) return;

      selectedDay.menu = text;
    },
    updateMenuFrequency: (state: StateProps, action) => {
      const { offering, text } = action.payload;
      let selectedOffering = state.mealOfferings.find(
        item => item.text === offering.text,
      );
      if (!selectedOffering) return;
      let selectedFreq = selectedOffering.frequency.find(
        item => item.text === text,
      );
      selectedFreq!.selected = !selectedFreq!.selected;
    },
    updateMenuPrices: (state: StateProps, action) => {
      const { offering, text, price } = action.payload;
      const selectedItem = state.mealOfferings
        .find(item => item.text === offering.text)
        ?.frequency.find(item => item.text === text);
      if (!selectedItem) return;
      selectedItem.price = price;
    },
    updateMealOfferings: (state: StateProps, action) => {
      const { text } = action.payload;
      const selectedItem = state.mealOfferings.find(item => item.text === text);
      if (!selectedItem) return;
      selectedItem.selected = !selectedItem.selected;
    },
    updateMealCuisines: (state: StateProps, action) => {
      const { text } = action.payload;
      const selectedItem = state.mealCuisines.find(item => item.text === text);
      if (!selectedItem) return;

      selectedItem.selected = !selectedItem.selected;
    },
    updateMealRestrictions: (state: StateProps, action) => {
      const { text } = action.payload;
      const selectedItem = state.mealRestrictions.find(
        item => item.text === text,
      );
      if (!selectedItem) return;

      selectedItem.selected = !selectedItem.selected;
    },
    uploadImage: (state: StateProps, action) => {
      const { image } = action.payload;
      state.image = image;
    },
    removeImage: (state: StateProps, action) => {
      state.image = {};
    },
    updateAllergy: (state: StateProps, action) => {
      const { isAllergy } = action.payload;
      state.isAllergyAccomodate = isAllergy;
    },
  },
});

export const {
  fetchAndStructureMenus,
  updateMenuDays,
  updateMenuFrequency,
  updateMenuPrices,
  updateMealOfferings,
  updateMealCuisines,
  updateMealRestrictions,
  uploadImage,
  removeImage,
  updateAllergy,
} = slice.actions;

export default slice.reducer;

export const fetchMenuForUser = (token: string, userId: string) => (
  dispatch: typeof store.dispatch,
) => {
  dispatch({
    type: apiCallBegan.type,
    payload: {
      apiMethod: fetchMenu,
      args: [token, userId],
      onSucess: [
        fetchAndStructureMenus.type,
        fetchAndStructureGeneral.type,
        fetchAndStructureDelivery.type,
        fetchAndStructureAdditional.type,
      ],
      loader: true,
    },
  });
};
