import { createSlice } from '@reduxjs/toolkit';

import commons from '../../../configs/commons';
import { structureAdditionalObject } from '../helpers/reducerUtil';

type SlotProps = {
  text: string;
  selected: boolean;
};

export type StateProps = {
  isMenuCustomizable: boolean;
  deliverySlots: Array<SlotProps>;
  kitchenCapacity: number;
  additionalMessages: string;
};

const slice = createSlice({
  name: 'additional',
  initialState: {
    isMenuCustomizable: false,
    deliverySlots: commons.deliveryTimeSlots,
    kitchenCapacity: 0,
    additionalMessages: '',
  },
  reducers: {
    fetchAndStructureAdditional: (state: StateProps, action) => {
      const { data } = action.payload;
      structureAdditionalObject(state, data);
    },
    updateMenuCustomization: (state: StateProps, action) => {
      const { isMenuCustomize } = action.payload;
      state.isMenuCustomizable = isMenuCustomize;
    },
    updateDeliverySlots: (state: StateProps, action) => {
      const { slot } = action.payload;
      const selectedDeliveryTime = state.deliverySlots.find(
        item => item.text === slot,
      );
      if (!selectedDeliveryTime) return;
      selectedDeliveryTime.selected = !selectedDeliveryTime.selected;
    },
    updateKitchenCapacity: (state: StateProps, action) => {
      const { capacity } = action.payload;
      state.kitchenCapacity = capacity;
    },
    updateAdditionalMessages: (state: StateProps, action) => {
      const { message } = action.payload;
      state.additionalMessages = message;
    },
  },
});

export const {
  fetchAndStructureAdditional,
  updateMenuCustomization,
  updateDeliverySlots,
  updateKitchenCapacity,
  updateAdditionalMessages,
} = slice.actions;

export default slice.reducer;
