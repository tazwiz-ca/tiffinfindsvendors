import { createSlice } from '@reduxjs/toolkit';

import commons from '../../../configs/commons';
import { structureDeliveryObject } from '../helpers/reducerUtil';

type DeliveryProps = {
  text: string;
  selected: boolean;
};
export type StateProps = {
  isDelivery: boolean;
  deliveryFrequency: Array<DeliveryProps>;
  deliveryRadius: number;
  isPickup: boolean;
};

const slice = createSlice({
  name: 'delivery',
  initialState: {
    isDelivery: false,
    deliveryFrequency: commons.deliveryFrequency,
    deliveryRadius: 0,
    isPickup: false,
  },
  reducers: {
    fetchAndStructureDelivery: (state: StateProps, action) => {
      const { data } = action.payload;
      structureDeliveryObject(state, data);
    },
    updateDeliveryPreference: (state: StateProps, action) => {
      const { deliveryPreference } = action.payload;
      state.isDelivery = deliveryPreference;
    },
    updateDeliveryFrequency: (state: StateProps, action) => {
      const { day } = action.payload;
      const selectedDeliveryDay = state.deliveryFrequency.find(
        item => item.text.toLowerCase() === day.toLowerCase(),
      );
      if (!selectedDeliveryDay) return;
      selectedDeliveryDay.selected = !selectedDeliveryDay.selected;
    },
    updateDeliveryRadius: (state: StateProps, action) => {
      const { radius } = action.payload;
      state.deliveryRadius = radius;
    },
    updatePickupPreference: (state: StateProps, action) => {
      const { pickupPreference } = action.payload;
      state.isPickup = pickupPreference;
    },
  },
});

export const {
  fetchAndStructureDelivery,
  updateDeliveryPreference,
  updateDeliveryFrequency,
  updateDeliveryRadius,
  updatePickupPreference,
} = slice.actions;

export default slice.reducer;
