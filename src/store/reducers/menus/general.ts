import { createSlice } from '@reduxjs/toolkit';
import { structureGeneralObject } from '../helpers/reducerUtil';

type AddressProps = {
  [key: string]: string;
};

type LocationProps = {
  address: AddressProps;
  coords: any;
};

export type StateProps = {
  planId: string;
  restaurantName: string;
  location: LocationProps;
  isCertified: boolean;
  isGoodStanding: boolean;
};

const slice = createSlice({
  name: 'general',
  initialState: {
    planId: '',
    restaurantName: '',
    location: { address: {}, coords: {} },
    isCertified: false,
    isGoodStanding: false,
  },
  reducers: {
    fetchAndStructureGeneral: (state: StateProps, action) => {
      const { data } = action.payload;
      structureGeneralObject(state, data);
    },
    updateRestaurantName: (state: StateProps, action) => {
      const { name } = action.payload;
      state.restaurantName = name;
    },
    updateResturantLocation: (state: StateProps, action) => {
      const { address, coords } = action.payload;
      state.location = {
        address,
        coords,
      };
    },
    updateIsCertified: (state: StateProps, action) => {
      const { isCertified } = action.payload;
      state.isCertified = isCertified;
    },
    updateIsGoodStanding: (state: StateProps, action) => {
      const { isGoodStanding } = action.payload;
      state.isGoodStanding = isGoodStanding;
    },
  },
});

export const {
  fetchAndStructureGeneral,
  updateRestaurantName,
  updateResturantLocation,
  updateIsCertified,
  updateIsGoodStanding,
} = slice.actions;

export default slice.reducer;
