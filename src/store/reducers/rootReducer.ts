import { combineReducers } from '@reduxjs/toolkit';

import loaderReducer from './common/loader';
import toastReducer from './common/toaster';
import authReducer from './auth';
import menusReducer from './menus/menus';
import deliveryReducer from './menus/delivery';
import additionalReducer from './menus/additional';
import generalReducer from './menus/general';
import paymentsReducer from './payments/payments';
import adminReducer from './admin/admin';
import ordersReducer from './orders/orders';

const rootReducer = combineReducers({
  toaster: toastReducer,
  loader: loaderReducer,
  auth: authReducer,
  menus: menusReducer,
  delivery: deliveryReducer,
  additional: additionalReducer,
  general: generalReducer,
  payments: paymentsReducer,
  admin: adminReducer,
  orders: ordersReducer,
});

export default rootReducer;
