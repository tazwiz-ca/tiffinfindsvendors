export default {
  AUTH: 'AUTH',
  AUTH_VENDOR: 'AUTH_VENDOR',
  AUTH_STACK_NAV: 'AuthStackNav',
  APP_STACK_NAV: 'AppStackNav',
  ONBOARDING: 'Onboarding',
  WELCOME: 'Welcome',
  SIGN_UP: 'SignUp',
  SIGN_IN: 'SignIn',
  FORGOT_PWD: 'ForgotPassword',
  FORGOT_PWD_SUCCESS: 'ForgotPasswordSuccess',
  RESET_PWD: 'ResetPassword',
  RESET_PWD_SUCCESS: 'ResetPasswordSuccess',
  MOBILE_NUMBER: 'MobileNumber',
  VERIFICATION: 'Verification',

  //ADMIN
  ADMIN_STACK_NAV: 'ADMIN_STACK_NAV',
  ADMIN_HOME: 'ADMIN_HOME',
  PROVIDER_DETAILS: 'PROVIDER_DETAILS',

  //NAV
  DRAWER_NAV: 'DRAWER_NAV',

  //Menu
  CREATE_MENU_NAV: 'CREATE_MENU_NAV',
  GENERAL_INFO: 'GENERAL_INFO',
  MENU_INFO: 'MENU_INFO',
  DELIVERY_INFO: 'DELIVERY_INFO',
  ADDITIONAL_INFO: 'ADDITIONAL_INFO',
  ADD_MENUS: 'ADD_MENUS',

  //Orders
  ORDERS: 'Orders',

  //Payments
  PAYMENTS: 'Payments',

  GET_HELP: 'GetHelp',
};
