import React, { useEffect, useState } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { useDispatch, useSelector } from 'react-redux';

import AppStackNav from './AppStackNav';
import { errorToast } from '../store/reducers/common/toaster';
import helpers from '../utils/helpers';
import routes from './routes';
import AuthStackNav from './AuthStackNav';
import { loadVendor } from '../store/reducers/auth';
import { RootState } from '../store';
import AdminStackNav from './AdminStackNav';

export default function RootStackNav(): React.ReactElement {
  const [isLoggedIn, setIsLoggedIn] = useState<any>(0);
  const vendor = useSelector((state: RootState) => state.auth.vendor);
  const Stack = createStackNavigator();
  const dispatch = useDispatch();

  useEffect(() => {
    if (!vendor.typeOfUser) {
      setIsLoggedIn(0);
    }
    (async () => {
      const data = await helpers.getData('vendor');
      if (!data || !data.token) {
        return setIsLoggedIn(false);
      } else if (data && data.token && !data.vendor?.phoneNum) {
        dispatch({
          type: errorToast.type,
          payload: {
            message:
              'You have not provided/verified your mobile number!!Please login again',
          },
        });
        return setIsLoggedIn(false);
      }
      dispatch({
        type: loadVendor.type,
        payload: {
          token: data.token,
          vendor: data.vendor,
        },
      });
      setIsLoggedIn(true);
    })();
  }, []);

  if (isLoggedIn === 0) {
    return <></>;
  }

  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      {!isLoggedIn && (
        <>
          <Stack.Screen name={routes.AUTH_STACK_NAV} component={AuthStackNav} />
        </>
      )}

      {isLoggedIn && (
        <>
          {vendor.typeOfUser === 'tiffinProvider' ? (
            <>
              <Stack.Screen
                name={routes.APP_STACK_NAV}
                component={AppStackNav}
              />
              <Stack.Screen
                name={routes.AUTH_STACK_NAV}
                component={AuthStackNav}
              />
            </>
          ) : (
            <>
              <Stack.Screen
                name={routes.ADMIN_STACK_NAV}
                component={AdminStackNav}
                options={{ headerShown: false }}
              />
              <Stack.Screen
                name={routes.AUTH_STACK_NAV}
                component={AuthStackNav}
              />
            </>
          )}
        </>
      )}
    </Stack.Navigator>
  );
}
