import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { useDispatch, useSelector } from 'react-redux';

import routes from './routes';
import AdminHome from '../screens/admin/AdminHome';
import colors from '../configs/colors';
import { fetchAllVendors } from '../store/reducers/admin/admin';
import { RootState } from '../store';
import ProviderDetails from '../screens/admin/ProviderDetails';
import { subscribe } from '../notification/NotificationService';

export default function AdminStackNav(): React.ReactElement {
  const Stack = createStackNavigator();
  const token = useSelector((state: RootState) => state.auth.token);
  const id = useSelector((state: RootState) => state.auth.vendor.id);
  const dispatch = useDispatch();
  const options = {
    title: 'Admin Home',
    headerTitleStyle: styles.title,
    headerBackground: () => <View style={styles.headerBg} />,
  };

  React.useEffect(() => {
    //Subscribe for notifications
    subscribe(id);
    dispatch(fetchAllVendors(token));
  }, []);

  return (
    <Stack.Navigator>
      <Stack.Screen
        name={routes.ADMIN_HOME}
        component={AdminHome}
        options={options}
      />
      <Stack.Screen
        name={routes.PROVIDER_DETAILS}
        component={ProviderDetails}
        options={{
          headerShown: true,
          headerTransparent: true,
          title: '',
        }}
      />
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({
  title: {
    fontFamily: 'JosefinSans-Regular',
    color: colors.white,
    fontSize: 20,
  },
  headerBg: {
    flex: 1,
    backgroundColor: colors.primary,
  },
});
