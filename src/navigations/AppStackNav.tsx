import * as React from 'react';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import { useDispatch, useSelector } from 'react-redux';

import routes from './routes';
import AddMenuPop from '../components/menu/AddMenuPop';
import DrawerStackNav from './DrawerStackNav';
import { RootState } from '../store';
import { fetchVendorProfile } from '../store/reducers/payments/payments';
import { subscribe } from '../notification/NotificationService';

export default function AppStackNav(): React.ReactElement {
  const token = useSelector((state: RootState) => state.auth.token);
  const id = useSelector((state: RootState) => state.auth.vendor.id);

  const dispatch = useDispatch();
  const Stack = createStackNavigator();

  React.useEffect(() => {
    if (!token) return;
    //Subscribe for notifications
    subscribe(id);
    dispatch(fetchVendorProfile(token));
  }, [token]);

  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name={routes.DRAWER_NAV} component={DrawerStackNav} />
      <Stack.Screen
        name={routes.ADD_MENUS}
        component={AddMenuPop}
        options={{
          cardStyleInterpolator:
            CardStyleInterpolators.forRevealFromBottomAndroid,
          gestureEnabled: false,
        }}
      />
    </Stack.Navigator>
  );
}
