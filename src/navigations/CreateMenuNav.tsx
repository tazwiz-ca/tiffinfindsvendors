import React, { useEffect } from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { StatusBar } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import colors from '../configs/colors';
import routes from './routes';
import GeneralInfo from '../screens/menu/GeneralInfo';
import NavHeader from '../components/common/NavHeader';
import MenuInfo from '../screens/menu/MenuInfo';
import DeliveryInfo from '../screens/menu/DeliveryInfo';
import AdditionalInfo from '../screens/menu/AdditionalInfo';
import { fetchMenuForUser } from '../store/reducers/menus/menus';
import { RootState } from '../store';

const CreateMenuNav: React.FunctionComponent = () => {
  const token = useSelector((state: RootState) => state.auth.token);
  const userId = useSelector((state: RootState) => state.auth.vendor.id);
  const dispatch = useDispatch();

  const tabBarOptions = {
    scrollEnabled: true,
    showIcon: true,
    pressColor: '',
    labelStyle: { fontSize: 12, fontFamily: 'Apercu-Bold' },
    activeTintColor: 'white',
    pressOpacity: 1,
    inactiveTintColor: '#9394A9',
    renderIndicator: () => null,
    style: { backgroundColor: colors.textInputBorder },
  };

  useEffect(() => {
    dispatch(fetchMenuForUser(token, userId));
  }, []);

  const Tab = createMaterialTopTabNavigator();
  return (
    <>
      <StatusBar barStyle="light-content" />
      <NavHeader title="Create Menu" />
      <Tab.Navigator
        swipeEnabled={false}
        backBehavior="none"
        tabBarOptions={tabBarOptions}
        initialRouteName={routes.GENERAL_INFO}>
        <Tab.Screen
          name={routes.GENERAL_INFO}
          component={GeneralInfo}
          listeners={() => ({
            tabPress: e => {
              e.preventDefault();
            },
          })}
        />
        <Tab.Screen
          name={routes.MENU_INFO}
          component={MenuInfo}
          listeners={() => ({
            tabPress: e => {
              e.preventDefault();
            },
          })}
        />
        <Tab.Screen
          name={routes.DELIVERY_INFO}
          component={DeliveryInfo}
          listeners={() => ({
            tabPress: e => {
              e.preventDefault();
            },
          })}
        />
        <Tab.Screen
          name={routes.ADDITIONAL_INFO}
          component={AdditionalInfo}
          listeners={() => ({
            tabPress: e => {
              e.preventDefault();
            },
          })}
        />
      </Tab.Navigator>
    </>
  );
};

export default CreateMenuNav;
