import * as React from 'react';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItem,
  DrawerItemList,
} from '@react-navigation/drawer';
import { SafeAreaView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { DrawerActions, useNavigation } from '@react-navigation/native';
import MaterialCommIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useDispatch, useSelector } from 'react-redux';

import routes from './routes';
import CreateMenuNav from './CreateMenuNav';
import Orders from '../screens/orders/Orders';
import Payments from '../screens/payments/Payments';
import VendorInfo from '../components/drawernav/VendorInfo';
import helpers from '../utils/helpers';
import BottomSlideMenu from '../components/common/BottomSlideMenu';
import { logout } from '../store/reducers/auth';
import colors from '../configs/colors';
import { RootState } from '../store';
import GetHelp from '../screens/help/GetHelp';
import {
  getInitialNotification,
  notificationForegroundHandler,
  onMessageClicked,
  unsubscribe,
} from '../notification/NotificationService';
import { fetchOrders } from '../store/reducers/orders/orders';

export default function DrawerStackNav(): React.ReactElement {
  const dispatch = useDispatch();
  const Drawer = createDrawerNavigator();
  const navigation = useNavigation();
  const [isModalVisible, setIsModalVisible] = React.useState(false);
  const id = useSelector((state: RootState) => state.auth.vendor.id);
  const token = useSelector((state: RootState) => state.auth.token);
  const hasMenuUpdated = useSelector(
    (state: RootState) => state.menus.hasMenuUpdated,
  );
  const isPaymentVerified = useSelector(
    (state: RootState) => state.payments.isPaymentVerified,
  );

  React.useEffect(() => {
    onMessageClicked(() => {
      navigation.navigate(routes.ORDERS);
      dispatch(fetchOrders(token));
    });
    getInitialNotification().then(() => dispatch(fetchOrders(token)));
    dispatch(notificationForegroundHandler(token));
  }, []);

  // Navigate the user to the orders screen if the menu and payment info is setup
  React.useEffect(() => {
    if (hasMenuUpdated && isPaymentVerified) {
      navigation.navigate(routes.ORDERS);
    }
  }, [hasMenuUpdated, isPaymentVerified]);

  const handleLogout = async () => {
    setIsModalVisible(false);
    //Unsubscribe the user from getting notifications for this channel
    await unsubscribe(id);
    dispatch({ type: logout.type });
    await helpers.removeData('vendor');
    navigation.reset({
      index: 0,
      routes: [{ name: routes.AUTH_STACK_NAV }],
    });
  };

  const handleLogoutPress = () => {
    DrawerActions.closeDrawer();
    setIsModalVisible(true);
  };

  const CustomDrawerComponent = (props: any) => (
    <SafeAreaView style={{ flex: 1 }}>
      <VendorInfo />
      <DrawerContentScrollView {...props} style={{ padding: 0 }}>
        <DrawerItemList
          {...props}
          itemStyle={{ marginTop: 0, padding: 0, margin: 0 }}
          labelStyle={{ marginLeft: 10, fontSize: 16, fontWeight: '600' }}
        />
        <DrawerItem
          {...props}
          onPress={handleLogoutPress}
          label="Logout"
          labelStyle={{
            marginLeft: 10,
            fontSize: 16,
            fontWeight: '600',
            marginTop: 20,
            color: colors.primary,
          }}
          icon={({ size, color }: any) => (
            <MaterialCommIcons
              name="logout"
              size={size}
              color={colors.primary}
              style={{ marginTop: 23 }}
            />
          )}
        />
      </DrawerContentScrollView>
    </SafeAreaView>
  );

  return (
    <>
      <BottomSlideMenu
        visibility={isModalVisible}
        onCancel={handleLogout}
        onSuccess={() => setIsModalVisible(false)}
        title="Logout?"
        subtitle="Are you sure you want to logout?"
        btn1Text="LOGOUT"
        btn2Text="CANCEL"
      />

      <Drawer.Navigator drawerContent={CustomDrawerComponent}>
        <Drawer.Screen
          name={routes.CREATE_MENU_NAV}
          component={CreateMenuNav}
          options={{
            title: 'Menus',
            drawerIcon: ({ size, color }: any) => (
              <Icon name="restaurant-menu" size={size} color={color} />
            ),
          }}
        />
        <Drawer.Screen
          name={routes.ORDERS}
          component={Orders}
          options={{
            title: 'Orders',
            drawerIcon: ({ size, color }: any) => (
              <Icon name="receipt" size={size} color={color} />
            ),
          }}
        />
        <Drawer.Screen
          name={routes.PAYMENTS}
          component={Payments}
          options={{
            title: 'Payments',
            drawerIcon: ({ size, color }: any) => (
              <Icon name="payment" size={size} color={color} />
            ),
          }}
        />
        <Drawer.Screen
          name={routes.GET_HELP}
          component={GetHelp}
          options={{
            title: 'Get Help',
            drawerIcon: ({ size, color }: any) => (
              <Icon name="help" size={size} color={color} />
            ),
          }}
        />
      </Drawer.Navigator>
    </>
  );
}
