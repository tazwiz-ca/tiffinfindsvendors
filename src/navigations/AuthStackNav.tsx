import React, { useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import * as Sentry from '@sentry/react-native';

import routes from './routes';
import Welcome from '../screens/auth/Welcome';
import SignUp from '../screens/auth/SignUp';
import SignIn from '../screens/auth/SignIn';
import MobileNumber from '../screens/auth/MobileNumber';
import Verification from '../screens/auth/Verification';
import AppStackNav from './AppStackNav';
import ForgotPassword from '../screens/auth/ForgotPassword';
import ForgotPwdSuccess from '../screens/auth/ForgotPwdSuccess';
import ResetPassword from '../screens/auth/ResetPassword';
import ResetPwdSuccess from '../screens/auth/ResetPwdSuccess';
import AdminStackNav from './AdminStackNav';
import { Platform } from 'react-native';
import { requestNotificationPermission } from '../notification/NotificationService';

export default function AuthStackNav() {
  useEffect(() => {
    if (Platform.OS == 'ios') {
      requestNotificationPermission().catch(error => {
        console.log(error);
        Sentry.captureException(error);
      });
    }
  }, []);
  const Stack = createStackNavigator();

  return (
    <Stack.Navigator
      screenOptions={{
        cardOverlayEnabled: false,
        headerStyle: {
          elevation: 1,
          shadowOpacity: 0,
        },
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontWeight: 'bold',
        },
        headerTintColor: '#212121',
        headerTitleAlign: 'center',
      }}>
      <Stack.Screen
        name={routes.WELCOME}
        component={Welcome}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={routes.SIGN_UP}
        component={SignUp}
        options={{
          title: 'Create Account',
          headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
          },
        }}
      />
      <Stack.Screen
        name={routes.SIGN_IN}
        component={SignIn}
        options={{
          title: 'Sign In',
          headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
          },
        }}
      />
      <Stack.Screen
        name={routes.FORGOT_PWD}
        component={ForgotPassword}
        options={{
          title: 'Forgot Password',
          headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
          },
        }}
      />
      <Stack.Screen
        name={routes.FORGOT_PWD_SUCCESS}
        component={ForgotPwdSuccess}
        options={{
          title: 'Forgot Password',
          headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
          },
        }}
      />
      <Stack.Screen
        name={routes.RESET_PWD}
        component={ResetPassword}
        options={{
          title: 'Reset Password',
          headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
          },
        }}
      />
      <Stack.Screen
        name={routes.RESET_PWD_SUCCESS}
        component={ResetPwdSuccess}
        options={{
          title: 'Reset Password',
          headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
          },
        }}
      />
      <Stack.Screen
        name={routes.MOBILE_NUMBER}
        component={MobileNumber}
        options={{ headerShown: true, title: '' }}
      />
      <Stack.Screen
        name={routes.VERIFICATION}
        component={Verification}
        options={{
          headerShown: true,
          headerTransparent: true,
          title: '',
          headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
          },
        }}
      />
      <Stack.Screen
        name={routes.APP_STACK_NAV}
        component={AppStackNav}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name={routes.ADMIN_STACK_NAV}
        component={AdminStackNav}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}
